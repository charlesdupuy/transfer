package org.charess.replication.transfer;

import org.charess.replication.transfer.controller.RestartController;
import org.charess.replication.transfer.service.RestartService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.List;
import java.util.Set;

@SpringBootApplication
public class TransferApplication implements ApplicationRunner {

    private static ConfigurableApplicationContext context;
    private static final Logger log = LoggerFactory.getLogger(RestartController.class);

    public static void main(String[] args) {
        context = SpringApplication.run(TransferApplication.class, args);
    }

    public static void restart(String dataBaseName) {
        ApplicationArguments args = context.getBean(ApplicationArguments.class);
        Thread thread = new Thread(() -> {
            context.close();
            context = SpringApplication.run(TransferApplication.class, args.getSourceArgs());
        });
        thread.setDaemon(false);
        thread.start();
        context.getEnvironment().getSystemProperties().put("data.base.name", dataBaseName);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        boolean containsOption = args.containsOption("data.base.name");
        log.info("data.base.name: " + containsOption);
    }
}
