package org.charess.replication.transfer.controller;

public class DataBaseName {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}