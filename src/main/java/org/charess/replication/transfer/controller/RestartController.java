package org.charess.replication.transfer.controller;

import org.charess.replication.transfer.TransferApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
@RequestMapping("/reload")
public class RestartController {

    private final Logger log = LoggerFactory.getLogger(RestartController.class);

    @Value("${data.base.name}")
    private String defaultDatabasename;

    @PostMapping
    public void restart(@RequestBody DataBaseName dataBaseName){
        log.info("------{}----", defaultDatabasename);
        setDataBaseName(dataBaseName.getName());
        TransferApplication.restart(dataBaseName.getName());
    }

    private void setDataBaseName(String dataBaseName){
        Path path = Paths.get(RestartController.class.getResource("/data.sql").getPath());
        log.info("--path absolute:{}---relative:{}--", path.toAbsolutePath(), path);

        Charset charset = StandardCharsets.UTF_8;
        try {
            String content = new String(Files.readAllBytes(path.toAbsolutePath()), charset);
            content = content.replaceAll(this.defaultDatabasename, dataBaseName);
            Files.write(path, content.getBytes(charset));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

