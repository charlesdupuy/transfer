
-- SURVEILLANCE DU TRAITEMENT TB----------------------------------------------------------------------------------------------------------------

-- BACIILOCCOPIE
INSERT INTO replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "BACIILOCCOPIE", e.createDate, now(), 'x', o.value_numeric
FROM itech.encounter e, itech.patient p, itech.obs o
WHERE e.patientID = p.patientID
  AND e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id in ('163625','163632','163639','163646','163653','163660','163667','163674')
    ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- GENXPERT
INSERT INTO replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "GENXPERT", e.createDate, now(), 'x', o.value_numeric
FROM itech.encounter e, itech.patient p, itech.obs o
WHERE e.patientID = p.patientID
  AND e.encounterType=1
  and o.encounter_id=e.encounter_id
  and (o.concept_id in ('163626','163632','163639','163646','163653','163660','163667','163674')
    OR o.concept_id in('163627','163634','163641','163648','163653','163660','163667','163674'))
    ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- CULTURE
INSERT INTO replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "CULTURE", e.createDate, now(), 'x', o.value_numeric
FROM itech.encounter e, itech.patient p, itech.obs o
WHERE e.patientID = p.patientID
  AND e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id in ('163628','163635','163642','163649','163656','163663','163670','163677')
    ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- DST
INSERT INTO replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "DST", e.createDate, now(), 'x', o.value_numeric
FROM itech.encounter e, itech.patient p, itech.obs o
WHERE e.patientID = p.patientID
  AND e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id in ('163629','163636','163643','163650','163657','163664','163671','163678')
    ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- POIDS
INSERT INTO replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "POIDS", e.createDate, now(), 'x', o.value_numeric
FROM itech.encounter e, itech.patient p, itech.obs o
WHERE e.patientID = p.patientID
  AND e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id in ('163630','163637','163644','163651','163658','163665','163672','163679')
    ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- MÉDICAMENTS : ALLERGIES---------------------------------------------------------------------------------------------------------------------------------------------

-- AUCUNE
INSERT INTO replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "AUCUNE", e.createDate, now(), 'x', noneTreatments
FROM itech.encounter e, itech.patient p, itech.vitals v
WHERE e.patientID = p.patientID
  AND e.encounterType=1
  AND e.patientID = v.patientID
  AND ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  AND e.seqnum=v.seqnum
  AND ifnull(v.noneTreatments,0)>0
    ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- MEDICAMENTS
INSERT INTO replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "MEDICAMENTS", e.createDate, now(), 'x', allergyName
FROM itech.encounter e, itech.patient p, itech.allergies a
WHERE e.patientID = p.patientID
  AND e.encounterType=1
  AND e.patientID = a.patientID
  AND ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(a.visitDateYy,a.visitDateMm,a.visitDateDd)
  AND e.seqnum=a.seqnum
  AND ifnull(a.allergyName,0)>0
    ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- DEBUT
INSERT INTO replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "DEBUT", e.createDate, now(), 'x', concat(allergyStartYy,' ',allergyStartMm)
FROM itech.encounter e, itech.patient p, itech.allergies a
WHERE e.patientID = p.patientID
  AND e.encounterType=1
  AND e.patientID = a.patientID
  AND ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(a.visitDateYy,a.visitDateMm,a.visitDateDd)
  AND e.seqnum=a.seqnum
  AND ifnull(a.allergyStartYy,0)>0
    ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- FIN
INSERT INTO replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "FIN", e.createDate, now(), 'x', concat(allergyStopYy,' ',allergyStopMm)
FROM itech.encounter e, itech.patient p, itech.allergies a
WHERE e.patientID = p.patientID
  AND e.encounterType=1
  AND e.patientID = a.patientID
  AND ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(a.visitDateYy,a.visitDateMm,a.visitDateDd)
  AND e.seqnum=a.seqnum
  AND ifnull(a.allergyStopYy,0)>0
    ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- TYPE DE RÉACTION
-- Erup
INSERT INTO replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "Erup", e.createDate, now(), 'x', rash
FROM itech.encounter e, itech.patient p, itech.allergies a
WHERE e.patientID = p.patientID
  AND e.encounterType=1
  AND e.patientID = a.patientID
  AND ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(a.visitDateYy,a.visitDateMm,a.visitDateDd)
  AND e.seqnum=a.seqnum
  AND ifnull(a.rash,0)>0
    ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- ErupF
INSERT INTO replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "ErupF", e.createDate, now(), 'x', rashF
FROM itech.encounter e, itech.patient p, itech.allergies a
WHERE e.patientID = p.patientID
  AND e.encounterType=1
  AND e.patientID = a.patientID
  AND ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(a.visitDateYy,a.visitDateMm,a.visitDateDd)
  AND e.seqnum=a.seqnum
  AND ifnull(a.rashF,0)>0
    ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- ABC
INSERT INTO replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "ABC", e.createDate, now(), 'x', ABC
FROM itech.encounter e, itech.patient p, itech.allergies a
WHERE e.patientID = p.patientID
  AND e.encounterType=1
  AND e.patientID = a.patientID
  AND ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(a.visitDateYy,a.visitDateMm,a.visitDateDd)
  AND e.seqnum=a.seqnum
  AND ifnull(a.ABC,0)>0
    ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- Pap
INSERT INTO replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "Pap", e.createDate, now(), 'x', hives
FROM itech.encounter e, itech.patient p, itech.allergies a
WHERE e.patientID = p.patientID
  AND e.encounterType=1
  AND e.patientID = a.patientID
  AND ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(a.visitDateYy,a.visitDateMm,a.visitDateDd)
  AND e.seqnum=a.seqnum
  AND ifnull(a.hives,0)>0
    ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- SJ
INSERT INTO replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "SJ", e.createDate, now(), 'x', SJ
FROM itech.encounter e, itech.patient p, itech.allergies a
WHERE e.patientID = p.patientID
  AND e.encounterType=1
  AND e.patientID = a.patientID
  AND ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(a.visitDateYy,a.visitDateMm,a.visitDateDd)
  AND e.seqnum=a.seqnum
  AND ifnull(a.SJ,0)>0
    ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- Anaph
INSERT INTO replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "Anaph", e.createDate, now(), 'x', anaph
FROM itech.encounter e, itech.patient p, itech.allergies a
WHERE e.patientID = p.patientID
  AND e.encounterType=1
  AND e.patientID = a.patientID
  AND ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(a.visitDateYy,a.visitDateMm,a.visitDateDd)
  AND e.seqnum=a.seqnum
  AND ifnull(a.anaph,0)>0
    ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- Autre
INSERT INTO replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "Autre", e.createDate, now(), 'x', allergyOther
FROM itech.encounter e, itech.patient p, itech.allergies a
WHERE e.patientID = p.patientID
  AND e.encounterType=1
  AND e.patientID = a.patientID
  AND ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(a.visitDateYy,a.visitDateMm,a.visitDateDd)
  AND e.seqnum=a.seqnum
  AND ifnull(a.allergyOther,0)>0
    ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- isanteplus requete

--       SURVEILLANCE DU TRAITEMENT TB  //no row selected
--       Obs: c2c7db51-d427-4c21-9655-ae9ee604c81c
--       bacilloccopie
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'bacilloccopie', date(e.encounter_datetime),
    case
    when o.value_coded =664 then 'Négatif'
    when o.value_coded =1362 then 'Positif+'
    when o.value_coded =1363 then 'Positif++'
    when o.value_coded =1364 then 'Positif+++'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0 and o.concept_id =307
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'c2c7db51-d427-4c21-9655-ae9ee604c81c'
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'genxpert_bk', date(e.encounter_datetime),
    case
    when o.value_coded =1301 then 'Positif'
    when o.value_coded =1302 then 'Négatif'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'c2c7db51-d427-4c21-9655-ae9ee604c81c'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       genxpert_rif --       ps: Meme uuid que genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'genxpert_rif', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then 'Sensible'
    when o.value_coded =162203 then 'Resistant'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'c2c7db51-d427-4c21-9655-ae9ee604c81c'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       culture --       --       -A revoir: deuxieme uuid inexistant
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'culture', date(e.encounter_datetime),
    case
    when c2.uuid ='36d6616b-8c7c-4768-9f38-2be4b704fccd' then 'Positif pour MTB'
    when c2.uuid  = 'f4ee3bcc-947c-4390-9190-a335c2cd5868' then 'Positif pour non MTB'
    when o.value_coded  =664 then 'Négatif'
    when o.value_coded  =160008 then 'Contaminé'
    else ''
end, 'x',date(now())
      from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0 and o.concept_id =159982
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'c2c7db51-d427-4c21-9655-ae9ee604c81c'
 inner join cepoz.concept c2 on c2.concept_id = o.value_coded and c2.uuid in ('36d6616b-8c7c-4768-9f38-2be4b704fccd', 'f4ee3bcc-947c-4390-9190-a335c2cd5868')
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       dst
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'dst', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then '"Sensible à la Rifampicine'
    when o.value_coded =162203 then 'Résistant à la rifampicine'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id =159984
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'c2c7db51-d427-4c21-9655-ae9ee604c81c'
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       poids
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'poids', date(e.encounter_datetime), o.value_numeric, 'x',date(now())
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id = 5089
    inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'c2c7db51-d427-4c21-9655-ae9ee604c81c'
    where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       Obs: b05762eb-b8f5-4af2-8105-cc68a6dc9d3b
--       bacilloccopie
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'bacilloccopie', date(e.encounter_datetime),
    case
    when o.value_coded =664 then 'Négatif'
    when o.value_coded =1362 then 'Positif+'
    when o.value_coded =1363 then 'Positif++'
    when o.value_coded =1364 then 'Positif+++'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0 and o.concept_id =307
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'b05762eb-b8f5-4af2-8105-cc68a6dc9d3b'
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'genxpert_bk', date(e.encounter_datetime),
    case
    when o.value_coded =1301 then 'Positif'
    when o.value_coded =1302 then 'Négatif'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'b05762eb-b8f5-4af2-8105-cc68a6dc9d3b'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       genxpert_rif --       ps: Meme uuid que genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'genxpert_rif', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then 'Sensible'
    when o.value_coded =162203 then 'Resistant'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'b05762eb-b8f5-4af2-8105-cc68a6dc9d3b'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       culture --       --       -A revoir: deuxieme uuid inexistant
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'culture', date(e.encounter_datetime),
    case
    when c2.uuid ='36d6616b-8c7c-4768-9f38-2be4b704fccd' then 'Positif pour MTB'
    when c2.uuid  = 'f4ee3bcc-947c-4390-9190-a335c2cd5868' then 'Positif pour non MTB'
    when o.value_coded  =664 then 'Négatif'
    when o.value_coded  =160008 then 'Contaminé'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0 and o.concept_id =159982
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'b05762eb-b8f5-4af2-8105-cc68a6dc9d3b'
 inner join cepoz.concept c2 on c2.concept_id = o.value_coded and c2.uuid in ('36d6616b-8c7c-4768-9f38-2be4b704fccd', 'f4ee3bcc-947c-4390-9190-a335c2cd5868')
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       dst
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'dst', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then '"Sensible à la Rifampicine'
    when o.value_coded =162203 then 'Résistant à la rifampicine'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id =159984
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'b05762eb-b8f5-4af2-8105-cc68a6dc9d3b'
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       poids
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'poids', date(e.encounter_datetime), o.value_numeric, 'x',date(now())
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id = 5089
    inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'b05762eb-b8f5-4af2-8105-cc68a6dc9d3b'
    where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       Obs: be7a27bb-2dcd-4bff-b696-ccdbbd2d3192
--       bacilloccopie
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'bacilloccopie', date(e.encounter_datetime),
    case
    when o.value_coded =664 then 'Négatif'
    when o.value_coded =1362 then 'Positif+'
    when o.value_coded =1363 then 'Positif++'
    when o.value_coded =1364 then 'Positif+++'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0 and o.concept_id =307
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'be7a27bb-2dcd-4bff-b696-ccdbbd2d3192'
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'genxpert_bk', date(e.encounter_datetime),
    case
    when o.value_coded =1301 then 'Positif'
    when o.value_coded =1302 then 'Négatif'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'be7a27bb-2dcd-4bff-b696-ccdbbd2d3192'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       genxpert_rif --       ps: Meme uuid que genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'genxpert_rif', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then 'Sensible'
    when o.value_coded =162203 then 'Resistant'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'be7a27bb-2dcd-4bff-b696-ccdbbd2d3192'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       culture --       --       -A revoir: deuxieme uuid inexistant
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'culture', date(e.encounter_datetime),
    case
    when c2.uuid ='36d6616b-8c7c-4768-9f38-2be4b704fccd' then 'Positif pour MTB'
    when c2.uuid  = 'f4ee3bcc-947c-4390-9190-a335c2cd5868' then 'Positif pour non MTB'
    when o.value_coded  =664 then 'Négatif'
    when o.value_coded  =160008 then 'Contaminé'
    else ''
end, 'x',date(now())
      from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0 and o.concept_id =159982
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'be7a27bb-2dcd-4bff-b696-ccdbbd2d3192'
 inner join cepoz.concept c2 on c2.concept_id = o.value_coded and c2.uuid in ('36d6616b-8c7c-4768-9f38-2be4b704fccd', 'f4ee3bcc-947c-4390-9190-a335c2cd5868')
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       dst
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'dst', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then '"Sensible à la Rifampicine'
    when o.value_coded =162203 then 'Résistant à la rifampicine'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and concept_id =159984
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'be7a27bb-2dcd-4bff-b696-ccdbbd2d3192'
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       poids
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'poids', date(e.encounter_datetime), o.value_numeric, 'x',date(now())
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id = 5089
    inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'be7a27bb-2dcd-4bff-b696-ccdbbd2d3192'
    where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       Obs: fdd9d953-004a-4e9a-ab93-9afaa0e090fa
--       bacilloccopie
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'bacilloccopie', date(e.encounter_datetime),
    case
    when o.value_coded =664 then 'Négatif'
    when o.value_coded =1362 then 'Positif+'
    when o.value_coded =1363 then 'Positif++'
    when o.value_coded =1364 then 'Positif+++'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0 and o.concept_id =307
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'fdd9d953-004a-4e9a-ab93-9afaa0e090fa'
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'genxpert_bk', date(e.encounter_datetime),
    case
    when o.value_coded =1301 then 'Positif'
    when o.value_coded =1302 then 'Négatif'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'fdd9d953-004a-4e9a-ab93-9afaa0e090fa'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       genxpert_rif --       ps: Meme uuid que genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'genxpert_rif', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then 'Sensible'
    when o.value_coded =162203 then 'Resistant'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'fdd9d953-004a-4e9a-ab93-9afaa0e090fa'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       culture --       --       -A revoir: deuxieme uuid inexistant
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'culture', date(e.encounter_datetime),
    case
    when c2.uuid ='36d6616b-8c7c-4768-9f38-2be4b704fccd' then 'Positif pour MTB'
    when c2.uuid  = 'f4ee3bcc-947c-4390-9190-a335c2cd5868' then 'Positif pour non MTB'
    when o.value_coded  =664 then 'Négatif'
    when o.value_coded  =160008 then 'Contaminé'
    else ''
end, 'x',date(now())
      from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0 and o.concept_id =159982
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'fdd9d953-004a-4e9a-ab93-9afaa0e090fa'
 inner join cepoz.concept c2 on c2.concept_id = o.value_coded and c2.uuid in ('36d6616b-8c7c-4768-9f38-2be4b704fccd', 'f4ee3bcc-947c-4390-9190-a335c2cd5868')
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       dst
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'dst', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then '"Sensible à la Rifampicine'
    when o.value_coded =162203 then 'Résistant à la rifampicine'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and concept_id =159984
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'fdd9d953-004a-4e9a-ab93-9afaa0e090fa'
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       poids
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'poids', date(e.encounter_datetime), o.value_numeric, 'x',date(now())
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id = 5089
    inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'fdd9d953-004a-4e9a-ab93-9afaa0e090fa'
    where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       Obs: dc12da69-937e-4cfc-afed-a2f07b44eff4
--       bacilloccopie
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'bacilloccopie', date(e.encounter_datetime),
    case
    when o.value_coded =664 then 'Négatif'
    when o.value_coded =1362 then 'Positif+'
    when o.value_coded =1363 then 'Positif++'
    when o.value_coded =1364 then 'Positif+++'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0 and o.concept_id =307
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'dc12da69-937e-4cfc-afed-a2f07b44eff4'
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'genxpert_bk', date(e.encounter_datetime),
    case
    when o.value_coded =1301 then 'Positif'
    when o.value_coded =1302 then 'Négatif'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'dc12da69-937e-4cfc-afed-a2f07b44eff4'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       genxpert_rif --       ps: Meme uuid que genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'genxpert_rif', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then 'Sensible'
    when o.value_coded =162203 then 'Resistant'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'dc12da69-937e-4cfc-afed-a2f07b44eff4'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       culture --       --       -A revoir: deuxieme uuid inexistant
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'culture', date(e.encounter_datetime),
    case
    when c2.uuid ='36d6616b-8c7c-4768-9f38-2be4b704fccd' then 'Positif pour MTB'
    when c2.uuid  = 'f4ee3bcc-947c-4390-9190-a335c2cd5868' then 'Positif pour non MTB'
    when o.value_coded  =664 then 'Négatif'
    when o.value_coded  =160008 then 'Contaminé'
    else ''
end, 'x',date(now())
      from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0 and o.concept_id =159982
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'dc12da69-937e-4cfc-afed-a2f07b44eff4'
 inner join cepoz.concept c2 on c2.concept_id = o.value_coded and c2.uuid in ('36d6616b-8c7c-4768-9f38-2be4b704fccd', 'f4ee3bcc-947c-4390-9190-a335c2cd5868')
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       dst
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'dst', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then '"Sensible à la Rifampicine'
    when o.value_coded =162203 then 'Résistant à la rifampicine'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and concept_id =159984
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'dc12da69-937e-4cfc-afed-a2f07b44eff4'
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       poids
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'poids', date(e.encounter_datetime), o.value_numeric, 'x',date(now())
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id = 5089
    inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'dc12da69-937e-4cfc-afed-a2f07b44eff4'
    where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       Obs: 224546bb-03da-4793-9ef6-3d42adc5b353
--       bacilloccopie
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'bacilloccopie', date(e.encounter_datetime),
    case
    when o.value_coded =664 then 'Négatif'
    when o.value_coded =1362 then 'Positif+'
    when o.value_coded =1363 then 'Positif++'
    when o.value_coded =1364 then 'Positif+++'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0 and o.concept_id =307
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '224546bb-03da-4793-9ef6-3d42adc5b353'
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'genxpert_bk', date(e.encounter_datetime),
    case
    when o.value_coded =1301 then 'Positif'
    when o.value_coded =1302 then 'Négatif'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '224546bb-03da-4793-9ef6-3d42adc5b353'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       genxpert_rif --       ps: Meme uuid que genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'genxpert_rif', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then 'Sensible'
    when o.value_coded =162203 then 'Resistant'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '224546bb-03da-4793-9ef6-3d42adc5b353'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       culture --       --       -A revoir: deuxieme uuid inexistant
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'culture', date(e.encounter_datetime),
    case
    when c2.uuid ='36d6616b-8c7c-4768-9f38-2be4b704fccd' then 'Positif pour MTB'
    when c2.uuid  = 'f4ee3bcc-947c-4390-9190-a335c2cd5868' then 'Positif pour non MTB'
    when o.value_coded  =664 then 'Négatif'
    when o.value_coded  =160008 then 'Contaminé'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0 and o.concept_id =159982
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '224546bb-03da-4793-9ef6-3d42adc5b353'
 inner join cepoz.concept c2 on c2.concept_id = o.value_coded and c2.uuid in ('36d6616b-8c7c-4768-9f38-2be4b704fccd', 'f4ee3bcc-947c-4390-9190-a335c2cd5868')
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       dst
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'dst', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then '"Sensible à la Rifampicine'
    when o.value_coded =162203 then 'Résistant à la rifampicine'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and concept_id =159984
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '224546bb-03da-4793-9ef6-3d42adc5b353'
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       poids
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'poids', date(e.encounter_datetime), o.value_numeric, 'x',date(now())
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id = 5089
    inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '224546bb-03da-4793-9ef6-3d42adc5b353'
    where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       Obs: 67f6a83d-5aa2-4da5-950c-833dadfa8916
--       bacilloccopie
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'bacilloccopie', date(e.encounter_datetime),
    case
    when o.value_coded =664 then 'Négatif'
    when o.value_coded =1362 then 'Positif+'
    when o.value_coded =1363 then 'Positif++'
    when o.value_coded =1364 then 'Positif+++'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0 and o.concept_id =307
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '67f6a83d-5aa2-4da5-950c-833dadfa8916'
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'genxpert_bk', date(e.encounter_datetime),
    case
    when o.value_coded =1301 then 'Positif'
    when o.value_coded =1302 then 'Négatif'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '67f6a83d-5aa2-4da5-950c-833dadfa8916'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       genxpert_rif --       ps: Meme uuid que genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'genxpert_rif', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then 'Sensible'
    when o.value_coded =162203 then 'Resistant'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '67f6a83d-5aa2-4da5-950c-833dadfa8916'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       culture --       --       -A revoir: deuxieme uuid inexistant
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'culture', date(e.encounter_datetime),
    case
    when c2.uuid ='36d6616b-8c7c-4768-9f38-2be4b704fccd' then 'Positif pour MTB'
    when c2.uuid  = 'f4ee3bcc-947c-4390-9190-a335c2cd5868' then 'Positif pour non MTB'
    when o.value_coded  =664 then 'Négatif'
    when o.value_coded  =160008 then 'Contaminé'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0 and o.concept_id =159982
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '67f6a83d-5aa2-4da5-950c-833dadfa8916'
 inner join cepoz.concept c2 on c2.concept_id = o.value_coded and c2.uuid in ('36d6616b-8c7c-4768-9f38-2be4b704fccd', 'f4ee3bcc-947c-4390-9190-a335c2cd5868')
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       dst
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'dst', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then '"Sensible à la Rifampicine'
    when o.value_coded =162203 then 'Résistant à la rifampicine'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',date(now())
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and concept_id =159984
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '67f6a83d-5aa2-4da5-950c-833dadfa8916'
         where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       poids
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'poids', date(e.encounter_datetime), o.value_numeric, 'x',date(now())
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id = 5089
    inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '67f6a83d-5aa2-4da5-950c-833dadfa8916'
    where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       MÉDICAMENTS : ALLERGIES  --       A revoir
--       medicaments_allergies_aucun
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'medicaments_allergies_aucun', date(e.encounter_datetime), o.value_coded, 'x',date(now())
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id = 160643  and o.encounter_id = e.encounter_id and o.voided=0 and o.value_coded =1107
    where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--       date_prochaine_visite
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'date_prochaine_visite', date(e.encounter_datetime), o.value_datetime, 'x',date(now())
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1 and e.voided=0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5096 and o.encounter_id = e.encounter_id and o.voided=0
    where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- *******************************************************************************************************
