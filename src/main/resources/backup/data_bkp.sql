-- consolidated isante database
-- itech
-- form: Registration
-- param: Nom
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, 'Registration', e.patientID, p.clinicPatientID, 'Nom', e.createDate, if(p.lname is null, '', p.lname), 'x', date(now())
       from itech.encounter e, itech.patient p
      where e.patientID = p.patientID
        and e.encounterType in (10, 15)
        and e.siteCode = 95698
        and e.encStatus < 255
        and p.patStatus <255
        and ifnull(p.lname,'')<>''
        on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Registration
-- param: Nom
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Registration', pid.identifier as patient_id, id.identifier, 'Nom', e.date_created, if(pn.family_name is null, '', pn.family_name) as 'nom', 'x', date(now())
       from cepoz.person_name pn
 inner join cepoz.patient p on p.patient_id = pn.person_id and p.voided=0
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id  left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '873f968a-73a8-4f9c-ac78-9f4778b751b6' and e.voided =0
      where pn.voided=0
     on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Registration
-- param: Prenom
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, 'Registration', e.patientID, p.clinicPatientID, 'Prenom', e.createDate, if(p.fname is null, '', p.fname), 'x', date(now())
       from itech.encounter e, itech.patient p
      where e.patientID = p.patientID
        and e.encounterType in(10, 15)
        and e.siteCode = 95698
        and e.encStatus < 255
        and p.patStatus <255
        and ifnull(p.fname,'')<>''
         on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Registration
-- param: Prenom
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Registration', pid.identifier as patient_id, id.identifier, 'Prenom', e.date_created, if(pn.given_name is null, '', pn.given_name) as 'prenom', 'x', date(now())
       from cepoz.person_name pn
 inner join cepoz.patient p on p.patient_id = pn.person_id and p.voided=0
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '873f968a-73a8-4f9c-ac78-9f4778b751b6' and e.voided =0
      where pn.voided=0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Registration
-- param:  St Code
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, 'Registration', e.patientID, p.clinicPatientID, 'St Code', e.createDate, if(p.clinicPatientID is null, '', p.clinicPatientID), 'x', date(now())
       from itech.encounter e, itech.patient p
      where e.patientID = p.patientID
        and e.encounterType in (10, 15)
        and e.siteCode = 95698
        and e.encStatus < 255
        and p.patStatus <255
        and ifnull(p.clinicPatientID,'')<>''
         on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Registration
-- param: St Code
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Registration', pid.identifier as patient_id, id.identifier, 'St Code', e.date_created, if(id.identifier is null, '', id.identifier) as 'st_code', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '873f968a-73a8-4f9c-ac78-9f4778b751b6' and e.voided =0
      where p.voided=0
        and id.identifier is not null
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Registration
-- param: sexe
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, 'Registration', e.patientID, p.clinicPatientID, 'Sexe', e.createDate, if(p.sex is null, '',
  case
       when p.sex = 2 then 'M'
       when p.sex = 1 then 'F'
       else 'U'
  end), 'x', date(now())
       from itech.encounter e, itech.patient p
      where e.patientID = p.patientID
        and e.encounterType in (10, 15)
        and e.siteCode = 95698
        and e.encStatus < 255
        and p.patStatus <255
        and ifnull(p.sex,'')<>''
         on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Registration
-- param: Sexe
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Registration', pid.identifier as patient_id, id.identifier, 'Sexe', e.date_created, if(pers.gender is null, '', pers.gender) as 'sexe', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.person pers on pers.person_id = p.patient_id and pers.voided=0
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Registration
-- param: Code National
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, 'Registration', e.patientID, p.clinicPatientID, 'Code National', e.createDate, if(p.nationalID is null, '', p.nationalID), 'x', date(now())
       from itech.encounter e, itech.patient p
      where e.patientID = p.patientID
        and e.encounterType in (10, 15)
        and e.siteCode = 95698
        and e.encStatus < 255
        and p.patStatus <255
        and ifnull(p.nationalID,'')<>''
         on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Registration
-- param: Code National
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Registration', pid.identifier as patient_id, id.identifier, 'Code National', e.date_created, if(pi.identifier is null, '', pi.identifier) as 'code_national', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.patient_identifier pi on pi.patient_id = p.patient_id and pi.identifier_type = 4
 inner join cepoz.encounter e on e.patient_id = p.patient_id     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '873f968a-73a8-4f9c-ac78-9f4778b751b6' and e.voided =0
      where p.voided=0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Registration
-- param: telephone
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, 'Registration', e.patientID, p.clinicPatientID, 'Telephone', e.createDate, if(p.telephone is null, '', p.telephone), 'x', date(now())
       from itech.encounter e, itech.patient p
      where e.patientID = p.patientID
        and e.encounterType in (10, 15)
        and e.siteCode = 95698
        and e.encStatus < 255
        and p.patStatus <255
        and ifnull(p.telephone,'')<>''
         on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Registration
-- param: telephone
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Registration', pid.identifier as patient_id, id.identifier, 'Telephone', e.date_created, if(pa.value is null,'', pa.value) as 'telephone', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.person_attribute pa on pa.person_id = p.patient_id and pa.person_attribute_type_id =8 and pa.voided=0
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Registration
-- param: Date de Visite
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, 'Registration', e.patientID, p.clinicPatientID, 'Date de Visite', e.createDate, date(concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)), 'x', date(now())
       from itech.encounter e, itech.patient p
      where e.patientID = p.patientID
        and e.encounterType in (10, 15)
        and e.siteCode = 95698
        and e.encStatus < 255
        and p.patStatus <255
         on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Registration
-- param: Date de Visite
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Registration', pid.identifier as patient_id, id.identifier, 'Date de Visite', e.date_created, if(e.encounter_datetime is null, '',date(e.encounter_datetime)) as 'date_visite', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Registration
-- param: Statut Marital
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, 'Registration', e.patientID, p.clinicPatientID, 'Statut Marital', e.createDate, if(p.maritalStatus is null, '',
  case
       when p.maritalStatus = 1 then 'Marie (e)'
       when p.maritalStatus = 2 then 'Concubinage'
       when p.maritalStatus = 4 then 'Veuf (ve)'
       when p.maritalStatus = 8 then 'Separe (e)'
       when p.maritalStatus = 16 then 'Celibataire'
       when p.maritalStatus = 32 then 'Inconnu'
       else ''
  end) as 'Statut Marital', 'x', date(now())
       from itech.encounter e, itech.patient p
      where e.patientID = p.patientID
        and e.encounterType in (10, 15)
        and e.siteCode = 95698
        and e.encStatus < 255
        and p.patStatus <255
        and ifnull(p.maritalStatus,'')<>''
         on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Registration
-- param: Statut Marital
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Registration', pid.identifier as patient_id, id.identifier, 'Statut Marital', e.date_created, if(o.value_coded is null, '',
  case
       when o.value_coded =5555 then 'Marie (e)'
       when o.value_coded =1060 then 'Concubinage'
       when o.value_coded =1059 then 'Veuf (ve)'
       when o.value_coded =1056 then 'Separe (e)'
       when o.value_coded =1058 then 'divorce'
       when o.value_coded =1057 then 'Celibataire'
       when o.value_coded =1067 then 'Inconnu'
       when o.value_coded =5622 then 'Autre'
       else ''
  end) as 'statut_marital','x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '873f968a-73a8-4f9c-ac78-9f4778b751b6' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id = 1054 and o.encounter_id = e.encounter_id and o.voided =0
      where p.voided=0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Registration
-- param: Date de Naissance
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, 'Registration', e.patientID, p.clinicPatientID, 'Date de Naissance', e.createDate,
         if( ifnull(p.dobYy,'')='' and ifnull(p.dobMm,'')='' and ifnull(p.dobDd,'')='', '', date(concat(p.dobYy,'-', p.dobMm,'-', p.dobDd))), 'x', date(now())
       from itech.encounter e, itech.patient p
      where e.patientID = p.patientID
        and e.encounterType in (10, 15)
        and e.siteCode = 95698
        and e.encStatus < 255
        and p.patStatus <255
         on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Registration
-- param: Date de Naissance
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Registration', pid.identifier as patient_id, id.identifier, 'Date de Naissance', e.date_created, if(pers.birthdate is null, '', pers.birthdate) as 'date_de_naissance', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.person pers on pers.person_id = p.patient_id and pers.voided=0
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Registration
-- param: Adresse
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Registration", e.patientID, p.clinicPatientID, 'Adresse',  e.createDate, if(p.addrDistrict is null, '', p.addrDistrict) as 'adresse', 'x', date(now())
       from itech.encounter e, itech.patient p
      where e.patientID = p.patientID
	    and e.encounterType in (10, 15)
        and e.siteCode = 95698
        and e.encStatus < 255
        and p.patStatus <255
        and ifnull(p.addrDistrict,'')<>''
         on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Registration
-- param: Adresse
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Registration', pid.identifier as patient_id, id.identifier, 'Adresse', e.date_created, if(paddr.address2 is null, '', paddr.address2) as 'adresse','x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.person_address paddr on paddr.person_id = p.patient_id
 inner join cepoz.encounter e on e.patient_id = p.patient_id     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '873f968a-73a8-4f9c-ac78-9f4778b751b6' and e.voided =0
      where p.voided=0
        and paddr.address2 is not null
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Registration
-- param: Code  PC
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, 'Registration', e.patientID, p.clinicPatientID, 'Code PC', e.createDate, if(o.value_text is null, '', o.value_text), 'x', date(now())
       from itech.encounter e, itech.patient p, itech.obs o
      where e.patientID = p.patientID
        and e.encounterType in (10, 15)
        and e.encounter_id = o.encounter_id
        and o.concept_id = 70039
        and e.siteCode = 95698
        and e.encStatus < 255
        and p.patStatus <255
        and ifnull(o.value_text,'')<>''
         on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Registration
-- param: Code PC
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Registration', pid.identifier as patient_id, id.identifier, 'Code PC', e.date_created, if(cp.identifier is null,'', cp.identifier) as 'code_pc', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.patient_identifier cp on cp.patient_id = p.patient_id and cp.identifier_type = 8
 inner join cepoz.encounter e on e.patient_id = p.patient_id     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '873f968a-73a8-4f9c-ac78-9f4778b751b6' and e.voided =0
      where p.voided=0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- -----------------------------------------------------------------------------------------Fiche Laboratory --------------------------------------------------------------------------------
-- consolidated iSante database
-- itech
-- form: Laboratory
-- param: Date de visite
-- site code: 95698
delete from replication.validation where form_origin='Laboratory' and parameter='Date de visite' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, 'Laboratory', e.patientID, p.clinicPatientID, 'Date de visite', e.createDate, date(concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)), 'x', date(now())
	   from itech.encounter e, itech.patient p
	  where e.patientID = p.patientID
	    and e.encounterType in (6, 19)
		and e.siteCode = 95698
        and e.encStatus < 255
		  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Laboratory
-- param: Date de visite
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Laboratory', pid.identifier as patient_id, id.identifier, 'Date de visite', e.date_created, date(e.encounter_datetime), 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'f037e97b-471e-4898-a07c-b8e169e0ddc4' and e.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- consolidated iSante database
-- itech
-- form: Laboratory
-- param: Resultat de charge virale
-- site code: 95698
delete from replication.validation where form_origin='Laboratory' and parameter='Resultat de charge virale' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, 'Laboratory', e.patientID, p.clinicPatientID, 'Resultat de charge virale', e.createDate, l.result, 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.labs l
	  where e.patientID = p.patientID
		and e.encounterType in (6, 19)
		and e.patientID = l.patientID
		and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(l.visitDateYy,'-',l.visitDateMm,'-',l.visitDateDd)
		and e.seqnum=l.seqnum
		and l.labID in (103, 1257)
		and e.siteCode = 95698
        and e.encStatus < 255
        and ifnull(l.result, '')<>''
		  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Laboratory
-- param: Resultat de charge virale
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Laboratory', pid.identifier as patient_id, id.identifier, 'Resultat de charge virale', e.date_created,
case
       when o.value_coded  = 1306 then 'AU-DELÀ DE LA LIMITE DeTECTABLE'
       when o.value_coded  = 1301 then 'DeTECTe'
       when o.concept_id = 856 and o.value_numeric is not null then o.value_numeric
	   else ''
end, 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'f037e97b-471e-4898-a07c-b8e169e0ddc4' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.voided=0
    inner join cepoz.concept c on c.concept_id = o.concept_id and  c.concept_id in (856, 1305)
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- consolidated isante database
-- itech
-- form: Laboratory
-- param: Date de resultat charge virale
-- site code: 95698
delete from replication.validation where form_origin='Laboratory' and parameter='Resultat de charge virale' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, 'Laboratory', e.patientID, p.clinicPatientID, 'Date de resultat charge virale', e.createDate,
	  if( ifnull(l.resultDateYy,'')='' and ifnull(l.resultDateMm,'')='' and ifnull(l.resultDateDd,'')='', '', date(concat(l.resultDateYy,'-', l.resultDateMm,'-', l.resultDateDd)))
	as date_resultat , 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.labs l
	  where e.patientID = p.patientID
		and e.encounterType in (6, 19)
		and e.patientID = l.patientID
		and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(l.visitDateYy,'-',l.visitDateMm,'-',l.visitDateDd)
		and e.seqnum=l.seqnum
		and l.labID in (103,1257)
        and e.encStatus < 255
		and ifnull(l.result,0)>0
		and e.siteCode = 95698
		  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Laboratory
-- param: Date de resultat charge virale
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Laboratory', pid.identifier as patient_id, id.identifier, 'Date de resultat charge virale', e.date_created, date(o.obs_datetime), 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'f037e97b-471e-4898-a07c-b8e169e0ddc4' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =856 and o.voided=0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- consolidated isante database
-- itech
-- form: Laboratory
-- param: Resultat PCR
-- site code: 95698
delete from replication.validation where form_origin='Laboratory' and parameter='Resultat PCR' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, 'Laboratory', e.patientID, p.clinicPatientID, 'Resultat PCR', e.createDate, l.result, 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.labs l
	  where e.patientID = p.patientID
		and e.encounterType in (6, 19)
		and e.patientID = l.patientID
		and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(l.visitDateYy,'-',l.visitDateMm,'-',l.visitDateDd)
		and e.seqnum=l.seqnum
		and l.labID = 181
		and ifnull(l.result,0)<>''
		and e.siteCode = 95698
        and e.encStatus < 255
		  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Laboratory
-- param: Resultat PCR
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Laboratory', pid.identifier as patient_id, id.identifier, 'Resultat de PCR', e.date_created,
case
       when o.value_coded = 1301 then 'Detecte'
       when o.value_coded = 1302 then 'Non-detecte'
       when o.value_coded = 1300 then 'Equivoque'
       when o.value_coded = 1304 then 'Echantillon de pauvre qualite'
	   else ''
end, 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'f037e97b-471e-4898-a07c-b8e169e0ddc4' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =844 and o.voided=0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- Consolidated isante database
-- itech
-- form: Laboratory
-- param: Date de resultat PCR
-- site code: 95698
delete from replication.validation where form_origin='Laboratory' and parameter='Date de resultat PCR' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, 'Laboratory', e.patientID, p.clinicPatientID, 'Date de resultat PCR', e.createDate,  date(concat(l.resultDateYy,'-',l.resultDateMm,'-',l.resultDateDd))
	     as dateResultat , 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.labs l
	  where e.patientID = p.patientID
		and e.encounterType in (6, 19)
		and e.patientID = l.patientID
		and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(l.visitDateYy,'-',l.visitDateMm,'-',l.visitDateDd)
		and e.seqnum=l.seqnum
		and l.labID = 181
		and e.siteCode = 95698
        and e.encStatus < 255
		  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Laboratory
-- param: Date de resultat PCR
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Laboratory', pid.identifier as patient_id, id.identifier, 'Date de resultat PCR', e.date_created, date(o.obs_datetime), 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'f037e97b-471e-4898-a07c-b8e169e0ddc4' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id = 844 and o.voided=0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- --------------------------------------------------------------------------Fiche Adherence Counseling-------------------------------------------------------------------
-- isante database
-- itech
-- form: Adherence Counseling
-- param: Date de Visite
-- site code: 95698
delete from replication.validation where site_id = 95698 and form_origin = 'Adherence Counseling' and parameter = 'Date de Visite' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, 'Adherence Counseling', e.patientID, p.clinicPatientID, 'Date de Visite', e.createDate, date(concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)), 'x', date(now())
       from itech.encounter e, itech.patient p
      where e.patientID = p.patientID
        and e.encounterType in (14, 20)
        and e.siteCode = 95698
        and e.encStatus < 255
        on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Adherence Counseling
-- param: Date de Visite
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Adherence Counseling', pid.identifier as patient_id, id.identifier, 'Date de Visite', e.date_created, date(e.encounter_datetime), 'x',date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'c45d7299-ad08-4cb5-8e5d-e0ce40532939' and e.voided =0
      where  p.voided =0
        on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);



-- isante consolidated database
-- cepoz
-- form: Adherence Counseling
-- param: Medicament non-disponible à la clinique
-- site code: 95698
delete from replication.validation where site_id = 95698 and form_origin = 'Adherence Counseling' and parameter = 'Medicament non-disponible à la clinique';

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Adherence Counseling", p.patientID, p.clinicPatientID, 'Medicament non-disponible à la clinique', e.createDate, 'oui' as 'medocs_non_dispo_dans_clinique', 'x',date(now())
       from itech.encounter e,itech.adherenceCounseling ac, itech.patient p
      where e.siteCode = ac.siteCode
	    and e.patientID = ac.patientID
	    and e.patientID = p.patientID
	    and concat(e.visitDateYy,"-",e.visitDateMm,"-",e.visitDateDd) =	concat(ac.visitDateYy,"-",ac.visitDateMm,"-",ac.visitDateDd)
	    and e.seqNum=ac.seqNum
	    and e.siteCode=95698
        and e.encStatus < 255
	    and ac.reasonNotAvail=1
        on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Adherence Counseling
-- param: Medicament non-disponible à la clinique
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Adherence Counseling", pid.identifier as patient_id, id.identifier, 'Medicament non-disponible à la clinique', e.date_created, 'oui' as 'medocs_non_dispo_dans_clinique', 'x',date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'c45d7299-ad08-4cb5-8e5d-e0ce40532939' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160582 and o.encounter_id = e.encounter_id and o.value_coded =1754 and o.voided =0
      where p.voided =0
        on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante database
-- itech
-- form: Adherence Counseling
-- param: 'Pourcentage de doses pour le mois dernier'
-- site code: 95698
delete from replication.validation where site_id = 95698 and form_origin = 'Adherence Counseling' and parameter = 'Pourcentage de doses pour le mois dernier' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, 'Adherence Counseling', e.patientID as patient_id, p.clinicPatientID, 'Pourcentage de doses pour le mois dernier', e.createDate,
  case
      when ac.doseProp =1 then '0%'
      when ac.doseProp =2 then '10%'
      when ac.doseProp =4 then '20%'
      when ac.doseProp =8 then '30%'
      when ac.doseProp =16 then '40%'
      when ac.doseProp =32 then '50%'
      when ac.doseProp =64 then '60%'
      when ac.doseProp =128 then '70%'
      when ac.doseProp =256 then '80%'
      when ac.doseProp =512 then '90%'
      when ac.doseProp =1024 then '100%'
      else ''
  end as 'pourcentage_dose', 'x',date(now())
      from itech.patient p, itech.encounter e,itech.adherenceCounseling ac
     where p.patientID = e.patientID
       and e.siteCode = ac.siteCode
       and e.patientID = ac.patientID
       and e.siteCode=95698
       and e.encounterType in (14, 20)
       and concat(e.visitDateYy,"-",e.visitDateMm,"-",e.visitDateDd) =	concat(ac.visitDateYy,"-",ac.visitDateMm,"-",ac.visitDateDd)
       and ac.doseProp IN(1,2,4,8,16,32,64,128,256,512,1024)
        on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Adherence Counseling
-- param: Pourcentage de doses pour le mois dernier
-- site code: 95698

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Adherence Counseling', pid.identifier as patient_id, id.identifier, 'Pourcentage de doses pour le mois dernier', e.date_created,
 case
       when o.value_numeric =0 then '0%'
       when o.value_numeric =5 then '5%'
       when o.value_numeric =10 then '10%'
       when o.value_numeric =15 then '15%'
       when o.value_numeric =20 then '20%'
       when o.value_numeric =25 then '25%'
       when o.value_numeric =30 then '30%'
       when o.value_numeric =35 then '35%'
       when o.value_numeric =40 then '40%'
       when o.value_numeric =45 then '45%'
       when o.value_numeric =50 then '50%'
       when o.value_numeric =55 then '55%'
       when o.value_numeric =60 then '60%'
       when o.value_numeric =65 then '65%'
       when o.value_numeric =70 then '70%'
       when o.value_numeric =75 then '75%'
       when o.value_numeric =80 then '80%'
       when o.value_numeric =85 then '85%'
       when o.value_numeric =90 then '90%'
       when o.value_numeric =95 then '95%'
       when o.value_numeric =100 then '100%'
       else ''
end as 'pourcentage_dose', 'x',date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'c45d7299-ad08-4cb5-8e5d-e0ce40532939' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =163710 and o.encounter_id = e.encounter_id and o.voided =0
      where p.voided =0
        on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante consolidated database
-- cepoz
-- form: Adherence Counseling
-- param: A Oublie
-- site code: 95698
delete from replication.validation where site_id = 95698 and form_origin = 'Adherence Counseling' and parameter = 'A Oublie' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Adherence Counseling", e.patientID, p.clinicPatientID, 'A Oublie', e.createDate, 'oui', 'x', date(now())
      from itech.patient p, itech.encounter e, itech.adherenceCounseling ac
	 where p.patientID = e.patientID
	   and e.siteCode = ac.siteCode
	   and e.patientID = ac.patientID
	   and e.seqNum=ac.seqNum
	   and e.siteCode=95698
	   and concat(e.visitDateYy,"-",e.visitDateMm,"-",e.visitDateDd)=concat(ac.visitDateYy,"-",ac.visitDateMm,"-",ac.visitDateDd)
	   and ac.reasonForgot=1
	   and e.encStatus < 255
        on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Adherence Counseling
-- param: A Oublie
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Adherence Counseling", pid.identifier as patient_id, id.identifier, 'A Oublie', e.date_created, 'oui', 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
     inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'c45d7299-ad08-4cb5-8e5d-e0ce40532939' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160582 and o.encounter_id = e.encounter_id and o.value_coded =160587 and o.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante consolidated database
-- cepoz
-- form: Adherence Counseling
-- param: Effets secondaires
-- site code: 95698
delete from replication.validation where site_id = 95698 and form_origin = 'Adherence Counseling' and parameter = 'Effets Secondaires' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, "Adherence Counseling", e.patientID, p.clinicPatientID, 'Effets Secondaires', e.createDate, 'oui', 'x', date(now())
       from itech.patient p, itech.encounter e, itech.adherenceCounseling ac
	  where p.patientID = e.patientID
		and e.siteCode = ac.siteCode
		and e.patientID = ac.patientID
		and e.seqNum=ac.seqNum
		and e.siteCode=95698
		and concat(e.visitDateYy,"-",e.visitDateMm,"-",e.visitDateDd) =	concat(ac.visitDateYy,"-",ac.visitDateMm,"-",ac.visitDateDd)
		and ac.reasonSideEff=1
		and e.encStatus < 255
         on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Adherence Counseling
-- param: Effets secondaires
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Adherence Counseling", pid.identifier as patient_id, id.identifier, 'Effets Secondaires', e.date_created,'oui', 'x',date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'c45d7299-ad08-4cb5-8e5d-e0ce40532939' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160582 and o.encounter_id = e.encounter_id and o.value_coded =1778 and o.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante consolidated database
-- cepoz
-- form: Adherence Counseling
-- param: Emprisonne
-- site code: 95698
delete from replication.validation where site_id = 95698 and form_origin = 'Adherence Counseling' and parameter = 'Emprisonne' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, "Adherence Counseling", e.patientID, p.clinicPatientID, 'Emprisonne', e.createDate, 'oui', 'x', date(now())
       from itech.patient p, itech.encounter e, itech.adherenceCounseling ac
	  where p.patientID = e.patientID
		and e.siteCode = ac.siteCode
		and e.patientID = ac.patientID
		and e.seqNum=ac.seqNum
		and e.siteCode=95698
		and concat(e.visitDateYy,"-",e.visitDateMm,"-",e.visitDateDd) =	concat(ac.visitDateYy,"-",ac.visitDateMm,"-",ac.visitDateDd)
		and ac.reasonPrison=1
		and e.encStatus < 255
         on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Adherence Counseling
-- param: Emprisonne
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Adherence Counseling", pid.identifier as patient_id, id.identifier, 'Emprisonne',e.date_created,'oui', 'x',date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'c45d7299-ad08-4cb5-8e5d-e0ce40532939' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160582 and o.encounter_id = e.encounter_id and o.value_coded =156761 and o.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante consolidated database
-- cepoz
-- form: Adherence Counseling
-- param: S'est senti trop malade
-- site code: 95698
delete from replication.validation where site_id = 95698 and form_origin = 'Adherence Counseling' and parameter = "S'est senti trop malade" and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, "Adherence Counseling", e.patientID, p.clinicPatientID, "S'est senti trop malade", e.createDate, 'oui', 'x', date(now())
       from itech.patient p, itech.encounter e, itech.adherenceCounseling ac
	  where p.patientID = e.patientID
		and e.siteCode = ac.siteCode
		and e.patientID = ac.patientID
		and e.seqNum=ac.seqNum
		and e.siteCode=95698
		and concat(e.visitDateYy,"-",e.visitDateMm,"-",e.visitDateDd) =	concat(ac.visitDateYy,"-",ac.visitDateMm,"-",ac.visitDateDd)
		and ac.reasonTooSick=1
		and e.encStatus < 255
         on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Adherence Counseling
-- param: S'est senti trop malade
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Adherence Counseling", pid.identifier as patient_id, id.identifier, "S'est senti trop malade", e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'c45d7299-ad08-4cb5-8e5d-e0ce40532939' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160582 and o.encounter_id = e.encounter_id and o.value_coded =160585 and o.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante consolidated database
-- cepoz
-- form: Adherence Counseling
-- param: Gêne de prendre des medicaments en presence d'autres personnes
-- site code: 95698
delete from replication.validation where site_id = 95698 and form_origin = 'Adherence Counseling' and parameter = "Gêne de prendre des medicaments en presence d'autres personnes" and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, "Adherence Counseling", e.patientID, p.clinicPatientID, "Gêne de prendre des medicaments en presence d'autres personnes", e.createDate, 'oui', 'x', date(now())
      from itech.patient p, itech.encounter e, itech.adherenceCounseling ac
	 WHERE p.patientID = e.patientID
	   and e.siteCode = ac.siteCode
	   and e.patientID = ac.patientID
	   and e.seqNum=ac.seqNum
	   and e.siteCode=95698
	   and e.encStatus < 255
	   and concat(e.visitDateYy,"-",e.visitDateMm,"-",e.visitDateDd) =	concat(ac.visitDateYy,"-",ac.visitDateMm,"-",ac.visitDateDd)
	   and ac.reasonNotComf=1
        on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Adherence Counseling
-- param: Gêne de prendre des medicaments en presence d'autres personnes
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Adherence Counseling", pid.identifier as patient_id, id.identifier, "Gêne de prendre des medicaments en presence d'autres personnes", e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'c45d7299-ad08-4cb5-8e5d-e0ce40532939' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160582 and o.encounter_id = e.encounter_id and o.value_coded =160589 and o.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante consolidated database
-- cepoz
-- form: Adherence Counseling '
-- param: Difficultes à avaler
-- site code: 95698
delete from replication.validation where site_id = 95698 and form_origin = 'Adherence Counseling' and parameter = 'Difficultes à Avaler' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, "Adherence Counseling", e.patientID, p.clinicPatientID, 'Difficultes à Avaler', e.createDate, 'oui', 'x', date(now())
       from itech.patient p, itech.encounter e, itech.adherenceCounseling ac
	  where p.patientID = e.patientID
		and e.siteCode = ac.siteCode
		and e.patientID = ac.patientID
		and e.seqNum=ac.seqNum
		and e.siteCode=95698
		and e.encStatus < 255
		and concat(e.visitDateYy,"-",e.visitDateMm,"-",e.visitDateDd) =	concat(ac.visitDateYy,"-",ac.visitDateMm,"-",ac.visitDateDd)
		and ac.reasonNoSwallow=1
         on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);


-- isanteplus database
-- cepoz
-- form: Adherence Counseling
-- param: Difficultes à avaler
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Adherence Counseling", pid.identifier as patient_id, id.identifier, 'Difficultes à Avaler', e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'c45d7299-ad08-4cb5-8e5d-e0ce40532939' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160582 and o.encounter_id = e.encounter_id and o.value_coded =5954 and o.voided =0
        where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante consolidated database
-- cepoz
-- form: Adherence Counseling
-- param: En voyage
-- site code: 95698
delete from replication.validation where site_id = 95698 and form_origin = 'Adherence Counseling' and parameter = 'En Voyage' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Adherence Counseling", e.patientID, p.clinicPatientID, 'En Voyage', e.createDate, 'oui', 'x', date(now())
       from itech.patient p, itech.encounter e, itech.adherenceCounseling ac
	  where p.patientID = e.patientID
		and e.siteCode = ac.siteCode
		and e.patientID = ac.patientID
		and e.seqNum=ac.seqNum
		and e.siteCode=95698
		and e.encStatus < 255
		and concat(e.visitDateYy,"-",e.visitDateMm,"-",e.visitDateDd) =	concat(ac.visitDateYy,"-",ac.visitDateMm,"-",ac.visitDateDd)
		and ac.reasonTravel=1
         on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Adherence Counseling
-- param: En voyage
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Adherence Counseling", pid.identifier as patient_id, id.identifier, 'En Voyage', e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
        left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
        left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
        inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
        inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
        inner join cepoz.encounter e on e.patient_id = p.patient_id
        inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'c45d7299-ad08-4cb5-8e5d-e0ce40532939' and e.voided =0
        inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160582 and o.encounter_id = e.encounter_id and o.value_coded =124153 and o.voided =0
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante consolidated database
-- cepoz
-- form: Adherence Counseling
-- param: Manque de nourriture
-- site code: 95698
delete from replication.validation where site_id = 95698 and form_origin = 'Adherence Counseling' and parameter = 'Manque de Nourriture' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, "Adherence Counseling", e.patientID, p.clinicPatientID, 'Manque de Nourriture', e.createDate, 'oui', 'x', date(now())
       from itech.patient p, itech.encounter e, itech.adherenceCounseling ac
	  where p.patientID = e.patientID
		and e.siteCode = ac.siteCode
		and e.patientID = ac.patientID
		and e.seqNum=ac.seqNum
		and e.siteCode=95698
		and e.encStatus < 255
		and concat(e.visitDateYy,"-",e.visitDateMm,"-",e.visitDateDd) =	concat(ac.visitDateYy,"-",ac.visitDateMm,"-",ac.visitDateDd)
		and ac.reasonNoFood=1
         on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);


-- isanteplus database
-- cepoz
-- form: Adherence Counseling
-- param: Manque de nourriture
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Adherence Counseling", pid.identifier as patient_id, id.identifier, 'Manque de Nourriture', e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'c45d7299-ad08-4cb5-8e5d-e0ce40532939' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160582 and o.encounter_id = e.encounter_id and o.value_coded =119533 and o.voided =0
      where p.voided =0
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
                when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
                when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
                else 'x'
                end);


-- --------------------------------------------------------Fiche VIH premiere visite--------------------------------------------
-- consolidated isante database
-- itech
-- form: Intake
-- param: Temperature
-- site code: 95698

delete from replication.validation where form_origin='Intake' and parameter='Temperature' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, 'Intake', e.patientID, p.clinicPatientID, 'Temperature', e.createDate, if(v.vitalTemp is null, '',
  case
       when v.vitalTempUnits =1 then if (vitalTemp <> '', convert(vitalTemp, decimal(10, 2)), vitalTemp)
       when v.vitalTempUnits =2 then round((vitalTemp-32) * 5/9, 2)
       else ''
  end), 'x', date(now())
       from itech.encounter e, itech.patient p, itech.vitals v
      where e.patientID = p.patientID
        and e.encounterType=1
        and e.patientID = v.patientID
        and e.encStatus < 255
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
        and e.seqnum=v.seqnum
        and e.siteCode = 95698
        and v.vitalTempUnits in (1, 2)
        on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Temperature
-- site code: 95698

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
    select 95698, 'Intake', pid.identifier as patient_id, id.identifier, 'Temperature', e.date_created, if(o.value_numeric is null, '', convert(o.value_numeric,decimal(10,2))), 'x',date (now())
        from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5088 and o.encounter_id = e.encounter_id and o.voided =0
    where p.voided =0
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);
-- consolidated isante database
-- itech
-- form: Intake
-- param: TA Systolique
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='TA Systolique' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Intake", e.patientID, p.clinicPatientID, 'TA Systolique', e.createDate, if(vitalBp1 is null, '',
  case
       when vitalBPUnits = 1 then if (vitalBp1 <>'', convert(vitalBp1 * 10, decimal(10, 2)), vitalBp1)
       when vitalBPUnits = 2 then if (vitalBp1 <>'', convert(vitalBp1, decimal(10, 2)), vitalBp1)
       else ''
  end), 'x', date(now())
       from itech.encounter e, itech.patient p, itech.vitals v
      where e.patientID = p.patientID
        and e.encounterType=1
        and e.encStatus < 255
        and e.patientID = v.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
        and e.seqnum=v.seqnum
        and e.siteCode = 95698
        and v.vitalBPUnits in (1, 2)
        and ifnull(v.vitalBp1, '') <> ''
           on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: TA Systolique
-- site code: 95698
-- a reverifier, pourquoi isanteplus met null

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Intake', pid.identifier as patient_id, id.identifier, 'TA Systolique', e.date_created, convert(o.value_numeric, decimal(10, 2)),'x',date (now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5085 and o.encounter_id = e.encounter_id and o.voided =0 and o.value_numeric is not null
      where p.voided =0
     on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Intake
-- param: TA Diastolique
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='TA Diastolique' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Intake", e.patientID, p.clinicPatientID, 'TA Diastolique', e.createDate, if(vitalBp2 is null, '',
case
       when vitalBPUnits = 1 then if (vitalBp2 <>'', convert(vitalBp2 * 10, decimal(10, 2)), vitalBp2)
       when vitalBPUnits = 2 then if (vitalBp2 <>'', convert(vitalBp2, decimal(10, 2)), vitalBp2)
       else ''
 end), 'x', date(now())
       from itech.encounter e, itech.patient p, itech.vitals v
      where e.patientID = p.patientID
        and e.encounterType=1
        and e.encStatus < 255
        and e.patientID = v.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
        and e.seqnum=v.seqnum
        and e.siteCode = 95698
        and v.vitalBPUnits in (1, 2)
        and ifnull(v.vitalBp2, '') <> ''
           on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: TA Diastolique
-- site code: 95698
-- a reverifier, pourquoi isanteplus met null
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Intake', pid.identifier as patient_id, id.identifier, 'TA Diastolique', e.date_created, convert(o.value_numeric, decimal(10,2)) as 'ta_diastolique','x',date (now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5086 and o.encounter_id = e.encounter_id and o.voided =0  and o.value_numeric is not null
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param: Pools
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Pools' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Intake", e.patientID, p.clinicPatientID, 'Pools', e.createDate, if( v.vitalHr <> '', convert(v.vitalHr, decimal(10,2)), v.vitalHr), 'x', date(now())
       from itech.encounter e, itech.patient p, itech.vitals v
      where e.patientID = p.patientID
        and e.encounterType=1
        and e.encStatus < 255
        and e.patientID = v.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
        and e.seqnum=v.seqnum
        and e.siteCode =95698
        and ifnull(v.vitalHr, '') <> ''
           on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Pools
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Intake', pid.identifier as patient_id, id.identifier, 'Pools', e.date_created, if(o.value_numeric is null, '', convert(o.value_numeric, decimal(10, 2))), 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5087 and o.encounter_id = e.encounter_id and o.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param: Frequence Respiratoire
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Frequence Respiratoire' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Intake", e.patientID, p.clinicPatientID, 'Frequence Respiratoire', e.createDate,  if(vitalRr <>'', convert(vitalRr, decimal(10,2)),''), 'x', date(now())
       from itech.encounter e, itech.patient p, itech.vitals v
      where e.patientID = p.patientID
        and e.encounterType=1
        and e.encStatus < 255
        and e.patientID = v.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
        and e.seqnum=v.seqnum
        and e.siteCode =95698
        and ifnull(v.vitalRr,'') <> ''
           on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Frequence Respiratoire
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Intake', pid.identifier as patient_id, id.identifier, 'Frequence Respiratoire', e.date_created, convert(o.value_numeric, decimal(10,2)) as 'frequence_respiratoire', 'x',date (now())
       from cepoz.patient p
        left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
        left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
        inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
        inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
        inner join cepoz.encounter e on e.patient_id = p.patient_id
        inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
        inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5242 and o.encounter_id = e.encounter_id and o.voided =0
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante database
-- itech
-- form: Intake
-- param: Taille
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Taille' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, 'Intake', e.patientID, p.clinicPatientID, 'Taille', e.createDate, if(vitalHeightCm=0 or vitalHeightCm is null, '',
case
       when vitalHeight is not null and vitalHeightCm is null then vitalHeight
       when vitalHeight is not null and vitalHeightCm is not null then (vitalHeight + vitalHeightCm)
       when vitalHeight is null and vitalHeightCm is not null then vitalHeightCm
       else ''
end) , 'x', date(now())
       from itech.encounter e, itech.patient p, itech.vitals v
      where e.patientID = p.patientID
        and e.encounterType=1
        and e.encStatus < 255
        and e.patientID = v.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
        and e.seqnum=v.seqnum
        and e.siteCode =95698
        and ifnull(vitalHeightCm, 0)>0
           on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Taille
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Intake', pid.identifier as patient_id, id.identifier, 'Taille', e.date_created, o.value_numeric, 'x',date (now())
       from cepoz.patient p
        left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
        left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
        inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
        inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
        inner join cepoz.encounter e on e.patient_id = p.patient_id
        inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
        inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5090 and o.encounter_id = e.encounter_id and o.voided =0
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante database
-- itech
-- form: Intake
-- param: Poids
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Poids' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, 'Intake', e.patientID, p.clinicPatientID, 'Poids', e.createDate, if(vitalWeight is null, '',
 case
       when vitalWeightUnits =1 then if(vitalWeight <> '', convert(vitalWeight, decimal(10, 2)), vitalWeight)
       when vitalWeightUnits =2 then round(vitalWeight * 0.453592, 2)
       ELSE ''
 end), 'x', date(now())
       from itech.encounter e, itech.patient p, itech.vitals v
      where e.patientID = p.patientID
        and e.encounterType=1
        and e.encStatus < 255
        and e.patientID = v.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
        and e.seqnum=v.seqnum
        and e.siteCode =95698
        and v.vitalWeightUnits in (1, 2)
        and ifnull(v.vitalWeight, 0)>0
           on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Poids
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Intake', pid.identifier as patient_id, id.identifier, 'Poids', e.date_created, if(o.value_numeric <> '', convert(o.value_numeric , decimal(10, 2)), '' ), 'x',date (now())
       from cepoz.patient p
        left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
        left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
        inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
        inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
        inner join cepoz.encounter e on e.patient_id = p.patient_id
        inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
        inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5089 and o.encounter_id = e.encounter_id and o.voided =0
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante database
-- itech
-- form: Intake
-- param: Resultat VIH recu par le patient
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Resultat VIH reçu par le patient' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, 'Intake', e.patientID, p.clinicPatientID, 'Resultat VIH reçu par le patient', e.createDate, if(v.firstTestResultsReceived is  null, '',
 case
       when v.firstTestResultsReceived =1 then 'oui'
       when v.firstTestResultsReceived =2 then 'non'
       else ''
 end), 'x', date(now())
       from itech.encounter e, itech.patient p, itech.vitals v
      where e.patientID = p.patientID
        and e.encounterType=1
        and e.encStatus < 255
        and e.patientID = v.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
        and e.seqnum=v.seqnum
        and e.siteCode =95698
        and v.firstTestResultsReceived in (1, 2)
           on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Resultat VIH recu par le patient
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'Resultat VIH reçu par le patient', e.date_created,
  case
       when o.value_coded =1065 then 'oui'
       when o.value_coded =1066 then 'non'
       else ''
  end, 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =164848 and o.encounter_id = e.encounter_id and o.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);


-- isante database
-- itech
-- form: Intake
-- param: Grossesse actuelle
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Grossesse actuelle' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Intake", e.patientID, p.clinicPatientID, 'Grossesse actuelle', e.createDate, if(pregnant is null, '',
 case
       when pregnant =1 then 'oui'
       when pregnant =2 then 'non'
       when pregnant =4 then 'inconnu'
       else ''
 end), 'x', date(now())
       from itech.encounter e, itech.patient p, itech.vitals v
      where e.patientID = p.patientID
        and e.encounterType=1
        and e.encStatus < 255
        and e.patientID = v.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
        and e.seqnum=v.seqnum
        and e.siteCode =95698
        and v.pregnant in (1, 2)
           on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Grossesse actuelle
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'Grossesse actuelle', e.date_created,
  case
       when o.value_coded =1065 then 'oui'
       when o.value_coded =1066 then 'non'
       when o.value_coded =134346 then 'Menopause'
       when o.value_coded =1067 then 'inconnu'
       else ''
  end, 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5272 and o.encounter_id = e.encounter_id and o.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param: Methode utilisee pour le depistage du cancer du col
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Methode utilisee pour le depistage du cancer du col' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Intake", e.patientID, p.clinicPatientID, 'Methode utilisee pour le depistage du cancer du col', e.createDate, if(value_numeric is null, '',
 case
       when o.value_numeric =1 then 'Pap test'
       when o.value_numeric =2 then 'VIA'
       else ''
 end), 'x', date(now())
       from itech.encounter e, itech.patient p, itech.obs o
      where e.patientID = p.patientID
        and e.encounterType=1
        and e.encStatus < 255
        and o.encounter_id=e.encounter_id
        and o.concept_id=163589
        and e.siteCode =95698
        and o.value_numeric in (1, 2)
           on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);


-- isanteplus database
-- cepoz
-- form: Intake
-- param: Methode utilisee pour le depistage du cancer du col
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'Methode utilisee pour le depistage du cancer du col', e.date_created,
  case
       when o.value_coded =885 then 'Pap test'
       when o.value_coded =162816 then 'VIA'
       else ''
  end, 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =163589 and o.encounter_id = e.encounter_id and o.voided=0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param: Resultat du test du cancer du col
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Resultat du test du cancer du col' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Intake", e.patientID, p.clinicPatientID, 'Resultat du test du cancer du col', e.createDate, if(value_numeric is null, '',
 case
       when o.value_numeric =1 then 'Normal'
       when o.value_numeric =2 then 'Anormal'
       when o.value_numeric =4 then 'Inconnu'
       else ''
 end), 'x', date(now())
       from itech.encounter e, itech.patient p, itech.obs o
      where e.patientID = p.patientID
        and e.encounterType=1
        and e.encStatus < 255
        and o.encounter_id=e.encounter_id
        and o.concept_id=70029
        and e.siteCode =95698
        and o.value_numeric in (1, 2, 4)
           on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Resultat du test du cancer du col
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'Resultat du test du cancer du col', e.date_created,
case
       when o.value_coded =1115 then 'Normal'
       when o.value_coded =1116 then 'Anormal'
       when o.value_coded =1067 then 'Inconnu'
       else ''
  end, 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160704 and o.encounter_id = e.encounter_id and o.voided=0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param: Planning Familial
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Planning Familial' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Intake", e.patientID, p.clinicPatientID, 'Planning Familial', e.createDate, if(famPlan is null, '',
 case
       when v.famPlan=1 then 'oui'
       when v.famPlan=2 then 'non'
       else ''
 end), 'x', date(now())
       from itech.encounter e, itech.patient p, itech.vitals v
      where e.patientID = p.patientID
        and e.encounterType=1
        and e.encStatus < 255
        and e.patientID = v.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
        and e.seqnum=v.seqnum
        and e.siteCode =95698
        and v.famPlan in (1, 2)
           on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Planning Familial
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'Planning Familial', e.date_created,
   case
       when o.value_coded =1 then 'oui'
       when o.value_coded =2 then 'non'
       else ''
  end, 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =965 and o.encounter_id = e.encounter_id and o.voided=0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param:Grossesse
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Grossesse' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Intake", e.patientID, p.clinicPatientID, 'Grossesse', e.createDate, if(o.value_numeric is null, '',
 case
       when o.value_numeric =1 then 'oui'
       when o.value_numeric =2 then 'non'
       else ''
 end), 'x', date(now())
       from itech.encounter e, itech.patient p, itech.obs o
      where e.patientID = p.patientID
        and e.encounterType=1
        and e.encStatus < 255
        and o.encounter_id=e.encounter_id
        and o.concept_id=163590
        and e.siteCode =95698
        and o.value_numeric in (1, 2)
           on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param:Grossesse
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'Grossesse', e.date_created,
      case
       when o.value_coded = 1065 then 'oui' --  Peut etre 1  ou 2 dans certains sites
       when o.value_coded = 1066 then 'non'
       else ''
  end, 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1434 and o.encounter_id = e.encounter_id and o.voided=0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param:Allaitement
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Allaitement' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Intake", e.patientID, p.clinicPatientID, 'Allaitement', e.createDate, if(value_numeric is null, '',
 case
       when o.value_numeric = 1 then 'oui' --  Peut etre 1  ou 2 dans certains sites
       when o.value_numeric = 2 then 'non'
       else ''
 end), 'x', date(now())
       from itech.encounter e, itech.patient p, itech.obs o
      where e.patientID = p.patientID
        and e.encounterType=1
        and e.encStatus < 255
        and o.encounter_id=e.encounter_id
        and o.concept_id=163620
        and e.siteCode =95698
        and o.value_numeric in (1, 2)
           on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param:Allaitement
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'Allaitement', e.date_created,
  case
       when o.value_coded =1065 then 'oui'
       when o.value_coded =1066 then 'non'
       else ''
  end, 'x', date(now())
       from cepoz.patient p
        left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
        left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
        inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
        inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
        inner join cepoz.encounter e on e.patient_id = p.patient_id
        inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
        inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5632 and o.encounter_id = e.encounter_id and o.voided=0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param: Date premier test VIH
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Date premier test VIH' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, "Intake", e.patientID, p.clinicPatientID, 'Date premier test VIH', e.createDate, concat(firstTestDd,'-',firstTestMm,'-',firstTestYy), 'x', date(now())
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType = 1
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and ifnull(firstTestYy, 0) <>0
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Date premier test VIH
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Intake', pid.identifier as patient_id, id.identifier, 'Date premier test VIH', e.date_created, date(o.value_datetime), 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160082 and o.encounter_id = e.encounter_id and o.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param: Dépistage du cancer du col
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Depistage du cancer du col' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Intake', e.patientID, p.clinicPatientID, 'Depistage du cancer du col', e.createDate, if(value_numeric is null, '',
   case
       when o.value_numeric = 1 then 'Oui'
   when o.value_numeric = 2 then 'Non'
   end), 'x',date(now())
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType = 1
  and e.encStatus < 255
  and o.encounter_id = e.encounter_id
  and o.concept_id = '146602'
  and e.siteCode = 95698
  and o.value_numeric in (1, 2)
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Despistage du cancer du Col
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Intake', pid.identifier as patient_id, id.identifier, 'Despistage du cancer du Col', e.date_created,
 case
      when o.value_coded =1065 then 'oui'
      when o.value_coded =1066 then 'non'
  end,'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160721 and o.encounter_id = e.encounter_id and o.voided=0
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id =160714
    inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o.concept_id =1651 and o.value_coded =151185
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param: Pas de signes ou symptomes suggestifs de TB
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Pas de signes ou symptomes suggestifs de TB' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Intake', e.patientID, p.clinicPatientID, 'Pas de signes ou symptomes suggestifs de TB', e.createDate, if(v.asymptomaticTb=1, 'Oui', ''), 'x',date(now())
from itech.encounter e, itech.patient p, itech.tbStatus v
where e.patientID = p.patientID
  and e.encounterType = 1
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and v.asymptomaticTb=1
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Pas de signes ou symptomes suggestifs de TB
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Intake', pid.identifier as patient_id, id.identifier, 'Pas de signes ou symptomes suggestifs de TB', e.date_created, 'oui', 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1659 and o.encounter_id = e.encounter_id and o.value_coded=1660 and o.voided=0
     where p.voided =0
        on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param: Traitement TB complete
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Traitement TB complete' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Intake', e.patientID, p.clinicPatientID, 'Traitement TB complete', e.createDate, if(v.completeTreat=1, 'Oui', ''), 'x', date(now())
from itech.encounter e, itech.patient p, itech.tbStatus v
where e.patientID = p.patientID
  and e.encounterType = 1
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and v.completeTreat=1
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Traitement TB complete
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Intake', pid.identifier as patient_id, id.identifier, 'Traitement TB complete', e.date_created, 'oui', 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1659 and o.encounter_id = e.encounter_id and o.value_coded=1663 and o.voided=0
     where p.voided =0
        on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param: Actuellement sous traitement TB
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Actuellement sous traitement TB' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Intake', e.patientID, p.clinicPatientID, 'Actuellement sous traitement TB', e.createDate, if(v.currentTreat=1, 'Oui', '') , 'x', date(now())
from itech.encounter e, itech.patient p, itech.tbStatus v
where e.patientID = p.patientID
  and e.encounterType = 1
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and v.currentTreat=1
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Actuellement sous traitement TB
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Intake', pid.identifier as patient_id, id.identifier, 'Actuellement sous traitement TB', e.date_created, 'oui', 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1659 and o.encounter_id = e.encounter_id and o.value_coded=1662 and o.voided=0
     where p.voided =0
        on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param: Date traitement TB complete
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Date traitement TB complete' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Intake', e.patientID, p.clinicPatientID, 'Date traitement TB complete', e.createDate, date(concat(completeTreatDd,'-',completeTreatMm,'-',completeTreatYy)), 'x', date(now())
from itech.encounter e, itech.patient p, itech.tbStatus v
where e.patientID = p.patientID
  and e.encounterType = 1
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and ifnull(completeTreatYy, 0) <> 0
  on duplicate key update isante_value = values(isante_value), is_valid = (case
    when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
    when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
    else 'x'
    end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Date traitement TB complete
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Intake', pid.identifier as patient_id, id.identifier, 'Date traitement TB complete', e.date_created, date(o.value_datetime), 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =159431 and o.encounter_id = e.encounter_id and o.voided=0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param: Preservatifs
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Preservatifs' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Intake', e.patientID, p.clinicPatientID, 'Preservatifs', e.createDate,
 case
   when famPlanMethodCondom = 1 then 'Oui'
   when famPlanMethodCondom = 2 then 'Non'
  end, 'x', date(now())
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType = 1
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and famPlanMethodCondom in (1,2)
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Preservatifs
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Intake', pid.identifier as patient_id, id.identifier, 'Preservatifs', e.date_created, 'oui', 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =374 and o.encounter_id = e.encounter_id and o.value_coded =190 and o.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param: DMPA
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='DMPA' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Intake', e.patientID, p.clinicPatientID, 'DMPA', e.createDate,
       case when famPlanMethodDmpa = 1 then 'Oui'
            when famPlanMethodDmpa = 2 then 'Non'
            else ''
           end, 'x', date(now())
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType = 1
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and famPlanMethodDmpa in (1,2)
  on duplicate key update isante_value = values(isante_value), is_valid = (case
    when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
    when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
    else 'x'
    end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: DMPA
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Intake', pid.identifier as patient_id, id.identifier, 'DMPA', e.date_created, 'oui', 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =374 and o.encounter_id = e.encounter_id and o.value_coded =5279 and o.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param: Pilules
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Pilules' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Intake', e.patientID, p.clinicPatientID, 'Pilules', e.createDate,
   case
       when famPlanMethodOcPills = 1 then 'Oui'
       when famPlanMethodOcPills = 2 then 'Non'
  end, 'x', date(now())
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType = 1
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and famPlanMethodOcPills in (1,2)
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Pilules
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Intake', pid.identifier as patient_id, id.identifier, 'Pilules', e.date_created, 'oui', 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =374 and o.encounter_id = e.encounter_id and o.value_coded =780 and o.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param: Ligatures des trompes
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Ligatures des trompes' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Intake', e.patientID, p.clinicPatientID, 'Ligatures des trompes', e.createDate,
       case when famPlanMethodTubalLig = 1 then 'Oui'
            when famPlanMethodTubalLig = 2 then 'Non'
           end, 'x', date(now())
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType = 1
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and famPlanMethodTubalLig in (1,2)
  on duplicate key update isante_value = values(isante_value), is_valid = (case
    when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
    when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
    else 'x'
    end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Ligatures des trompes
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Intake', pid.identifier as patient_id, id.identifier, 'Ligatures des trompes', e.date_created, 'oui', 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =374 and o.encounter_id = e.encounter_id and o.value_coded =1472 and o.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param: Suspicion TB  selon symptomes
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Suspicion TB selon symptomes' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Intake', e.patientID, p.clinicPatientID, 'Suspicion TB selon symptomes', e.createDate, 'Oui', 'x', date(now())
from itech.encounter e, itech.patient p, itech.tbStatus v
where e.patientID = p.patientID
  and e.encounterType = 1
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and suspicionTBwSymptoms=1
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Suspicion TB  selon symptomes
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Intake', pid.identifier as patient_id, id.identifier, 'Suspicion TB  selon symptomes', e.date_created, 'oui', 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1659 and o.encounter_id = e.encounter_id and o.value_coded =142177
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param: Acun signe ou sympôtme indicatif de TB
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Aucun signe ou symptome indicatif de TB' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Intake', e.patientID, p.clinicPatientID, 'Aucun signe ou symptome indicatif de TB', e.createDate, 'Oui', 'x', date(now())
from itech.encounter e, itech.patient p, itech.tbStatus v
where e.patientID = p.patientID
  and e.encounterType = 1
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and noTBsymptoms = 1
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Acun signe ou sympôtme indicatif de TB
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Intake', pid.identifier as patient_id, id.identifier, 'Aucun signe ou symptome indicatif de TB', e.date_created, 'oui', 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1659 and o.encounter_id = e.encounter_id and o.value_coded =1660
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param: Date debut Prophylaxie INH
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Date debut Prophylaxie INH' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Intake', e.patientID, p.clinicPatientID, 'Date debut Prophylaxie INH', e.createDate, concat(debutINHMm,'-',debutINHYy), 'x', date(now())
from itech.encounter e, itech.patient p, itech.tbStatus v
where e.patientID = p.patientID
  and e.encounterType = 1
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and ifnull(debutINHYy, 0)<>0
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Date debut Prophylaxie INH
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Intake', pid.identifier as patient_id, id.identifier, 'Date debut Prophylaxie INH', e.date_created, date(o.value_datetime), 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =162320 and o.encounter_id = e.encounter_id
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param: Date arret INH
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Date arret INH' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Intake', e.patientID, p.clinicPatientID, 'Date arret INH', e.createDate, concat(arretINHMm,'-',arretINHYy), 'x', date(now())
from itech.encounter e, itech.patient p, itech.tbStatus v
where e.patientID = p.patientID
  and e.encounterType = 1
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and ifnull(arretINHYy, 0)<>0
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: 'Date arret INH'
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Intake', pid.identifier as patient_id, id.identifier,'Date arret INH', e.date_created, date(o.value_datetime), 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =163284 and o.encounter_id = e.encounter_id
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param: Eligibilite medicale aux Arv: Stade I (Asymptomatique)
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Eligibilite medicale aux Arv: Stade I (Asymptomatique)' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Intake', e.patientID, p.clinicPatientID, 'Eligibilite medicale aux Arv: Stade I (Asymptomatique)', e.createDate, 'oui', 'x', date(now())
from itech.encounter e, itech.patient p, itech.medicalEligARVs v
where e.patientID = p.patientID
  and e.encounterType = 1
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and  v.currentHivStage = 1
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Eligibilite medicale aux Arv: Stade I (Asymptomatique)
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Intake', pid.identifier as patient_id, id.identifier, 'Eligibilite medicale aux Arv: Stade I (Asymptomatique)', e.date_created, 'oui', 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5356 and o.encounter_id = e.encounter_id and o.voided=0 and o.value_coded=1204
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param: Eligibilite medicale aux Arv: Stade II (symptomatique)
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Eligibilite medicale aux Arv: Stade II' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Intake', e.patientID, p.clinicPatientID, 'Eligibilite medicale aux Arv: Stade II', e.createDate, 'oui', 'x', date(now())
from itech.encounter e, itech.patient p, itech.medicalEligARVs v
where e.patientID = p.patientID
  and e.encounterType = 1
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and v.currentHivStage = 2
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Eligibilite medicale aux Arv: Stade II (Symptomatique)
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Intake', pid.identifier as patient_id, id.identifier, 'Eligibilite medicale aux Arv: Stade II', e.date_created, if(o.value_coded is null, '','oui'), 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5356 and o.encounter_id = e.encounter_id and o.voided=0 and o.value_coded=1205
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param: Eligibilite medicale aux Arv: Stade III (symptomatique)
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Eligibilite medicale aux Arv: Stade III' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Intake', e.patientID, p.clinicPatientID, 'Eligibilite medicale aux Arv: Stade III', e.createDate, 'oui', 'x', date(now())
from itech.encounter e, itech.patient p, itech.medicalEligARVs v
where e.patientID = p.patientID
  and e.encounterType = 1
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and v.currentHivStage = 4
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Eligibilite medicale aux Arv: Stade III (Symptomatique)
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'Eligibilite medicale aux Arv: Stade III', e.date_created, if(o.value_coded is null, '','oui'), 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5356 and o.encounter_id = e.encounter_id and o.voided=0 and o.value_coded=1206
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param: 'Eligibilite medicale aux Arv: Stade IV
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Eligibilite medicale aux Arv: Stade IV' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Intake', e.patientID, p.clinicPatientID, 'Eligibilite medicale aux Arv: Stade IV', e.createDate, 'oui', 'x', date(now())
from itech.encounter e, itech.patient p, itech.medicalEligARVs v
where e.patientID = p.patientID
  and e.encounterType = 1
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and v.currentHivStage = 8
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Eligibilite medicale aux Arv: Stade IV'
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Intake", pid.identifier as patient_id, id.identifier, 'Eligibilite medicale aux Arv: Stade IV', e.date_created, if(o.value_coded is null, '', 'oui'), 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5356 and o.encounter_id = e.encounter_id and o.voided=0 and o.value_coded=1207
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param: Date du début du traitement TB
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Date du debut du traitement TB' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Intake', e.patientID, p.clinicPatientID, 'Date du debut du traitement TB', e.createDate, o.value_datetime , 'x',date(now())
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and e.encStatus < 255
  and o.encounter_id=e.encounter_id
  and o.concept_id='163607'
  and e.siteCode =95698
  and ifnull(o.value_datetime, '') <>''
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Date du début du traitement TB
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Intake', pid.identifier as patient_id, id.identifier, 'Date du debut du traitement TB', e.date_created, date(o.value_datetime), 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1113 and o.encounter_id = e.encounter_id and o.voided=0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param:'Date dernieres regles
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Date dernieres regles' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, "Intake", e.patientID, p.clinicPatientID, 'Date dernieres regles', e.createDate, date(concat(pregnantLmpYy,'-',pregnantLmpMm,'-',pregnantLmpDd)), 'x', date(now())
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType = 1
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and ifnull(pregnantLmpYy, 0)<>0
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Date derniere regles
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Intake', pid.identifier as patient_id, id.identifier, 'Date dernieres regles', e.date_created, date(o.value_datetime), 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1427 and o.encounter_id = e.encounter_id and o.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param: Date depistage cancer col
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Date depistage du cancer col' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, "Intake", e.patientID, p.clinicPatientID, 'Date depistage du cancer col', e.createDate,  date(concat(papTestYy,'-',papTestMm,'-',papTestDd)), 'x', date(now())
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType = 1
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and ifnull(papTestYy, '') <> ''
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Date depistage cancer col
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Intake', pid.identifier as patient_id, id.identifier, 'Date depistage cancer col', e.date_created, date(o.value_datetime), 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =165429 and o.encounter_id = e.encounter_id and o.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- isante database
-- itech
-- form: Intake
-- param: Autres
-- site code: 95698
delete from replication.validation where form_origin='Intake' and parameter='Autres' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Intake', e.patientID, p.clinicPatientID, 'Autres', e.createDate, 'oui', 'x', date(now())
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType = 1
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and famPlanOther=1
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Intake
-- param: Autres
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Intake', pid.identifier as patient_id, id.identifier, 'Autres', e.date_created, 'oui', 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '17536ba6-dd7c-4f58-8014-08c7cb798ac7' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =374 and o.encounter_id = e.encounter_id and o.value_coded =5622
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);




-- ----------------------------------------------------------Fiche VIH visite suivie--------------------------------------------
-- consolidated isante database
-- itech
-- form: Followup
-- param: Temperature
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Temperature' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, 'Followup', e.patientID, p.clinicPatientID, 'Temperature', e.createDate, if(vitalTemp is null, '',
case
       when vitalTempUnits =1 then if (vitalTemp <> '', convert(vitalTemp, decimal(10, 2)), vitalTemp)
       when vitalTempUnits =2 then round((vitalTemp-32) * 5/9, 2)
       else ''
end), 'x', date(now())
       from itech.encounter e, itech.patient p, itech.vitals v
      where e.patientID = p.patientID
        and e.encounterType=2
        and e.encStatus < 255
        and e.patientID = v.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
        and e.seqnum=v.seqnum
        and e.siteCode = 95698
        and v.vitalTempUnits in (1, 2)
         on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Followup
-- param: Temperature
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, 'Followup', pid.identifier as patient_id, id.identifier, 'Temperature', e.date_created, convert(o.value_numeric,decimal(10,2)) as 'temperature', 'x',date (now())
           from cepoz.patient p
        left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
        left  outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
        inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
        inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
        inner join cepoz.encounter e on e.patient_id = p.patient_id
        inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
        inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5088 and o.encounter_id = e.encounter_id and o.voided =0
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- consolidated isante database
-- itech
-- form: Followup
-- param: TA Systolique
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='TA Systolique' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Followup", e.patientID, p.clinicPatientID, 'TA Systolique', date(e.createDate), if(vitalBp1 is null, '',
case
       when vitalBPUnits = 1 then if (vitalBp1 <>'', convert(vitalBp1 * 10, decimal(10, 2)), vitalBp1)
       when vitalBPUnits = 2 then if (vitalBp1 <>'', convert(vitalBp1, decimal(10, 2)), vitalBp1)
       else ''
end), 'x',date(now())
       from itech.encounter e, itech.patient p, itech.vitals v
      where e.patientID = p.patientID
        and e.encounterType=2
        and e.encStatus < 255
        and e.patientID = v.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
        and e.seqnum=v.seqnum
        and e.siteCode = 95698
        and v.vitalBPUnits in (1, 2)
        and ifnull(v.vitalBp1, '') <> ''
           on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Followup
-- param: TA Systolique
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, 'Followup', pid.identifier as patient_id, id.identifier, 'TA Systolique', e.date_created, convert(o.value_numeric, decimal(10, 2)),'x',date (now())
           from cepoz.patient p
        left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
        left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
        inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
        inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
        inner join cepoz.encounter e on e.patient_id = p.patient_id
        inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
        inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5085 and o.encounter_id = e.encounter_id and o.value_numeric is not null and o.voided =0
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- consolidated isante database
-- itech
-- form: Followup
-- param: TA Diastolique
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='TA Diastolique' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Followup", e.patientID, p.clinicPatientID, 'TA Diastolique', e.createDate, if(vitalBp2 is null, '',
case
       when vitalBPUnits = 1 then if (vitalBp2 <>'', convert(vitalBp2 * 10, decimal(10, 2)), vitalBp2)
       when vitalBPUnits = 2 then if (vitalBp2 <>'', convert(vitalBp2, decimal(10, 2)), vitalBp2)
       else ''
end), 'x',date(now())
       from itech.encounter e, itech.patient p, itech.vitals v
      where e.patientID = p.patientID
        and e.encounterType=2
        and e.encStatus < 255
        and e.patientID = v.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
        and e.seqnum=v.seqnum
        and e.siteCode = 95698
        and v.vitalBPUnits in (1, 2)
        and ifnull(v.vitalBp2, '') <> ''
           on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Followup
-- param: TA Diastolique
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, 'Followup', pid.identifier as patient_id, id.identifier, 'TA Diastolique', e.date_created, convert(o.value_numeric, decimal(10,2)),'x',date (now())
           from cepoz.patient p
        left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
        left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
        inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
        inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
        inner join cepoz.encounter e on e.patient_id = p.patient_id
        inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
        inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5086 and o.encounter_id = e.encounter_id and o.value_numeric is not null and o.voided =0
          where p.voided =0 and o.value_numeric is not null
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante database
-- itech
-- form: Followup
-- param: Pools
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Pools' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Followup", e.patientID, p.clinicPatientID, 'Pools', e.createDate, if(v.vitalHr<>'', convert(v.vitalHr, decimal(10, 2)), v.vitalHr), 'x', date(now())
       from itech.encounter e, itech.patient p, itech.vitals v
      where e.patientID = p.patientID
        and e.encounterType=2
        and e.encStatus < 255
        and e.patientID = v.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
        and e.seqnum=v.seqnum
        and e.siteCode =95698
        and ifnull(v.vitalHr, '') <> ''
           on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Followup
-- param: Pools
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, 'Followup', pid.identifier as patient_id, id.identifier, 'Pools', e.date_created, convert(o.value_numeric, decimal(10, 2)) ,'x', date(now())
           from cepoz.patient p
        left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
        left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
        inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
        inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
        inner join cepoz.encounter e on e.patient_id = p.patient_id
        inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
        inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5087 and o.encounter_id = e.encounter_id and o.voided =0
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante database
-- itech
-- form: Followup
-- param: Frequence Respiratoire
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Frequence Respiratoire' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Followup", e.patientID, p.clinicPatientID, 'Frequence Respiratoire', e.createDate,  if(vitalRr <>'', convert(vitalRr, decimal(10, 2)),''), 'x', date(now())
       from itech.encounter e, itech.patient p, itech.vitals v
      where e.patientID = p.patientID
        and e.encounterType=2
        and e.encStatus < 255
        and e.patientID = v.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
        and e.seqnum=v.seqnum
        and e.siteCode =95698
        and ifnull(v.vitalRr,'') <> ''
           on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Followup
-- param: Frequence Respiratoire
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, 'Followup', pid.identifier as patient_id, id.identifier, 'Frequence Respiratoire', e.date_created, convert(o.value_numeric, decimal(10,2)) as 'frequence_respiratoire', 'x',date (now())
           from cepoz.patient p
        left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
        left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
        inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
        inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
        inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
        inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5242 and o.encounter_id = e.encounter_id and o.voided =0
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante database  aaa
-- itech
-- form: Followup
-- param: Taille
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Taille' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Followup", e.patientID, p.clinicPatientID, 'Taille', e.createDate, if(vitalHeightCm=0 or vitalHeightCm is null, '',
case
       when vitalHeight is not null and vitalHeightCm is null then vitalHeight
       when vitalHeight is not null and vitalHeightCm is not null then (vitalHeight + vitalHeightCm)
       when vitalHeightCm is not null and vitalHeight is null then vitalHeightCm
       else ''
end), 'x', date(now())
       from itech.encounter e, itech.patient p, itech.vitals v
      where e.patientID = p.patientID
        and e.encounterType=2
        and e.encStatus < 255
        and e.patientID = v.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
        and e.seqnum=v.seqnum
        and e.siteCode =95698
        and ifnull(vitalHeightCm, 0)>0
           on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Followup
-- param: Taille
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, 'Followup', pid.identifier as patient_id, id.identifier, 'Taille', e.date_created, o.value_numeric as 'taille', 'x',date (now())
           from cepoz.patient p
        left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
        left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
        inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
        inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
        inner join cepoz.encounter e on e.patient_id = p.patient_id
        inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
        inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5090 and o.encounter_id = e.encounter_id and o.voided =0
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante database
-- itech
-- form: Followup
-- param: Poids
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Poids' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Followup", e.patientID, p.clinicPatientID, 'Poids', e.createDate, if(vitalWeight is null, '',
case
       when vitalWeightUnits =1 then if(vitalWeight <> '', convert(vitalWeight, decimal(10, 2)), vitalWeight)
       when vitalWeightUnits =2 then round(vitalWeight * 0.453592, 2)
       ELSE ''
end), 'x', date(now())
       from itech.encounter e, itech.patient p, itech.vitals v
      where e.patientID = p.patientID
        and e.encounterType=2
        and e.encStatus < 255
        and e.patientID = v.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
        and e.seqnum=v.seqnum
        and e.siteCode =95698
        and v.vitalWeightUnits in (1, 2)
        and ifnull(v.vitalWeight, 0)>0
           on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Followup
-- param: Poids
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, 'Followup', pid.identifier as patient_id, id.identifier, 'Poids', e.date_created, if(o.value_numeric <> '', convert(o.value_numeric , decimal(10,2)),'' ), 'x',date (now())
           from cepoz.patient p
        left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
        left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
        inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
        inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
        inner join cepoz.encounter e on e.patient_id = p.patient_id
        inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
        inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5089 and o.encounter_id = e.encounter_id and o.voided =0
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante database
-- itech
-- form: Followup
-- param: Resultat VIH recu par le patient
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Resultat VIH reçu par le patient' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Followup", e.patientID, p.clinicPatientID, 'Resultat VIH reçu par le patient', e.createDate, if(v.firstTestResultsReceived is null, '',
case
       when v.firstTestResultsReceived =1 then 'oui'
       when v.firstTestResultsReceived =2 then 'non'
       else ''
end), 'x', date(now())
       from itech.encounter e, itech.patient p, itech.vitals v
      where e.patientID = p.patientID
        and e.encounterType=2
        and e.encStatus < 255
        and e.patientID = v.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
        and e.seqnum=v.seqnum
        and e.siteCode =95698
        and v.firstTestResultsReceived in (1, 2)
           on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Followup
-- param: Resultat VIH recu par le patient
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Followup", pid.identifier as patient_id, id.identifier, 'Resultat VIH reçu par le patient', e.date_created,
    case
           when o.value_coded =1065 then 'oui'
           when o.value_coded =1066 then 'non'
           else ''
    end,'x',date(now())
           from cepoz.patient p
        left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
        left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
        inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
        inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
        inner join cepoz.encounter e on e.patient_id = p.patient_id
        inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
        inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =164848 and o.encounter_id = e.encounter_id and o.voided =0
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);


-- isante database
-- itech
-- form: Followup
-- param: Grossesse actuelle
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Grossesse actuelle' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Followup", e.patientID, p.clinicPatientID, 'Grossesse actuelle', e.createDate, if(pregnant is null, '',
case
       when pregnant =1 then 'oui'
       when pregnant =2 then 'non'
       when pregnant =4 then 'inconnu'
       else ''
end), 'x', date(now())
       from itech.encounter e, itech.patient p, itech.vitals v
      where e.patientID = p.patientID
        and e.encounterType=2
        and e.encStatus < 255
        and e.patientID = v.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
        and e.seqnum=v.seqnum
        and e.siteCode =95698
        and v.pregnant in (1, 2, 4)
           on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Followup
-- param: Grossesse actuelle
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Followup", pid.identifier as patient_id, id.identifier, 'Grossesse actuelle', e.date_created,
     case
           when o.value_coded =1065 then 'oui'
           when o.value_coded =1066 then 'non'
           when o.value_coded =134346 then 'Menopause'
           when o.value_coded =1067 then 'inconnu'
           else ''
    end as 'grossesse','x', date(now())
           from cepoz.patient p
        left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
        left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
        inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
        inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
        inner join cepoz.encounter e on e.patient_id = p.patient_id
        inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
        inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5272 and o.encounter_id = e.encounter_id and o.voided =0
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante database
-- itech
-- form: Followup
-- param: Methode utilisee pour le depistage du cancer du col
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Methode utilisee pour le depistage du cancer du col' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Followup", e.patientID, p.clinicPatientID, 'Methode utilisee pour le depistage du cancer du col', e.createDate, if(value_numeric is null, '',
case
       when o.value_numeric =1 then 'Pap test'
       when o.value_numeric =2 then 'VIA'
       else ''
end), 'x',date(now())
       from itech.encounter e, itech.patient p, itech.obs o
      where e.patientID = p.patientID
        and e.encounterType=2
        and e.encStatus < 255
        and o.encounter_id=e.encounter_id
        and o.concept_id=163589
        and e.siteCode =95698
        and o.value_numeric in (1, 2)
           on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);


-- isanteplus database
-- cepoz
-- form: Followup
-- param: Methode utilisee pour le depistage du cancer du col
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Followup", pid.identifier as patient_id, id.identifier, 'Methode utilisee pour le depistage du cancer du col', e.date_created,
    case
           when o.value_coded =885 then 'Pap test'
           when o.value_coded =162816 then 'VIA'
           else ''
    end, 'x',date(now())
           from cepoz.patient p
        left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
        left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
        inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
        inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
        inner join cepoz.encounter e on e.patient_id = p.patient_id
            inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
        inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =163589 and o.encounter_id = e.encounter_id and o.voided=0
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante database
-- itech
-- form: Followup
-- param: Resultat du test du cancer du col
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Resultat du test du cancer du col' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Followup", e.patientID, p.clinicPatientID, 'Resultat du test du cancer du col', e.createDate, if(o.value_numeric is null, '',
case
       when o.value_numeric =1 then 'Normal'
       when o.value_numeric =2 then 'Anormal'
       when o.value_numeric =4 then 'Inconnu'
       else ''
end), 'x', date(now())
       from itech.encounter e, itech.patient p, itech.obs o
      where e.patientID = p.patientID
        and e.encounterType=2
        and e.encStatus < 255
        and o.encounter_id=e.encounter_id
        and o.concept_id=70029
        and e.siteCode =95698
        and o.value_numeric in (1, 2, 4)
           on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Followup
-- param: Resultat du test du cancer du col
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Followup", pid.identifier as patient_id, id.identifier, 'Resultat du test du cancer du col', e.date_created,
    case
           when o.value_coded =1115 then 'Normal'
           when o.value_coded =1116 then 'Anormal'
           when o.value_coded =1067 then 'Inconnu'
           else ''
    end, 'x',date(now())
           from cepoz.patient p
        left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
        left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
        inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
        inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
        inner join cepoz.encounter e on e.patient_id = p.patient_id
            inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
        inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160704 and o.encounter_id = e.encounter_id and o.voided=0
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);


-- isante database
-- itech
-- form: Followup
-- param: Planning Familial
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Planning Familial' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Followup", e.patientID, p.clinicPatientID, 'Planning Familial', e.createDate, if(v.famPlan is null, '',
case
       when v.famPlan=1 then 'oui'
       when v.famPlan =2 then 'non'
       else ''
end), 'x',  date(now())
       from itech.encounter e, itech.patient p, itech.vitals v
      where e.patientID = p.patientID
        and e.encounterType=2
        and e.encStatus < 255
        and e.patientID = v.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
        and e.seqnum=v.seqnum
        and e.siteCode =95698
        and v.famPlan in (1, 2)
           on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Followup
-- param: Planning Familial
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Followup", pid.identifier as patient_id, id.identifier, 'Planning Familial', e.date_created,
case
       when o.value_coded =1 then 'oui'
       when o.value_coded =2 then 'non'
       else ''
end, 'x',date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
        inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =965 and o.encounter_id = e.encounter_id and o.voided=0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);


select name from concept_name where concept_id=119533 and locale in ('en') limit 1 ;


select o.obs_id, o.person_id as ID, o.concept_id,o.obs_group_id ,o.encounter_id,o.obs_datetime as obs_date, o.value_coded as val_c,
       o.value_coded_name_id as val_cn, o.value_datetime as val_dt, o.value_numeric as val_num, o.value_text as v_t,
       o.date_created, o.voided, o.date_voided, o.void_reason
from obs o inner join patient p on p.patient_id=o.person_id and o.concept_id =965
 limit 10;
person_id = 562
-- isante database
-- itech
-- form: Followup
-- param:Grossesse
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Grossesse' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Followup", e.patientID, p.clinicPatientID, 'Grossesse', e.createDate, if(o.value_numeric is null, '',
case
       when o.value_numeric =1 then 'oui'
       when o.value_numeric =2 then 'non'
       else ''
end), 'x',date(now())
       from itech.encounter e, itech.patient p, itech.obs o
      where e.patientID = p.patientID
        and e.encounterType=2
        and e.encStatus < 255
        and o.encounter_id=e.encounter_id
        and o.concept_id=163590
        and e.siteCode =95698
        and o.value_numeric in (1, 2)
           on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Followup
-- param: Grossesse
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Followup", pid.identifier as patient_id, id.identifier, 'Grossesse', e.date_created,
    case
           when o.value_coded = 1065 then 'oui'
           when o.value_coded = 1066 then 'non'
           else ''
    end, 'x',date(now())
           from cepoz.patient p
        left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
        left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
        inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
        inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
        inner join cepoz.encounter e on e.patient_id = p.patient_id
            inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
        inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1434 and o.encounter_id = e.encounter_id and o.voided=0
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante database
-- itech
-- form: Followup
-- param:Allaitement
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Allaitement' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Followup", e.patientID, p.clinicPatientID, 'Allaitement', e.createDate, if(o.value_numeric is null, '',
case
       when o.value_numeric = 1 then 'oui'
       when o.value_numeric = 2 then 'non'
       else ''
end), 'x',date(now())
       from itech.encounter e, itech.patient p, itech.obs o
      where e.patientID = p.patientID
        and e.encounterType=2
        and e.encStatus < 255
        and o.encounter_id=e.encounter_id
        and o.concept_id=163620
        and e.siteCode =95698
        and o.value_numeric in (1, 2)
           on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Followup
-- param:Allaitement
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Followup", pid.identifier as patient_id, id.identifier, 'Allaitement', e.date_created,
    case
           when o.value_coded =1065 then 'oui'
           when o.value_coded =1066 then 'non'
           else ''
    end, 'x',date(now())
           from cepoz.patient p
        left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
        left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
        inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
        inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
        inner join cepoz.encounter e on e.patient_id = p.patient_id
            inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
        inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5632 and o.encounter_id = e.encounter_id and o.voided=0
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante database
-- itech
-- form: Followup
-- param: Date premier test VIH
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Date premier test VIH' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, "Followup", e.patientID, p.clinicPatientID, 'Date premier test VIH', e.createDate, concat(firstTestDd,'-',firstTestMm,'-',firstTestYy), 'x', date(now())
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType = 2
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and ifnull(firstTestYy, 0) <>0
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);


-- isanteplus database
-- cepoz
-- form: Followup
-- param: Date premier test VIH
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
    select 95698, 'Followup', pid.identifier as patient_id, id.identifier, 'Date premier test VIH', e.date_created, date(o.value_datetime), 'x', date(now())
        from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160082 and o.encounter_id = e.encounter_id and o.voided =0
        where p.voided =0
        on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);



-- isante database
-- itech
-- form: Followup
-- param: Dépistage du cancer du col
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Depistage du cancer du col' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Followup', e.patientID, p.clinicPatientID, 'Depistage du cancer du col', e.createDate, if(value_numeric is null, '',
case
   when o.value_numeric = 1 then 'Oui'
   when o.value_numeric = 2 then 'Non'
   end), 'x',date(now())
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType = 2
  and e.encStatus < 255
  and o.encounter_id = e.encounter_id
  and o.concept_id = '146602'
  and e.siteCode = 95698
  and o.value_numeric in (1, 2)
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Followup
-- param: Despistage du cancer du Col
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
    select 95698, 'Followup', pid.identifier as patient_id, id.identifier, 'Despistage du cancer du Col', e.date_created,
           case
               when o.value_coded =1065 then 'oui'
               when o.value_coded =1066 then 'non'
               end,'x', date(now())
        from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160721 and o.encounter_id = e.encounter_id and o.voided=0
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id =160714
        where p.voided =0
        on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante database
-- itech
-- form: Followup
-- param: Pas de signes ou symptomes suggestifs de TB
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Pas de signes ou symptomes suggestifs de TB' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Followup', e.patientID, p.clinicPatientID, 'Pas de signes ou symptomes suggestifs de TB', e.createDate, if(v.asymptomaticTb=1, 'Oui', ''), 'x',date(now())
from itech.encounter e, itech.patient p, itech.tbStatus v
where e.patientID = p.patientID
  and e.encounterType = 2
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and v.asymptomaticTb=1
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Followup
-- param: Pas de signes ou symptomes suggestifs de TB
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
    select 95698, 'Followup', pid.identifier as patient_id, id.identifier, 'Pas de signes ou symptomes suggestifs de TB', e.date_created, 'oui', 'x', date(now())
        from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1659 and o.encounter_id = e.encounter_id and o.value_coded=1660 and o.voided=0
        where p.voided =0
        on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);


-- isante database
-- itech
-- form: Followup
-- param: Traitement TB complete
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Traitement TB complete' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Followup', e.patientID, p.clinicPatientID, 'Traitement TB complete', e.createDate, if(v.completeTreat=1, 'Oui', ''), 'x', date(now())
from itech.encounter e, itech.patient p, itech.tbStatus v
where e.patientID = p.patientID
  and e.encounterType = 2
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and v.completeTreat=1
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);


-- isanteplus database
-- cepoz
-- form: Followup
-- param: Traitement TB complete
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
    select 95698, 'Followup', pid.identifier as patient_id, id.identifier, 'Traitement TB complete', e.date_created, 'oui', 'x', date(now())
        from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1659 and o.encounter_id = e.encounter_id and o.value_coded=1663 and o.voided=0
        where p.voided =0
        on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante database
-- itech
-- form: Followup
-- param: Actuellement sous traitement TB
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Actuellement sous traitement TB' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Followup', e.patientID, p.clinicPatientID, 'Actuellement sous traitement TB', e.createDate, if(v.currentTreat=1, 'Oui', '') , 'x', date(now())
from itech.encounter e, itech.patient p, itech.tbStatus v
where e.patientID = p.patientID
  and e.encounterType = 2
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and v.currentTreat=1
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Followup
-- param: Actuellement sous traitement TB
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
    select 95698, 'Followup', pid.identifier as patient_id, id.identifier, 'Actuellement sous traitement TB', e.date_created, 'oui', 'x', date(now())
        from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1659 and o.encounter_id = e.encounter_id and o.value_coded=1662 and o.voided=0
        where p.voided =0
            on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
                when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
                when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
                else 'x'
                end);



-- isante database
-- itech
-- form: Followup
-- param: Date traitement TB complete
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Date traitement TB complete' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Followup', e.patientID, p.clinicPatientID, 'Date traitement TB complete', e.createDate, date(concat(completeTreatDd,'-',completeTreatMm,'-',completeTreatYy)), 'x', date(now())
from itech.encounter e, itech.patient p, itech.tbStatus v
where e.patientID = p.patientID
  and e.encounterType = 2
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and ifnull(completeTreatYy, 0) <> 0
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Followup
-- param: Date traitement TB complete
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
    select 95698, 'Followup', pid.identifier as patient_id, id.identifier, 'Date traitement TB complete', e.date_created, date(o.value_datetime), 'x', date(now())
        from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =159431 and o.encounter_id = e.encounter_id and o.voided=0
        where p.voided =0
        on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);



-- isante database
-- itech
-- form: Followup
-- param: Preservatifs
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Preservatifs' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Followup', e.patientID, p.clinicPatientID, 'Preservatifs', e.createDate,
       case
           when famPlanMethodCondom = 1 then 'Oui'
           when famPlanMethodCondom = 2 then 'Non'
           end, 'x', date(now())
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType = 2
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and famPlanMethodCondom in (1,2)
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Followup
-- param: Preservatifs
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
        select 95698, 'Followup', pid.identifier as patient_id, id.identifier, 'Preservatifs', e.date_created, 'oui', 'x', date(now())
    from cepoz.patient p
        left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
        left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
        inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
        inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
        inner join cepoz.encounter e on e.patient_id = p.patient_id
        inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
        inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =374 and o.encounter_id = e.encounter_id and o.value_coded =190 and o.voided =0
            where p.voided =0
                on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
                when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
                when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
                else 'x'
                    end);


-- isante database
-- itech
-- form: Followup
-- param: DMPA
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='DMPA' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Followup', e.patientID, p.clinicPatientID, 'DMPA', e.createDate,
       case when famPlanMethodDmpa = 1 then 'Oui'
            when famPlanMethodDmpa = 2 then 'Non'
            else ''
           end, 'x', date(now())
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType = 2
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and famPlanMethodDmpa in (1,2)
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);


-- isanteplus database
-- cepoz
-- form: Followup
-- param: DMPA
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
    select 95698, 'Followup', pid.identifier as patient_id, id.identifier, 'DMPA', e.date_created, 'oui', 'x', date(now())
        from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =374 and o.encounter_id = e.encounter_id and o.value_coded =5279 and o.voided =0
        where p.voided =0
        on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);


-- isante database
-- itech
-- form: Followup
-- param: Pilules
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Pilules' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Followup', e.patientID, p.clinicPatientID, 'Pilules', e.createDate,
       case
           when famPlanMethodOcPills = 1 then 'Oui'
           when famPlanMethodOcPills = 2 then 'Non'
           end, 'x', date(now())
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType = 2
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and famPlanMethodOcPills in (1,2)
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);


-- isanteplus database
-- cepoz
-- form: Followup
-- param: Pilules
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
    select 95698, 'Followup', pid.identifier as patient_id, id.identifier, 'Pilules', e.date_created, 'oui', 'x', date(now())
        from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =374 and o.encounter_id = e.encounter_id and o.value_coded =780 and o.voided =0
        where p.voided =0
        on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);


-- isante database
-- itech
-- form: Followup
-- param: Ligatures des trompes
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Ligatures des trompes' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Followup', e.patientID, p.clinicPatientID, 'Ligatures des trompes', e.createDate,
       case when famPlanMethodTubalLig = 1 then 'Oui'
            when famPlanMethodTubalLig = 2 then 'Non'
           end, 'x', date(now())
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType = 2
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and famPlanMethodTubalLig in (1,2)
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);


-- isanteplus database
-- cepoz
-- form: Followup
-- param: Ligatures des trompes
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
    select 95698, 'Followup', pid.identifier as patient_id, id.identifier, 'Ligatures des trompes', e.date_created, 'oui', 'x', date(now())
        from cepoz.patient p
        left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
        left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
        inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
        inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
        inner join cepoz.encounter e on e.patient_id = p.patient_id
        inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
        inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =374 and o.encounter_id = e.encounter_id and o.value_coded =1472 and o.voided =0
            where p.voided =0
            on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
                when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
                when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
                else 'x'
                end);



-- isante database
-- itech
-- form: Followup
-- param: Suspicion TB  selon symptomes
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Suspicion TB selon symptomes' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Followup', e.patientID, p.clinicPatientID, 'Suspicion TB selon symptomes', e.createDate, 'Oui', 'x', date(now())
from itech.encounter e, itech.patient p, itech.tbStatus v
where e.patientID = p.patientID
  and e.encounterType = 2
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and suspicionTBwSymptoms=1
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Followup
-- param: Suspicion TB  selon symptomes
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
    select 95698, 'Followup', pid.identifier as patient_id, id.identifier, 'Suspicion TB  selon symptomes', e.date_created, 'oui', 'x', date(now())
        from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1659 and o.encounter_id = e.encounter_id and o.value_coded =142177
        where p.voided =0
        on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);



-- isante database
-- itech
-- form: Followup
-- param: Acun signe ou sympôtme indicatif de TB
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Aucun signe ou symptome indicatif de TB' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Followup', e.patientID, p.clinicPatientID, 'Aucun signe ou symptome indicatif de TB', e.createDate, 'Oui', 'x', date(now())
from itech.encounter e, itech.patient p, itech.tbStatus v
where e.patientID = p.patientID
  and e.encounterType = 2
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and noTBsymptoms = 1
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);


-- isanteplus database
-- cepoz
-- form: Followup
-- param: Acun signe ou sympôtme indicatif de TB
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
    select 95698, 'Followup', pid.identifier as patient_id, id.identifier, 'Aucun signe ou symptome indicatif de TB', e.date_created, 'oui', 'x', date(now())
        from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1659 and o.encounter_id = e.encounter_id and o.value_coded =1660
        where p.voided =0
        on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante database
-- itech
-- form: Followup
-- param: Date debut Prophylaxie INH
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Date debut Prophylaxie INH' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Followup', e.patientID, p.clinicPatientID, 'Date debut Prophylaxie INH', e.createDate, concat(debutINHMm,'-',debutINHYy), 'x', date(now())
from itech.encounter e, itech.patient p, itech.tbStatus v
where e.patientID = p.patientID
  and e.encounterType = 2
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and ifnull(debutINHYy, 0)<>0
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Followup
-- param: Date debut Prophylaxie INH
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
    select 95698, 'Followup', pid.identifier as patient_id, id.identifier, 'Date debut Prophylaxie INH', e.date_created, date(o.value_datetime), 'x', date(now())
        from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =162320 and o.encounter_id = e.encounter_id
        where p.voided =0
        on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante database
-- itech
-- form: Followup
-- param: Date arret INH
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Date arret INH' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Followup', e.patientID, p.clinicPatientID, 'Date arret INH', e.createDate, concat(arretINHMm,'-',arretINHYy), 'x', date(now())
from itech.encounter e, itech.patient p, itech.tbStatus v
where e.patientID = p.patientID
  and e.encounterType = 2
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and ifnull(arretINHYy, 0)<>0
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Followup
-- param: 'Date arret INH'
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
    select 95698, 'Followup', pid.identifier as patient_id, id.identifier,'Date arret INH', e.date_created, date(o.value_datetime), 'x', date(now())
        from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =163284 and o.encounter_id = e.encounter_id
        where p.voided =0
        on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);


-- isante database
-- itech
-- form: Followup
-- param: Eligibilite medicale aux Arv: Stade I (Asymptomatique)
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Eligibilite medicale aux Arv: Stade I (Asymptomatique)' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Followup', e.patientID, p.clinicPatientID, 'Eligibilite medicale aux Arv: Stade I (Asymptomatique)', e.createDate, 'oui', 'x', date(now())
from itech.encounter e, itech.patient p, itech.medicalEligARVs v
where e.patientID = p.patientID
  and e.encounterType = 2
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and  v.currentHivStage = 1
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);


-- isanteplus database
-- cepoz
-- form: Followup
-- param: Eligibilite medicale aux Arv: Stade I (Asymptomatique)
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
    select 95698, 'Followup', pid.identifier as patient_id, id.identifier, 'Eligibilite medicale aux Arv: Stade I (Asymptomatique)', e.date_created, 'oui', 'x', date(now())
        from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5356 and o.encounter_id = e.encounter_id and o.voided=0 and o.value_coded=1204
        where p.voided =0
        on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);


-- isante database
-- itech
-- form: Followup
-- param: Eligibilite medicale aux Arv: Stade II (symptomatique)
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Eligibilite medicale aux Arv: Stade II' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Followup', e.patientID, p.clinicPatientID, 'Eligibilite medicale aux Arv: Stade II', e.createDate, 'oui', 'x', date(now())
from itech.encounter e, itech.patient p, itech.medicalEligARVs v
where e.patientID = p.patientID
  and e.encounterType = 2
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and v.currentHivStage = 2
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Followup
-- param: Eligibilite medicale aux Arv: Stade II (Symptomatique)
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
    select 95698, 'Followup', pid.identifier as patient_id, id.identifier, 'Eligibilite medicale aux Arv: Stade II', e.date_created, if(o.value_coded is null, '','oui'), 'x', date(now())
        from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5356 and o.encounter_id = e.encounter_id and o.voided=0 and o.value_coded=1205
        where p.voided =0
        on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);


-- isante database
-- itech
-- form: Followup
-- param: Eligibilite medicale aux Arv: Stade III (symptomatique)
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Eligibilite medicale aux Arv: Stade III' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Followup', e.patientID, p.clinicPatientID, 'Eligibilite medicale aux Arv: Stade III', e.createDate, 'oui', 'x', date(now())
from itech.encounter e, itech.patient p, itech.medicalEligARVs v
where e.patientID = p.patientID
  and e.encounterType = 2
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and v.currentHivStage = 4
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Followup
-- param: Eligibilite medicale aux Arv: Stade III (Symptomatique)
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
    select 95698, "Followup", pid.identifier as patient_id, id.identifier, 'Eligibilite medicale aux Arv: Stade III', e.date_created, if(o.value_coded is null, '','oui'), 'x', date(now())
        from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5356 and o.encounter_id = e.encounter_id and o.voided=0 and o.value_coded=1206
        where p.voided =0
        on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);


-- isante database
-- itech
-- form: Followup
-- param: 'Eligibilite medicale aux Arv: Stade IV
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Eligibilite medicale aux Arv: Stade IV' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Followup', e.patientID, p.clinicPatientID, 'Eligibilite medicale aux Arv: Stade IV', e.createDate, 'oui', 'x', date(now())
from itech.encounter e, itech.patient p, itech.medicalEligARVs v
where e.patientID = p.patientID
  and e.encounterType = 2
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and v.currentHivStage = 8
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Followup
-- param: Eligibilite medicale aux Arv: Stade IV'
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
    select 95698, "Followup", pid.identifier as patient_id, id.identifier, 'Eligibilite medicale aux Arv: Stade IV', e.date_created, if(o.value_coded is null, '', 'oui'), 'x', date(now())
        from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5356 and o.encounter_id = e.encounter_id and o.voided=0 and o.value_coded=1207
        where p.voided =0
        on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);


-- isante database
-- itech
-- form: Followup
-- param: Date du début du traitement TB
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Date du debut du traitement TB' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Followup', e.patientID, p.clinicPatientID, 'Date du debut du traitement TB', e.createDate, o.value_datetime , 'x',date(now())
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=2
  and e.encStatus < 255
  and o.encounter_id=e.encounter_id
  and o.concept_id='163607'
  and e.siteCode =95698
  and ifnull(o.value_datetime, '') <>''
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Followup
-- param: Date du début du traitement TB
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
    select 95698, 'Followup', pid.identifier as patient_id, id.identifier, 'Date du debut du traitement TB', e.date_created, date(o.value_datetime), 'x', date(now())
        from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1113 and o.encounter_id = e.encounter_id and o.voided=0
        where p.voided =0
        on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);


-- isante database
-- itech
-- form: Followup
-- param:'Date dernieres regles
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Date dernieres règles' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, "Followup", e.patientID, p.clinicPatientID, 'Date dernieres règles', e.createDate, date(concat(pregnantLmpYy,'-',pregnantLmpMm,'-',pregnantLmpDd)), 'x', date(now())
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType = 2
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and ifnull(pregnantLmpYy, 0)<>0
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Followup
-- param: Date derniere regles
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
    select 95698, 'Followup', pid.identifier as patient_id, id.identifier, 'Date dernieres règles', e.date_created, date(o.value_datetime), 'x', date(now())
        from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1427 and o.encounter_id = e.encounter_id and o.voided =0
        where p.voided =0
        on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante database
-- itech
-- form: Followup
-- param: Date depistage cancer col
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Date dépistage du cancer col' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, "Followup", e.patientID, p.clinicPatientID, 'Date dépistage du cancer col', e.createDate,  date(concat(papTestYy,'-',papTestMm,'-',papTestDd)), 'x', date(now())
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType = 2
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and ifnull(papTestYy, '') <> ''
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);


-- isanteplus database
-- cepoz
-- form: Followup
-- param: Date depistage cancer col
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
    select 95698, 'Followup', pid.identifier as patient_id, id.identifier, 'Date dépistage cancer col', e.date_created, date(o.value_datetime), 'x', date(now())
        from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =165429 and o.encounter_id = e.encounter_id and o.voided =0
        where p.voided =0
        on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
                when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
                when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
                else 'x'
                end);


-- isante database
-- itech
-- form: Followup
-- param: Autres
-- site code: 95698
delete from replication.validation where form_origin='Followup' and parameter='Autres' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, 'Followup', e.patientID, p.clinicPatientID, 'Autres', e.createDate, 'oui', 'x', date(now())
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType = 2
  and e.encStatus < 255
  and e.patientID = v.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(v.visitDateYy,'-',v.visitDateMm,'-',v.visitDateDd)
  and e.seqnum = v.seqnum
  and e.siteCode = 95698
  and famPlanOther=1
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);


-- isanteplus database
-- cepoz
-- form: Followup
-- param: Autres
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
    select 95698, 'Followup', pid.identifier as patient_id, id.identifier, 'Autres', e.date_created, 'oui', 'x', date(now())
        from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '204ad066-c5c2-4229-9a62-644bc5617ca2' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =374 and o.encounter_id = e.encounter_id and o.value_coded =5622
        where p.voided =0
        on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);




-- -----------------------------------------Fiche de discontinuation----------------------------------------------------------
-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Date de Visite
-- site code: 95698

delete from replication.validation where form_origin='Discontinuation' and parameter='Date de Visite' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, 'Discontinuation', e.patientID, p.clinicPatientID, 'Date de Visite', e.createDate, date(e.visitDate), 'x', date(now())
       from itech.encounter e, itech.patient p
      where e.patientID = p.patientID
        and e.encounterType in (12, 21)
		and e.encStatus<255
        and e.siteCode = 95698
         on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Date de Visite
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Date de Visite', e.date_created, date(e.encounter_datetime), 'x',date(now())
       from cepoz.patient p
<<<<<<< HEAD:src/main/resources/data_bkp.sql
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '9d0113c6-f23a-4461-8428-7e9a7344f2ba' and e.voided =0
=======
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '9d0113c6-f23a-4461-8428-7e9a7344f2ba' and e.voided =0
>>>>>>> d5ac40cc26be941c3f6dfa7f261bde22667bda1c:src/main/resources/backup/data_bkp.sql
      where  p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Date d'arret de traitement VIH
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, 'Discontinuation', e.patientID as patient_id, p.clinicPatientID, "Date d'arret de traitement VIH",e.createDate, if( ifnull(de.disEnrollYy,'')='' and ifnull(de.disEnrollMm,'')='' and ifnull(de.disEnrollDd,'')='', '', date(concat(de.disEnrollYy,'-', de.disEnrollMm,'-', de.disEnrollDd))), 'x',date(now())
	   from itech.patient p, itech.encounter e, itech.discEnrollment de
	  where p.patientID = e.patientID
	    and e.patientID = de.patientID
	    and e.siteCode = de.siteCode
	    and e.encStatus<255
	    and e.siteCode='95698'
	    and concat(e.visitdateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(de.visitdateYy,'-',de.visitDateMm,'-',de.visitDateDd)
	    and e.seqNum = de.seqNum
	    and (de.disEnrollYy <> "" and de.disEnrollYy is not null)
	      on duplicate key update isante_value = values(isante_value), is_valid = (case
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
        else 'x'
        end);

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Date d'arret de traitement VIH
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, "Date d'arret de traitement VIH", e.date_created, date(o.value_datetime), 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id left outer join cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '9d0113c6-f23a-4461-8428-7e9a7344f2ba' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =164094 and o.encounter_id = e.encounter_id and o.voided =0
      where  p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Date du dernier contact avec le patient
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, 'Discontinuation', e.patientID as patient_id, p.clinicPatientID, "Date du dernier contact avec le patient", e.createDate, itech.formatDate(de.lastContactYy,de.lastContactMm,de.lastContactDd), 'x',date(now())
       from itech.patient p, itech.encounter e, itech.discEnrollment de
      where p.patientID = e.patientID
        and e.patientID = de.patientID
        and e.siteCode = de.siteCode
        and e.encStatus<255
        and e.siteCode='95698'
        and e.seqNum = de.seqNum
        and ifnull(de.lastContactYy,0)>0
        and concat(e.visitdateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(de.visitdateYy,'-',de.visitDateMm,'-',de.visitDateDd)
          on duplicate key update isante_value = values(isante_value), is_valid = (case
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
        else 'x'
        end);

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Date du dernier contact avec le patient
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, "Date du dernier contact avec le patient", e.date_created, date(o.value_datetime), 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '9d0113c6-f23a-4461-8428-7e9a7344f2ba' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =164093 and o.encounter_id = e.encounter_id and o.voided =0
      where  p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Perdu de Vue de plus de trois ans
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, 'Discontinuation', e.patientID as patient_id, p.clinicPatientID, 'Raison: Perdu de Vue de plus de trois ans', e.createDate, 'oui', 'x',date(now())
       from itech.patient p, itech.encounter e, itech.discEnrollment de
	  where p.patientID = e.patientID
	    and e.patientID = de.patientID
	    and e.siteCode = de.siteCode
	    and e.siteCode='95698'
	    and e.encStatus<255
	    and concat(e.visitdateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(de.visitdateYy,'-',de.visitDateMm,'-',de.visitDateDd)
	    and e.seqNum = de.seqNum
	    and (de.reasonDiscNoFollowup=1)
	      on duplicate key update isante_value = values(isante_value), is_valid = (case
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
        else 'x'
        end);

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Perdu de Vue de plus de trois ans
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Raison: Perdu de Vue de plus de trois ans', e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '9d0113c6-f23a-4461-8428-7e9a7344f2ba' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =161555 and o.encounter_id = e.encounter_id and o.value_coded =5240 and o.voided =0
      where  p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: emigration
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, 'Discontinuation', e.patientID as patient_id, p.clinicPatientID, 'Raison: emigration', e.createDate, 'oui', 'x',date(now())
       from itech.patient p,itech.encounter e,itech.obs o
      where p.patientID=e.patientID
        and e.encounter_id=o.encounter_id
        and e.encStatus<255
        and o.concept_id=163623
        and o.value_boolean=1
        and e.siteCode=95698
          on duplicate key update isante_value = values(isante_value), is_valid = (case
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
        else 'x'
        end);

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: emigration
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Raison: emigration', e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '9d0113c6-f23a-4461-8428-7e9a7344f2ba' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =161555 and o.encounter_id = e.encounter_id and o.value_coded =160415 and o.voided =0
      where  p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Transfert vers un autre etablissement
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, 'Discontinuation',e.patientID as patient_id, p.clinicPatientID, 'Raison: Transfert vers un autre etablissement', e.createDate,'oui', 'x',date(now())
       from itech.patient p, itech.encounter e, itech.discEnrollment de
	  where p.patientID = e.patientID
	    and e.patientID = de.patientID
	    and e.siteCode = de.siteCode
	    and e.siteCode =95698
	    and e.encStatus<255
	    and concat(e.visitdateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(de.visitdateYy,'-',de.visitDateMm,'-',de.visitDateDd)
	    and e.seqNum = de.seqNum
	    and de.reasonDiscTransfer=1
          on duplicate key update isante_value = values(isante_value), is_valid = (case
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
        else 'x'
        end);

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Transfert vers un autre etablissement
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Raison: Transfert vers un autre etablissement',e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '9d0113c6-f23a-4461-8428-7e9a7344f2ba' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =161555 and o.encounter_id = e.encounter_id and o.value_coded =159492  and o.voided =0
      where  p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Preference du patient
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, 'Discontinuation',e.patientID as patient_id, p.clinicPatientID, 'Raison: Preference du patient', e.createDate,'oui', 'x',date(now())
       from itech.patient p, itech.encounter e, itech.discEnrollment de
      where p.patientID = e.patientID
        and e.patientID = de.patientID
        and e.siteCode = de.siteCode
        and e.siteCode =95698
        and e.encStatus<255
        and e.seqNum = de.seqNum
        and de.reasonDiscRef=1
        and concat(e.visitdateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(de.visitdateYy,'-',de.visitDateMm,'-',de.visitDateDd)
          on duplicate key update isante_value = values(isante_value), is_valid = (case
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
        else 'x'
        end);

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Preference du patient
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Raison: Preference du patient', e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '9d0113c6-f23a-4461-8428-7e9a7344f2ba' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =164089 and o.encounter_id = e.encounter_id and o.value_coded =162571 and o.voided =0
      where  p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param:Reference du medecin
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, 'Discontinuation',e.patientID as patient_id, p.clinicPatientID, 'Raison: Reference du medecin', e.createDate,'oui', 'x',date(now())
       from itech.patient p, itech.encounter e, itech.discEnrollment de
	  where p.patientID = e.patientID
	    and e.patientID = de.patientID
	    and e.siteCode = de.siteCode
	    and e.siteCode =95698
	    and e.encStatus<255
	    and concat(e.visitdateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(de.visitdateYy,'-',de.visitDateMm,'-',de.visitDateDd)
	    and e.seqNum = de.seqNum
	    and de.reasonDiscRef=2
          on duplicate key update isante_value = values(isante_value), is_valid = (case
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
        else 'x'
        end);

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param:Reference du medecin
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Raison: Reference du medecin', e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '9d0113c6-f23a-4461-8428-7e9a7344f2ba' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =161555 and o.encounter_id = e.encounter_id and o.value_coded =162591
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Tuberculose
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, 'Discontinuation', e.patientID as patient_id, p.clinicPatientID, 'Decès: Tuberculose', e.createDate, 'oui', 'x',date(now())
       from itech.patient p,itech.encounter e,itech.obs o
      where p.patientID=e.patientID
        and e.encounter_id=o.encounter_id
        and e.encStatus<255
        and o.concept_id=163610
        and o.value_boolean=1
        and e.siteCode=95698
          on duplicate key update isante_value = values(isante_value), is_valid = (case
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
        else 'x'
        end);

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Tuberculose
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Decès: Tuberculose',e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '9d0113c6-f23a-4461-8428-7e9a7344f2ba' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1748 and o.encounter_id = e.encounter_id and o.value_coded =112141 and o.voided =0
      where  p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Maladies infectieuses et/ou parasitaires liees au VIH
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, 'Discontinuation', e.patientID as patient_id, p.clinicPatientID, 'Decès: Maladies infectieuses et/ou parasitaires liees au VIH', e.createDate, 'oui', 'x',date(now())
       from itech.patient p,itech.encounter e,itech.obs o
      where p.patientID=e.patientID
        and e.encounter_id=o.encounter_id
        and e.encStatus<255
        and o.concept_id=163611
        and o.value_boolean=1
        and e.siteCode=95698
          on duplicate key update isante_value = values(isante_value), is_valid = (case
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
        else 'x'
        end);

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Maladies infectieuses et/ou parasitaires liees au VIH
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Decès: Maladies infectieuses et/ou parasitaires liees au VIH',e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '9d0113c6-f23a-4461-8428-7e9a7344f2ba' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1748 and o.encounter_id = e.encounter_id and o.value_coded =112141 and o.voided =0
      where  p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Cancer lie au VIH
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, 'Discontinuation', e.patientID as patient_id, p.clinicPatientID, 'Decès: Cancer lie au VIH', e.createDate, 'oui', 'x',date(now())
       from itech.patient p,itech.encounter e,itech.obs o
      where p.patientID=e.patientID
        and e.encounter_id=o.encounter_id
        and e.encStatus<255
        and o.concept_id=163612
        and o.value_boolean=1
        and e.siteCode=95698
          on duplicate key update isante_value = values(isante_value), is_valid = (case
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
        else 'x'
        end);

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Cancer lie au VIH
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Decès: Cancer lie au VIH', e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '9d0113c6-f23a-4461-8428-7e9a7344f2ba' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1748 and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.concept c on c.concept_id=o.concept_id and  c.uuid='8efa7daf-0fb1-49bd-8e1e-1702334246ba'
      where  p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Autres maladies ou conditions liees au VIH
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, 'Discontinuation', e.patientID as patient_id, p.clinicPatientID, 'Decès: Autres maladies ou conditions liees au VIH', e.createDate, 'oui', 'x',date(now())
       from itech.patient p,itech.encounter e,itech.obs o
      where p.patientID=e.patientID
        and e.encounter_id=o.encounter_id
        and e.encStatus<255
        and o.concept_id=163613
        and o.value_boolean=1
        and e.siteCode=95698
          on duplicate key update isante_value = values(isante_value), is_valid = (case
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
        else 'x'
        end);

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Autres maladies ou conditions liees au VIH
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Decès: Autres maladies ou conditions liees au VIH', e.date_created, 'oui' , 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '9d0113c6-f23a-4461-8428-7e9a7344f2ba' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1748 and o.encounter_id = e.encounter_id and o.value_coded= 5622 and o.voided =0
      where  p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Causes naturelles (cancer et infections, etc ) non liees au VIH
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, 'Discontinuation', e.patientID as patient_id, p.clinicPatientID, 'Decès: Causes naturelles (cancer et infections, etc ) non liees au VIH', e.createDate, 'oui', 'x',date(now())
       from itech.patient p,itech.encounter e,itech.obs o
      where p.patientID=e.patientID
        and e.encounter_id=o.encounter_id
        and e.encStatus<255
        and o.concept_id=163614
        and o.value_boolean=1
        and e.siteCode=95698
          on duplicate key update isante_value = values(isante_value), is_valid = (case
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
        else 'x'
        end);

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Causes naturelles (cancer et infections, etc ) non liees au VIH
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Decès: Causes naturelles (cancer et infections, etc ) non liees au VIH', e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '9d0113c6-f23a-4461-8428-7e9a7344f2ba' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1748 and o.encounter_id = e.encounter_id and o.value_coded= 133481 and o.voided =0
      where  p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Causes non naturelles (traumatisme, accident, suicide, homicide, guerre, etc)
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, 'Discontinuation', e.patientID as patient_id, p.clinicPatientID, 'Decès: Causes non naturelles (traumatisme, accident, suicide, homicide, guerre, etc)', e.createDate, 'oui', 'x',date(now())
       from itech.patient p,itech.encounter e,itech.obs o
      where p.patientID=e.patientID
        and e.encounter_id=o.encounter_id
        and e.encStatus<255
        and o.concept_id=163615
        and o.value_boolean=1
        and e.siteCode=95698
          on duplicate key update isante_value = values(isante_value), is_valid = (case
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
        else 'x'
        end);

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Causes non naturelles (traumatisme, accident, suicide, homicide, guerre, etc)
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Decès: Causes non naturelles (traumatisme, accident, suicide, homicide, guerre, etc)', e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '9d0113c6-f23a-4461-8428-7e9a7344f2ba' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1748 and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.concept c on c.concept_id=o.concept_id and c.uuid='768778dc-ce6a-47a0-8f78-b0777c8cf081'
      where  p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Inconnu
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, 'Discontinuation', e.patientID as patient_id, p.clinicPatientID, 'Decès: Inconnu', e.createDate, 'oui', 'x',date(now())
       from itech.patient p,itech.encounter e,itech.obs o
      where p.patientID=e.patientID
        and e.encounter_id=o.encounter_id
        and e.encStatus<255
        and o.concept_id=163616
        and o.value_boolean=1
        and e.siteCode=95698
          on duplicate key update isante_value = values(isante_value), is_valid = (case
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
        else 'x'
        end);

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Inconnu
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Decès: Inconnu', e.date_created,'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '9d0113c6-f23a-4461-8428-7e9a7344f2ba' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1748 and o.encounter_id = e.encounter_id and o.voided =0 and o.value_coded= 1067
      where  p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Preference du patient
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, 'Discontinuation',e.patientID as patient_id, p.clinicPatientID, 'Arrêt: Preference du patient ou de la personne responsable', e.createDate,'oui', 'x',date(now())
       from itech.patient p, itech.encounter e, itech.discEnrollment de
	  where p.patientID = e.patientID
	    and e.patientID = de.patientID
	    and e.siteCode = de.siteCode
	    and e.siteCode =95698
	    and e.encStatus<255
	    and concat(e.visitdateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(de.visitdateYy,'-',de.visitDateMm,'-',de.visitDateDd)
	    and e.seqNum = de.seqNum
	    and de.patientPreference=1
          on duplicate key update isante_value = values(isante_value), is_valid = (case
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
        else 'x'
        end);

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Preference du patient
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Arrêt: Preference du patient ou de la personne responsable',e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '9d0113c6-f23a-4461-8428-7e9a7344f2ba' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1667 and o.encounter_id = e.encounter_id and o.value_coded= 159737 and o.voided =0
      where  p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Decision du prestataire
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, 'Discontinuation', e.patientID as patient_id, p.clinicPatientID, 'Arrêt: Decision du prestataire', e.createDate, 'oui', 'x',date(now())
       from itech.patient p,itech.encounter e,itech.obs o
      where p.patientID=e.patientID
        and e.encounter_id=o.encounter_id
        and e.encStatus<255
        and o.concept_id=163617
        and o.value_boolean=1
        and e.siteCode=95698
          on duplicate key update isante_value = values(isante_value), is_valid = (case
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
        else 'x'
        end);

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Decision du prestataire
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Arrêt: Decision du prestataire',e.date_created,'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '9d0113c6-f23a-4461-8428-7e9a7344f2ba' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1667 and o.encounter_id = e.encounter_id and o.value_coded= 162591 and o.voided =0
      where  p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Adherence inadequate
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, 'Discontinuation',e.patientID as patient_id, p.clinicPatientID, 'Adherence inadequate', e.createDate,'oui', 'x',date(now())
       from itech.patient p, itech.encounter e, itech.discEnrollment de
	  where p.patientID = e.patientID
	    and e.patientID = de.patientID
	    and e.siteCode = de.siteCode
	    and e.siteCode =95698
	    and e.encStatus<255
	    and concat(e.visitdateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(de.visitdateYy,'-',de.visitDateMm,'-',de.visitDateDd)
	    and e.seqNum = de.seqNum
	    and de.poorAdherence=1
          on duplicate key update isante_value = values(isante_value), is_valid = (case
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
        else 'x'
        end);

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Adherence inadequate
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Adherence inadequate', e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '9d0113c6-f23a-4461-8428-7e9a7344f2ba' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1667 and o.encounter_id = e.encounter_id and o.value_coded= 115198 and o.voided =0
      where  p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Deni
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, 'Discontinuation', e.patientID as patient_id, p.clinicPatientID, 'Deni', e.createDate, 'oui', 'x',date(now())
       from itech.patient p,itech.encounter e,itech.obs o
      where p.patientID=e.patientID
        and e.encounter_id=o.encounter_id
        and e.encStatus<255
        and o.concept_id=163618
        and o.value_boolean=1
        and e.siteCode=95698
          on duplicate key update isante_value = values(isante_value), is_valid = (case
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
        else 'x'
        end);

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Deni
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Deni',e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '9d0113c6-f23a-4461-8428-7e9a7344f2ba' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1667 and o.encounter_id = e.encounter_id and o.value_coded= 155891 and o.voided =0
      where  p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Troubles psychiatriques et/ou psychologiques
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, 'Discontinuation', e.patientID as patient_id, p.clinicPatientID, 'Troubles psychiatriques et/ou psychologiques', e.createDate, 'oui', 'x',date(now())
       from itech.patient p,itech.encounter e,itech.obs o
      where p.patientID=e.patientID
        and e.encounter_id=o.encounter_id
        and e.encStatus<255
        and o.concept_id=163619
        and o.value_boolean=1
        and e.siteCode=95698
          on duplicate key update isante_value = values(isante_value), is_valid = (case
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
        else 'x'
        end);

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Troubles psychiatriques et/ou psychologiques
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Troubles psychiatriques et/ou psychologiques',e.date_created,'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '9d0113c6-f23a-4461-8428-7e9a7344f2ba' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1667 and o.encounter_id = e.encounter_id and o.value_coded= 134337 and o.voided =0
      where  p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Autre raison
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, 'Discontinuation',e.patientID as patient_id, p.clinicPatientID, 'Autre raison', e.createDate,'oui', 'x',date(now())
       from itech.patient p, itech.encounter e, itech.discEnrollment de
	  where p.patientID = e.patientID
	    and e.patientID = de.patientID
	    and e.siteCode = de.siteCode
	    and e.siteCode =95698
	    and e.encStatus<255
	    and concat(e.visitdateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(de.visitdateYy,'-',de.visitDateMm,'-',de.visitDateDd)
	    and e.seqNum = de.seqNum
	    and de.discReasonOther=1
          on duplicate key update isante_value = values(isante_value), is_valid = (case
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
        else 'x'
        end);

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Autre raison
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Autre raison',e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '9d0113c6-f23a-4461-8428-7e9a7344f2ba' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1667 and o.encounter_id = e.encounter_id and o.voided =0 and o.value_coded= 5622
      where  p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);


-- ----------------------------------------------------------Fiche Prescription--------------------------------------------------------------------
-- consolidated isante database
-- itech
-- form: Prescription
-- param:  Date de visite
-- site code: 95698

delete from replication.validation where form_origin='Prescription' and parameter='Date de visite' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Date de visite', e.createDate, date(concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)), 'x', date(now())
       from itech.encounter e, itech.patient p
      where e.patientID = p.patientID
        and e.encounterType=5
        and e.siteCode =95698
        and e.encStatus < 255
         on duplicate key update isante_value = values(isante_value), is_valid = (case
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
        else 'x'
        end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Date de visite
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Date de visite', e.date_created, if(e.encounter_datetime is null, '',date(e.encounter_datetime)) as 'date_visite', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Date d'initiation ARV
-- site code: 95698

delete from replication.validation where form_origin='Prescription' and parameter="Date d'initiation ARV" and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, 'Prescription', e.patientID, p.clinicPatientID, "Date d'initiation ARV", e.createDate,
      date(concat(t.arvStartDateYy,'-', t.arvStartDateMm,'-', t.arvStartDateDd)), 'x', date(now())
       from itech.encounter e, itech.patient p, itech.prescriptionOtherFields  t
	  where e.patientID = p.patientID
        and p.patientID=t.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(t.visitDateYy,'-',t.visitDateMm,'-',t.visitDateDd)
        and e.seqnum=t.seqnum
        and e.encounterType=5
        and e.siteCode =95698
        and e.encStatus < 255
        and ifnull(t.arvStartDateYy,'')<>''
         on duplicate key update isante_value = values(isante_value), is_valid = (case
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
        else 'x'
        end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Date d'initiation ARV
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, "Date d'initiation ARV", e.date_created,
     if(o.value_datetime is null, '',date(o.value_datetime)) as 'date_initiation_arv', 'x', date(now())
       from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =159599 and o.encounter_id = e.encounter_id and o.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

 -- consolidated isante database'
-- itech
-- form: Prescription
-- param: Regime de 1ere ligne
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Regime de 1ere ligne', e.createDate, 'oui', 'x', date(now())
	   from itech.encounter e, itech.patient p,itech.obs  o
	  where e.patientID = p.patientID
        and o.encounter_id=e.encounter_id
        and o.concept_id=163608
        and o.value_numeric=1
		and e.encounterType=5
		and e.siteCode =95698
        and e.encStatus < 255
         on duplicate key update isante_value = values(isante_value), is_valid = (case
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
        else 'x'
        end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Regime de 1ere ligne
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Regime de 1ere ligne', e.date_created, 'oui' as 'regime_1ere_ligne', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =164432 and o.encounter_id = e.encounter_id and o.voided =0
    inner join cepoz.concept c on c.concept_id = o.concept_id  and c.uuid ='dd69cffe-d7b8-4cf1-bc11-3ac302763d48'
        where p.voided =0
           on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
         when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
         when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
         else 'x'
          end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Regime de 2e ligne
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Regime de 2eme ligne', e.createDate, o.value_numeric, 'x', date(now())
	   from itech.encounter e, itech.patient p,itech.obs  o
	  where e.patientID = p.patientID
        and o.encounter_id=e.encounter_id
        and o.concept_id=163608
        and o.value_numeric=2
		and e.encounterType=5
		and e.siteCode =95698
        and e.encStatus < 255
         on duplicate key update isante_value = values(isante_value), is_valid = (case
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
        else 'x'
        end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Regime de 2eme ligne
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Regime de 2eme ligne', e.date_created, 'oui' as 'regime_2eme_ligne', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =164432 and o.encounter_id = e.encounter_id and o.voided =0
    inner join cepoz.concept c on c.concept_id = o.concept_id  and c.uuid ='77488a7b-957f-4ebc-892a-e53e7c910363'
         where p.voided =0
            on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
          when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
          when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
          else 'x'
           end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Regime de 3e ligne
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Regime de 3eme ligne', e.createDate, 'oui', 'x', date(now())
	   from itech.encounter e, itech.patient p,itech.obs  o
	  where e.patientID = p.patientID
        and o.encounter_id=e.encounter_id
        and o.concept_id=163608
        and o.value_numeric=4
		and e.encounterType=5
		and e.siteCode =95698
        and e.encStatus < 255
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Regime de 3eme ligne
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Regime de 3eme ligne', e.date_created, 'oui' as 'regime_3eme_ligne', 'x', date(now())
       from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
inner join cepoz.encounter e on e.patient_id = p.patient_id
inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =164432 and o.encounter_id = e.encounter_id and o.voided =0
inner join cepoz.concept c on c.concept_id = o.concept_id  and c.uuid ='99d88c3e-00ad-4122-a300-a88ff5c125c9'
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: ABC RX  prophy
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'ABC RX Prophy', e.createDate,
  case when pr.forPepPmtct=1 then 'ABC Prophy'
	   when pr.forPepPmtct=2 then 'ABC RX'
	   else ''
		end, 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=1
		and e.encounterType=5
		and e.siteCode =95698
        and e.encStatus < 255
        and pr.forPepPmtct in(1,2)
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: ABC  RX  Prophy
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'ABC RX Prophy', e.date_created,
case
       when o.value_coded =138405 then 'ABC RX'
       when o.value_coded =163768 then 'ABC Prophy'
end
     as 'abc_rx_prophy', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160742 and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 70056
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: 3TC  RX  prophy
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, '3TC RX prophy', e.createDate,
  case when pr.forPepPmtct=1 then '3TC Prophy'
	   when pr.forPepPmtct=2 then '3TC RX'
	   end, 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=20
		and e.encounterType=5
		and e.siteCode =95698
		and e.encStatus < 255
		and pr.forPepPmtct in (1,2)
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param:3TC  RX  Prophy
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, '3TC RX Prophy', e.date_created,
case
       when o.value_coded =138405 then '3TC RX'
       when o.value_coded =163768 then '3TC Prophy'
end
     as '3tc_rx_prophy', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160742 and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 78643
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: FTC RX prophy
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'FTC RX Prophy', e.createDate,
  case
       when pr.forPepPmtct=1 then 'FTC Prophy'
	   when pr.forPepPmtct=2 then 'FTC RX'
  end, 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=12
		and e.encounterType=5
		and e.encStatus < 255
		and e.siteCode =95698
		and pr.forPepPmtct in (1,2)
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: FTC  RX  Prophy
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'FTC RX Prophy', e.date_created,
case
       when o.value_coded =138405 then 'FTC RX'
       when o.value_coded =163768 then 'FTC Prophy'
end
     as 'ftc_rx_prophy', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160742 and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 75628
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: TNF RX prophy
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'TNF RX Prophy', e.createDate,
  case when pr.forPepPmtct=1 then 'TNF Prophy'
	   when pr.forPepPmtct=2 then 'TNF RX'
	   else ''
	   end, 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=31
		and e.encounterType=5
		and e.encStatus < 255
		and e.siteCode =95698
		and pr.forPepPmtct in (1,2)
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param:TNF  RX  Prophy
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'TNF RX Prophy', e.date_created,
case
       when o.value_coded =138405 then 'TNF RX'
       when o.value_coded =163768 then 'TNF Prophy'
end
     as 'tnf_rx_prophy', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160742 and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 84795
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: AZT RX prophy
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'AZT RX prophy', e.createDate,
  case when pr.forPepPmtct=1 then 'AZT Prophy'
	   when pr.forPepPmtct=2 then 'AZT RX'
	   else ''
	   end, 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=34
		and e.encounterType=5
		and e.encStatus < 255
		and e.siteCode =95698
		and pr.forPepPmtct in (1,2)
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: AZT  RX  Prophy
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'AZT RX Prophy', e.date_created,
case
       when o.value_coded =138405 then 'AZT RX'
       when o.value_coded =163768 then 'AZT Prophy'
end
     as 'azt_rx_prophy', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160742 and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 86663
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: EFV rx prophy
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'EFV RX Prophy', e.createDate,
  case
       when pr.forPepPmtct=1 then 'EFV Prophy'
	   when pr.forPepPmtct=2 then 'EFV RX'
  end, 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=11
		and e.encounterType=5
		and e.encStatus < 255
		and e.siteCode =95698
		and pr.forPepPmtct in (1,2)
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: EFV  Rx  Prophy
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'EFV RX Prophy', e.date_created,
case
       when o.value_coded =138405 then 'EFV RX'
       when o.value_coded =163768 then 'EFV Prophy'
end
     as 'efv_rx_prophy', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160742 and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 75523
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Atazanavir + BostRTV RX Prophy
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Atazanavir + BostRTV RX Prophy', e.createDate,
  case
       when pr.forPepPmtct=1 then 'Atazanavir + BostRTV Prophy'
	   when pr.forPepPmtct=2 then 'Atazanavir + BostRTV RX'
  end, 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateyy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateyy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=6
		and e.encounterType=5
		and e.encStatus < 255
		and e.siteCode =95698
		and pr.forPepPmtct in (1,2)
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Atazanavir + BostRTV  Rx  Prophy
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Atazanavir + BostRTV Rx Prophy', e.date_created,
case
       when o.value_coded =138405 then 'Atazanavir + BostRTV RX'
       when o.value_coded =163768 then 'Atazanavir + BostRTV Prophy'
end
     as 'atazanavir_bostRTV_rx_prophy', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160742 and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 159809
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param:Lopinavir + BostRTV RX Prophy
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Lopinavir + BostRTV RX Prophy', e.createDate,
  case when pr.forPepPmtct=1 then 'Lopinavir + BostRTV Prophy'
	   when pr.forPepPmtct=2 then 'Lopinavir + BostRTV RX'
	   end, 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=21
		and e.encounterType=5
		and e.encStatus < 255
		and e.siteCode =95698
		and pr.forPepPmtct in (1,2)
	       on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Lopinavir + BostRTV Rx  Prophy
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Lopinavir + BostRTV RX Prophy', e.date_created,
case
       when o.value_coded =138405 then 'Lopinavir + BostRTV RX'
       when o.value_coded =163768 then 'Lopinavir + BostRTV Prophy'
end
     as 'lopinavir_bostRTV_rx_prophy', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160742 and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 794
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Raltegravir RX Prophy
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Raltegravir RX Prophy', e.createDate,
  case when pr.forPepPmtct=1 then 'Raltegravir Prophy'
	   when pr.forPepPmtct=2 then 'Raltegravir RX'
	   end, 'x', date(now())
	   from itech.encounter e, itech.patient p,itech.prescriptions  pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=87
		and e.encounterType=5
		and e.encStatus < 255
		and e.siteCode =95698
		and pr.forPepPmtct in (1,2)
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Raltegravir Rx  Prophy
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Raltegravir RX Prophy', e.date_created,
case
       when o.value_coded =138405 then 'Raltegravir RX'
       when o.value_coded =163768 then  'Raltegravir Prophy'
end
     as 'raltegravir_rx_prophy', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160742 and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 154378
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Dolutegravir RX Prophy
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Dolutegravir RX Prophy', e.createDate,
  case
       when pr.forPepPmtct=1 then 'Dolutegravir Prophy'
	   when pr.forPepPmtct=2 then 'Dolutegravir RX'
  end, 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateyy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateyy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=89
		and e.encounterType=5
		and e.encStatus < 255
		and e.siteCode =95698
		and pr.forPepPmtct in (1,2)
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Dolutegravir Rx  Prophy
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Dolutegravir RX Prophy', e.date_created,
case
       when o.value_coded =138405 then 'Dolutegravir RX'
       when o.value_coded =163768 then  'Dolutegravir Prophy'
end as 'dolutegravir_rx_prophy', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160742 and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 165085
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Cotrimoxazole RX Prophy
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Cotrimoxazole RX Prophy', e.createDate,
  case
       when pr.forPepPmtct=1 then 'Cotrimoxazole Prophy'
	   when pr.forPepPmtct=2 then 'Cotrimoxazole RX'
  end, 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateyy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateyy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=9
		and e.encounterType=5
		and e.encStatus < 255
		and e.siteCode =95698
		and pr.forPepPmtct in (1,2)
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Cotrimoxazole Rx Prophy
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Cotrimoxazole RX Prophy', e.date_created,
case
       when o.value_coded =138405 then 'Cotrimoxazole RX'
       when o.value_coded =163768 then  'Cotrimoxazole Prophy'
end
     as 'cotrimoxazole_rx_prophy', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160742 and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 105281
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Isoniazide (INH) RX Prophy
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Isoniazide (INH) RX Prophy', e.createDate,
  case
  	   when pr.forPepPmtct=1 then 'Isoniazide (INH) Prophy'
	   when pr.forPepPmtct=2 then 'Isoniazide (INH) RX'
 end, 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateyy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateyy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=18
		and e.encounterType=5
		and e.encStatus < 255
		and e.siteCode =95698
		and pr.forPepPmtct in (1,2)
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Isoniazide (INH) Rx  Prophy
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Isoniazide (INH) RX Prophy', e.date_created,
case
       when o.value_coded =138405 then 'Isoniazide (INH) RX'
       when o.value_coded =163768 then 'Isoniazide (INH) Prophy'
end
     as 'inh_rx_prophy', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160742 and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 78280
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: INH nombre de jours
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'INH nombre de jours', e.createDate, pr.numDaysDesc, 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateyy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateyy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=18
		and e.encounterType=5
		and e.encStatus < 255
		and e.siteCode =95698
        and ifnull(pr.numDaysDesc,0)>0
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: INH nombre de jours
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'INH nombre de jours', e.date_created, o.value_numeric as 'nbre_jrs_inh', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =159368 and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 78280
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Medicament ABC dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Medicament ABC dispense', e.createDate,
  case
       when pr.dispensed=1 then 'oui'
	   when pr.dispensed=2 then 'Non'
  end, 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateyy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateyy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=1
		and e.encounterType=5
		and e.encStatus < 255
		and e.siteCode =95698
		 and pr.dispensed in (1,2)
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Medicament ABC dispense
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Medicament ABC dispense', e.date_created,
case
       when o.value_coded =1 then 'oui'
end
     as 'medicament_dispense_abc', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 70056
 where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Medicament 3TC dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Medicament 3TC dispense',  e.createDate,
  case
       when pr.dispensed=1 then 'oui'
	   when pr.dispensed=2 then 'Non'
 end, 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateyy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateyy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=20
		and e.encounterType=5
		and e.encStatus < 255
		and e.siteCode =95698
		and pr.dispensed in (1,2)
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Medicament 3TC dispense
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Medicament 3TC dispense', e.date_created,
case
       when o.value_coded =1 then 'oui'
end
     as 'medicament_dispense_3tc', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 78643
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Medicament FTC dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Medicament FTC dispense', e.createDate,
  case
       when pr.dispensed=1 then 'oui'
	   when pr.dispensed=2 then 'Non'
 end, 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateyy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateyy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=12
		and e.encounterType=5
		and e.encStatus < 255
		and e.siteCode =95698
		and pr.dispensed in (1,2)
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Medicament FTC dispense
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Medicament FTC dispense', e.date_created,
case
       when o.value_coded =1 then 'oui'
end
     as 'medicament_dispense_ftc', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 75628
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Medicament TNF dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Medicament TNF dispense', e.createDate,
  case when pr.dispensed=1 then 'oui'
	   when pr.dispensed=2 then 'Non'
	   end, 'x', date(now())
       from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateyy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateyy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=31
		and e.encounterType=5
		and e.encStatus < 255
		and e.siteCode =95698
		and pr.dispensed in (1,2)
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Medicament TNF dispense
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Medicament TNF dispense', e.date_created,
case
       when o.value_coded =1 then 'oui'
end
     as 'medicament_dispense_tnf', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 84795
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Medicament AZT dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Medicament AZT dispense', e.createDate,
  case when pr.dispensed=1 then 'oui'
	   when pr.dispensed=2 then 'Non'
	   else ''
	   end, 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateyy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateyy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=34
		and e.encounterType=5
		and e.encStatus < 255
		and e.siteCode =95698
		and pr.dispensed in (1,2)
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Medicament AZT dispense
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Medicament AZT dispense', e.date_created,
case
       when o.value_coded =1 then 'oui'
end
     as 'medicament_dispense_azt', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 86663
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Medicament EFV dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Medicament EFV dispense', e.createDate,
  case
  	   when pr.dispensed=1 then 'oui'
	   when pr.dispensed=2 then 'Non'
 end, 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateyy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateyy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=11
		and e.encounterType=5
		and e.encStatus < 255
		and e.siteCode =95698
		and pr.dispensed in (1,2)
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Medicament dispense EFV
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Medicament EFV dispense', e.date_created,
case
       when o.value_coded =1 then 'oui'
end
     as 'medicament_dispense_efv', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 75523
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Medicament Atazanavir + BostRTV dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Medicament Atazanavir + BostRTV dispense', e.createDate,
  case
       when pr.dispensed=1 then 'oui'
	   when pr.dispensed=2 then 'Non'
  end, 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateyy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateyy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=6
		and e.encounterType=5
		and e.encStatus < 255
		and e.siteCode =95698
		and pr.dispensed in (1,2)
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Medicament Atazanavir + BostRTV dispense
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Medicament Atazanavir + BostRTV dispense', e.date_created,
case
       when o.value_coded =1 then 'oui'
end
     as 'medicament_dispense_atazanavir_bostRTV', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 159809
 where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: lopinavir + bostrtv dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Medicament Lopinavir + BostRTV dispense', e.createDate,
  case
       when pr.dispensed=1 then 'oui'
	   when pr.dispensed=2 then 'Non'
  end, 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateyy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateyy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=21
		and e.encounterType=5
		and e.encStatus < 255
		and e.siteCode =95698
		and pr.dispensed in (1,2)
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Medicament Lopinavir + BostRTV dispense
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Medicament Lopinavir + BostRTV dispense', e.date_created,
case
       when o.value_coded =1 then 'oui'
end
     as 'medicament_dispense_lopinavir_bostRTV', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 794
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Medicament Raltegravir dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Medicament Raltegravir dispense', e.createDate,
  case
       when pr.dispensed=1 then 'oui'
	   when pr.dispensed=2 then 'Non'
   end, 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=87
		and e.encounterType=5
        and e.siteCode=95698
        and e.encStatus < 255
		and pr.dispensed in (1,2)
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Medicament Raltegravir dispense
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Medicament Raltegravir dispense', e.date_created,
case
       when o.value_coded =1 then 'oui'
end
     as 'medicament_dispense_raltegravir', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 154378
 where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Medicament Dolutegravir dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Medicament Dolutegravir dispense', e.createDate,
  case
       when pr.dispensed=1 then 'oui'
	   when pr.dispensed=2 then 'non'
  end, 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=89
		and e.encounterType=5
		and e.siteCode=95698
		and e.encStatus < 255
		and pr.dispensed in (1,2)
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Medicament Dolutegravir dispense
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Medicament Dolutegravir dispense', e.date_created,
case
       when o.value_coded =1 then 'oui'
end
     as 'medicament_dispense_dolutegravir', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 165085
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Medicament Cotrimoxazole dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Medicament Cotrimoxazole dispense', e.createDate,
  case
       when pr.dispensed=1 then 'oui'
	   when pr.dispensed=2 then 'non'
  end, 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=9
		and e.encounterType=5
		and e.encStatus < 255
		and e.siteCode =95698
		and pr.dispensed in (1,2)
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Medicament Cotrimoxazole dispense
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Medicament Cotrimoxazole dispense', e.date_created,
case
       when o.value_coded =1 then 'oui'
end
     as 'medicament_dispense_cotrimoxazole', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 105281
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Medicament Isoniazide(INH) dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Medicament Isoniazide(INH) dispense', e.createDate,
  case
       when pr.dispensed=1 then 'oui'
	   when pr.dispensed=2 then 'non'
  end, 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=18
		and e.encounterType=5
		and e.encStatus < 255
		and e.siteCode =95698
		and pr.dispensed in (1,2)
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Medicament Isoniazide(INH) dispense
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Medicament Isoniazide(INH) dispense', e.date_created,
case
       when o.value_coded =1 then 'oui'
end
     as 'medicament_dispense_isoniazide', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 78280
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Date ABC dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Date ABC dispense', e.createDate,
       date(concat(pr.dispDateYy,'-', pr.dispDateMm,'-', pr.dispDateDd)), 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=1
		and e.encounterType=5
		and e.siteCode=95698
		and e.encStatus < 255
		and ifnull(pr.dispDateYy,0)>0
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Date ABC dispense
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Date ABC dispense', e.date_created, o.obs_datetime as 'date_dispense_abc', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 70056
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Date 3TC dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Date 3TC dispense', e.createDate,
	   date(concat(pr.dispDateYy,'-',pr.dispDateMm,'-',pr.dispDateDd)), 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=20
		and e.encounterType=5
        and e.siteCode=95698
        and e.encStatus < 255
        and ifnull(pr.dispDateYy,0)>0
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Date 3TC dispense
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Date 3TC dispense', e.date_created, date(o.obs_datetime) as 'date_dispense_3tc', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 78643
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Date FTC dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Date FTC dispense', e.createDate,
	   date(concat(pr.dispDateYy,'-',pr.dispDateMm,'-',pr.dispDateDd)), 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=12
		and e.encounterType=5
        and e.siteCode=95698
        and e.encStatus < 255
		and ifnull(pr.dispDateYy,0)>0
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Date FTC dispense
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Date FTC dispense', e.date_created, date(o.obs_datetime) as 'date_dispense_ftc', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 75628
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Date TNF dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Date TNF dispense', e.createDate,
	   date(concat(pr.dispDateYy,'-',pr.dispDateMm,'-',pr.dispDateDd)),'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=31
		and e.encounterType=5
        and e.siteCode=95698
        and e.encStatus < 255
		and ifnull(pr.dispDateYy,0)>0
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Date TNF dispense
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Date TNF dispense', e.date_created, date(o.obs_datetime) as 'date_dispense_tnf', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 84795
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Date AZT dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Date AZT dispense', e.createDate,
	   date(concat(pr.dispDateYy,'-',pr.dispDateMm,'-',pr.dispDateDd)), 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=34
		and e.encounterType=5
        and e.siteCode=95698
        and e.encStatus < 255
		and ifnull(pr.dispDateYy,0)>0
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Date AZT dispense
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Date AZT dispense', e.date_created, date(o.obs_datetime) as 'date_dispense_azt', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 86663
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Date EFV dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Date EFV dispense', e.createDate,
	   date(concat(pr.dispDateYy,'-',pr.dispDateMm,'-',pr.dispDateDd)), 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=11
		and e.encounterType=5
        and e.siteCode=95698
        and e.encStatus < 255
		and ifnull(pr.dispDateYy,0)>0
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Date EFV dispense
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Date EFV dispense', e.date_created, date(o.obs_datetime) as 'date_dispense_efv', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 75523
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param:Date Atazanavir + BostRTV dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Date Atazanavir + BostRTV dispense', e.createDate,
	   date(concat(pr.dispDateYy,'-',pr.dispDateMm,'-',pr.dispDateDd)), 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=6
		and e.encounterType=5
        and e.siteCode=95698
        and e.encStatus < 255
		and ifnull(pr.dispDateYy,0)>0
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Date Atazanavir + BostRTV dispense
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Date Atazanavir + BostRTV dispense', e.date_created, date(o.obs_datetime) as 'date_dispense_atazanavir_bostRTV', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 159809
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Date Lopinavir + BostRTV dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Date Lopinavir + BostRTV dispense', e.createDate,
	   date(concat(pr.dispDateYy,'-',pr.dispDateMm,'-',pr.dispDateDd)), 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=21
		and e.encounterType=5
        and e.siteCode=95698
        and e.encStatus < 255
		and ifnull(pr.dispDateYy,0)>0
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Date Lopinavir + BostRTV' dispense
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Date Lopinavir + BostRTV dispense', e.date_created, date(o.obs_datetime) as 'date_dispense_lopinavir_bostRTV', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 794
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Date Raltegravir dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Date Raltegravir dispense', e.createDate,
	   date(concat(pr.dispDateYy,'-',pr.dispDateMm,'-',pr.dispDateDd)), 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=87
		and e.encounterType=5
        and e.siteCode=95698
        and e.encStatus < 255
		and ifnull(pr.dispDateYy,0)>0
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Date Raltegravir dispense
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Date Raltegravir dispense', e.date_created, date(o.obs_datetime) as 'medicament_dispense_raltegravir', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 154378
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Date Dolutegravir  dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Date Dolutegravir dispense', e.createDate,
       date(concat(pr.dispDateYy,'-',pr.dispDateMm,'-',pr.dispDateDd)), 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=89
		and e.encounterType=5
        and e.siteCode=95698
        and e.encStatus < 255
		and ifnull(pr.dispDateYy,0)>0
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Date Dolutegravir dispense
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Date Dolutegravir dispense', e.date_created, date(o.obs_datetime) as 'date_dispense_dolutegravir', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 165085
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Date Cotrimoxazole dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	 select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Date Cotrimoxazole dispense', e.createDate,
	   date(concat(pr.dispDateYy,'-',pr.dispDateMm,'-',pr.dispDateDd)), 'x', date(now())
	   from itech.encounter e, itech.patient p, itech.prescriptions pr
	  where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=9
		and e.encounterType=5
        and e.siteCode=95698
        and e.encStatus < 255
		and ifnull(pr.dispDateYy,0)>0
		   on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Date Cotrimoxazole dispense
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Date Cotrimoxazole dispense', e.date_created, date(o.obs_datetime) as 'date_dispense_cotrimoxazole', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 105281
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Prescription
-- param: Date Isoniazide(INH) dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
	select 95698, "Prescription", e.patientID, p.clinicPatientID, 'Date Isoniazide(INH) dispense', e.createDate,
	  date(concat(pr.dispDateYy,'-',pr.dispDateMm,'-',pr.dispDateDd)), 'x', date(now())
	from itech.encounter e, itech.patient p,itech.prescriptions  pr
		where e.patientID = p.patientID
            and pr.patientID=e.patientID
            and concat(e.visitDateYy,e.visitDateMm,e.visitDateDd)=concat(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            and e.seqnum=pr.seqnum
            and pr.drugID=18
			and e.encounterType=5
			and e.siteCode =95698
			and e.encStatus < 255
		    and ifnull(pr.dispDateYy,0)>0
			  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Prescription
-- param: Date  Isoniazide(INH) dispense
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Prescription", pid.identifier as patient_id, id.identifier, 'Date Isoniazide(INH) dispense', e.date_created, date(o.obs_datetime) as 'medicament_dispense_isoniazide', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = '10d73929-54b6-4d18-a647-8b7316bc1ae3' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
 inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 78280
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);


-- ----------------------------------------------------------Pediatric Prescription--------------------------------------------------------------------
-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param:  Date de visite
-- site code: 95698
delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Date de visite' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Date de visite', e.createDate, date(concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)), 'x', date(now())
       from itech.encounter e, itech.patient p
      where e.patientID = p.patientID
        and e.encounterType=11
        and e.siteCode =95698
        and e.encStatus < 255
         on duplicate key update isante_value = values(isante_value), is_valid = (case
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
       else 'x'
        end );

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Date de visite
-- site code: 95698
<<<<<<< HEAD:src/main/resources/data_bkp.sql
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Date de visite', e.date_created, if(e.encounter_datetime is null, '',date(e.encounter_datetime)) as 'date_visite', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);
=======
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Date de visite', e.date_created, if(e.encounter_datetime is null, '',date(e.encounter_datetime)) as 'date_visite', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );
>>>>>>> d5ac40cc26be941c3f6dfa7f261bde22667bda1c:src/main/resources/backup/data_bkp.sql

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Date d'initiation ARV
-- site code: 95698
delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter =  "Date d'initiation ARV" and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, "Date d'initiation ARV", e.createDate,
       date(concat(t.arvStartDateYy,'-', t.arvStartDateMm,'-', t.arvStartDateDd)), 'x', date(now())
       from itech.encounter e, itech.patient p, itech.prescriptionOtherFields  t
      where e.patientID = p.patientID
        and p.patientID=t.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(t.visitDateYy,'-',t.visitDateMm,'-',t.visitDateDd)
        and e.seqnum=t.seqnum
        and e.encounterType=11
        and e.siteCode =95698
        and e.encStatus < 255
        and ifnull(t.arvStartDateYy,'')<>''
         on duplicate key update isante_value = values(isante_value), is_valid = (case
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
       else 'x'
        end );

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Date d'initiation ARV
-- site code: 95698
<<<<<<< HEAD:src/main/resources/data_bkp.sql
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, "Date d'initiation ARV", e.date_created,
       if(o.value_datetime is null, '',date(o.value_datetime)) as 'date_initiation_arv', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
 inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =159599 and o.encounter_id = e.encounter_id and o.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);
=======
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, "Date d'initiation ARV", e.date_created,
                if(o.value_datetime is null, '',date(o.value_datetime)) as 'date_initiation_arv', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =159599 and o.encounter_id = e.encounter_id and o.voided =0
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );
>>>>>>> d5ac40cc26be941c3f6dfa7f261bde22667bda1c:src/main/resources/backup/data_bkp.sql

-- consolidated isante database'
-- itech
-- form: Pediatric Prescription
-- param: Regime de 1ere ligne
-- site code: 95698

delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Regime de 1ere ligne' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Regime de 1ere ligne', e.createDate, 'oui', 'x', date(now())
       from itech.encounter e, itech.patient p,itech.obs  o
      where e.patientID = p.patientID
        and o.encounter_id=e.encounter_id
        and o.concept_id=163608
        and o.value_numeric=1
        and e.encounterType=11
        and e.siteCode =95698
        and e.encStatus < 255
         on duplicate key update isante_value = values(isante_value), is_valid = (case
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
       else 'x'
        end );

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Regime de 1ere ligne
-- site code: 95698
<<<<<<< HEAD:src/main/resources/data_bkp.sql
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Regime de 1ere ligne', e.date_created, 'oui' as 'regime_1ere_ligne', 'x', date(now())
from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =164432 and o.encounter_id = e.encounter_id and o.voided =0
    inner join cepoz.concept c on c.concept_id = o.concept_id  and c.uuid ='dd69cffe-d7b8-4cf1-bc11-3ac302763d48'
where p.voided =0
on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);
=======
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Regime de 1ere ligne', e.date_created, 'oui' as 'regime_1ere_ligne', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =164432 and o.encounter_id = e.encounter_id and o.voided =0
     inner join cepoz.concept c on c.concept_id = o.concept_id  and c.uuid ='dd69cffe-d7b8-4cf1-bc11-3ac302763d48'
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );
>>>>>>> d5ac40cc26be941c3f6dfa7f261bde22667bda1c:src/main/resources/backup/data_bkp.sql

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Regime de 2e ligne
-- site code: 95698

delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Regime de 2eme ligne' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Regime de 2eme ligne', e.createDate, o.value_numeric, 'x', date(now())
       from itech.encounter e, itech.patient p,itech.obs  o
      where e.patientID = p.patientID
        and o.encounter_id=e.encounter_id
        and o.concept_id=163608
        and o.value_numeric=2
        and e.encounterType=11
        and e.siteCode =95698
        and e.encStatus < 255
         on duplicate key update isante_value = values(isante_value), is_valid = (case
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
       else 'x'
        end );

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Regime de 2eme ligne
-- site code: 95698
<<<<<<< HEAD:src/main/resources/data_bkp.sql
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Regime de 2eme ligne', e.date_created, 'oui' as 'regime_2eme_ligne', 'x', date(now())
from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =164432 and o.encounter_id = e.encounter_id and o.voided =0
    inner join cepoz.concept c on c.concept_id = o.concept_id  and c.uuid ='77488a7b-957f-4ebc-892a-e53e7c910363'
where p.voided =0
on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);
=======
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Regime de 2eme ligne', e.date_created, 'oui' as 'regime_2eme_ligne', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =164432 and o.encounter_id = e.encounter_id and o.voided =0
     inner join cepoz.concept c on c.concept_id = o.concept_id  and c.uuid ='77488a7b-957f-4ebc-892a-e53e7c910363'
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );
>>>>>>> d5ac40cc26be941c3f6dfa7f261bde22667bda1c:src/main/resources/backup/data_bkp.sql

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Regime de 3e ligne
-- site code: 95698

delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Regime de 3eme ligne' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Regime de 3eme ligne', e.createDate, 'oui', 'x', date(now())
       from itech.encounter e, itech.patient p,itech.obs  o
      where e.patientID = p.patientID
        and o.encounter_id=e.encounter_id
        and o.concept_id=163608
        and o.value_numeric=4
        and e.encounterType=11
        and e.siteCode =95698
        and e.encStatus < 255
         on duplicate key update isante_value = values(isante_value), is_valid = (case
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
       else 'x'
        end );

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Regime de 3eme ligne
-- site code: 95698
<<<<<<< HEAD:src/main/resources/data_bkp.sql
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Regime de 3eme ligne', e.date_created, 'oui' as 'regime_3eme_ligne', 'x', date(now())
from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =164432 and o.encounter_id = e.encounter_id and o.voided =0
    inner join cepoz.concept c on c.concept_id = o.concept_id  and c.uuid ='99d88c3e-00ad-4122-a300-a88ff5c125c9'
where p.voided =0
on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);
=======
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Regime de 3eme ligne', e.date_created, 'oui' as 'regime_3eme_ligne', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =164432 and o.encounter_id = e.encounter_id and o.voided =0
     inner join cepoz.concept c on c.concept_id = o.concept_id  and c.uuid ='99d88c3e-00ad-4122-a300-a88ff5c125c9'
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );
>>>>>>> d5ac40cc26be941c3f6dfa7f261bde22667bda1c:src/main/resources/backup/data_bkp.sql

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: ABC RX  prophy
-- site code: 95698

delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'ABC RX Prophy' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'ABC RX Prophy', e.createDate,
       case
             when pr.forPepPmtct=1 then 'ABC Prophy'
             when pr.forPepPmtct=2 then 'ABC RX'
             else ''
       end   , 'x', date(now())
      from itech.encounter e, itech.patient p, itech.prescriptions pr
     where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=1
        and e.encounterType=11
        and e.siteCode =95698
        and e.encStatus < 255
        and pr.forPepPmtct in(1,2)
         on duplicate key update isante_value = values(isante_value), is_valid = (case
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
       else 'x'
        end );

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: ABC  RX  Prophy
-- site code: 95698
<<<<<<< HEAD:src/main/resources/data_bkp.sql
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'ABC RX Prophy', e.date_created,
       case
           when o.value_coded =138405 then 'ABC RX'
           when o.value_coded =163768 then 'ABC Prophy'
           end
    as 'abc_rx_prophy', 'x', date(now())
from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160742 and o.encounter_id = e.encounter_id and o.voided =0
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
    inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 70056
where p.voided =0
on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);
=======
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'ABC RX Prophy', e.date_created,
           case
                 when o.value_coded =138405 then 'ABC RX'
                 when o.value_coded =163768 then 'ABC Prophy'
            end  as 'abc_rx_prophy', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160742 and o.encounter_id = e.encounter_id and o.voided =0
     inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
     inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 70056
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );
>>>>>>> d5ac40cc26be941c3f6dfa7f261bde22667bda1c:src/main/resources/backup/data_bkp.sql

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: 3TC  RX  prophy
-- site code: 95698

delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = '3TC RX prophy' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, '3TC RX prophy', e.createDate,
       case
             when pr.forPepPmtct=1 then '3TC Prophy'
             when pr.forPepPmtct=2 then '3TC RX'
        end   , 'x', date(now())
       from itech.encounter e, itech.patient p, itech.prescriptions pr
      where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=20
        and e.encounterType=11
        and e.siteCode =95698
        and e.encStatus < 255
        and pr.forPepPmtct in (1,2)
         on duplicate key update isante_value = values(isante_value), is_valid = (case
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
       else 'x'
        end );

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param:3TC  RX  Prophy
-- site code: 95698
<<<<<<< HEAD:src/main/resources/data_bkp.sql
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, '3TC RX Prophy', e.date_created,
       case
           when o.value_coded =138405 then '3TC RX'
           when o.value_coded =163768 then '3TC Prophy'
           end
    as '3tc_rx_prophy', 'x', date(now())
from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
        left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160742 and o.encounter_id = e.encounter_id and o.voided =0
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
    inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 78643
where p.voided =0
on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);
=======
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, '3TC RX Prophy', e.date_created,
           case
                  when o.value_coded =138405 then '3TC RX'
                  when o.value_coded =163768 then '3TC Prophy'
            end   as '3tc_rx_prophy', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
     inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160742 and o.encounter_id = e.encounter_id and o.voided =0
     inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
     inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 78643
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );
>>>>>>> d5ac40cc26be941c3f6dfa7f261bde22667bda1c:src/main/resources/backup/data_bkp.sql

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: FTC RX prophy
-- site code: 95698
delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'FTC RX Prophy' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'FTC RX Prophy', e.createDate,
       case
             when pr.forPepPmtct=1 then 'FTC Prophy'
             when pr.forPepPmtct=2 then 'FTC RX'
        end  , 'x', date(now())
       from itech.encounter e, itech.patient p, itech.prescriptions pr
      where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=12
        and e.encounterType=11
        and e.encStatus < 255
        and e.siteCode =95698
        and pr.forPepPmtct in (1,2)
         on duplicate key update isante_value = values(isante_value), is_valid = (case
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
       else 'x'
        end );

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: FTC  RX  Prophy
-- site code: 95698
<<<<<<< HEAD:src/main/resources/data_bkp.sql
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'FTC RX Prophy', e.date_created,
       case
           when o.value_coded =138405 then 'FTC RX'
           when o.value_coded =163768 then 'FTC Prophy'
           end
    as 'ftc_rx_prophy', 'x', date(now())
from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160742 and o.encounter_id = e.encounter_id and o.voided =0
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
    inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 75628
where p.voided =0
on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);
=======
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'FTC RX Prophy', e.date_created,
           case
                 when o.value_coded =138405 then 'FTC RX'
                 when o.value_coded =163768 then 'FTC Prophy'
            end  as 'ftc_rx_prophy', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
     inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id
     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160742 and o.encounter_id = e.encounter_id and o.voided =0
     inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
     inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 75628
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );
>>>>>>> d5ac40cc26be941c3f6dfa7f261bde22667bda1c:src/main/resources/backup/data_bkp.sql

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: TNF RX prophy
-- site code: 95698
delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'TNF RX Prophy' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'TNF RX Prophy', e.createDate,
       case
             when pr.forPepPmtct=1 then 'TNF Prophy'
             when pr.forPepPmtct=2 then 'TNF RX'
             else ''
        end  , 'x', date(now())
       from itech.encounter e, itech.patient p, itech.prescriptions pr
      where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=31
        and e.encounterType=11
        and e.encStatus < 255
        and e.siteCode =95698
        and pr.forPepPmtct in (1,2)
         on duplicate key update isante_value = values(isante_value), is_valid = (case
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
       else 'x'
        end );

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param:TNF  RX  Prophy
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'TNF RX Prophy', e.date_created,
       case
           when o.value_coded =138405 then 'TNF RX'
           when o.value_coded =163768 then 'TNF Prophy'
           end
    as 'tnf_rx_prophy', 'x', date(now())
from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160742 and o.encounter_id = e.encounter_id and o.voided =0
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
    inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 84795
where p.voided =0
on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: AZT RX prophy
-- site code: 95698

delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'AZT RX prophy' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'AZT RX prophy', e.createDate,
       case
             when pr.forPepPmtct=1 then 'AZT Prophy'
             when pr.forPepPmtct=2 then 'AZT RX'
             else ''
        end  , 'x', date(now())
       from itech.encounter e, itech.patient p, itech.prescriptions pr
      where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=34
        and e.encounterType=11
        and e.encStatus < 255
        and e.siteCode =95698
        and pr.forPepPmtct in (1,2)
         on duplicate key update isante_value = values(isante_value), is_valid = (case
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
       else 'x'
        end );

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: AZT  RX  Prophy
-- site code: 95698
<<<<<<< HEAD:src/main/resources/data_bkp.sql
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'AZT RX Prophy', e.date_created,
       case
           when o.value_coded =138405 then 'AZT RX'
           when o.value_coded =163768 then 'AZT Prophy'
           end
    as 'azt_rx_prophy', 'x', date(now())
from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160742 and o.encounter_id = e.encounter_id and o.voided =0
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
    inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 86663
where p.voided =0
on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);
=======
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'AZT RX Prophy', e.date_created,
           case
                 when o.value_coded =138405 then 'AZT RX'
                 when o.value_coded =163768 then 'AZT Prophy'
            end  as 'azt_rx_prophy', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
     inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id
     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160742 and o.encounter_id = e.encounter_id and o.voided =0
     inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
     inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 86663
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );
>>>>>>> d5ac40cc26be941c3f6dfa7f261bde22667bda1c:src/main/resources/backup/data_bkp.sql

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: EFV rx prophy
-- site code: 95698

delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'EFV RX Prophy' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'EFV RX Prophy', e.createDate,
       case
             when pr.forPepPmtct=1 then 'EFV Prophy'
             when pr.forPepPmtct=2 then 'EFV RX'
        end  , 'x', date(now())
       from itech.encounter e, itech.patient p, itech.prescriptions pr
<<<<<<< HEAD:src/main/resources/data_bkp.sql
    where e.patientID = p.patientID
  and pr.patientID=e.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
  and e.seqnum=pr.seqnum
  and pr.drugID=11
  and e.encounterType=11
  and e.encStatus < 255
  and e.siteCode =95698
  and pr.forPepPmtct in (1,2)
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);
=======
      where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=11
        and e.encounterType=11
        and e.encStatus < 255
        and e.siteCode =95698
        and pr.forPepPmtct in (1,2)
         on duplicate key update isante_value = values(isante_value), is_valid = (case
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
       else 'x'
        end );
>>>>>>> d5ac40cc26be941c3f6dfa7f261bde22667bda1c:src/main/resources/backup/data_bkp.sql

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: EFV  Rx  Prophy
-- site code: 95698

     insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
          select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'EFV RX Prophy', e.date_created,
            case
                  when o.value_coded =138405 then 'EFV RX'
                  when o.value_coded =163768 then 'EFV Prophy'
             end  as 'efv_rx_prophy', 'x', date(now())
            from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
 left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
      inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
      inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
      inner join cepoz.encounter e on e.patient_id = p.patient_id
      inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
      inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160742 and o.encounter_id = e.encounter_id and o.voided =0
      inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
      inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 75523
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
             end );

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Atazanavir + BostRTV RX Prophy
-- site code: 95698

<<<<<<< HEAD:src/main/resources/data_bkp.sql
=======
delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Atazanavir + BostRTV RX Prophy' and site_id=95698;

>>>>>>> d5ac40cc26be941c3f6dfa7f261bde22667bda1c:src/main/resources/backup/data_bkp.sql
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Atazanavir + BostRTV RX Prophy', e.createDate,
       case
             when pr.forPepPmtct=1 then 'Atazanavir + BostRTV Prophy'
             when pr.forPepPmtct=2 then 'Atazanavir + BostRTV RX'
        end  , 'x', date(now())
       from itech.encounter e, itech.patient p, itech.prescriptions pr
      where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateyy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateyy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=6
        and e.encounterType=11
        and e.encStatus < 255
        and e.siteCode =95698
        and pr.forPepPmtct in (1,2)
         on duplicate key update isante_value = values(isante_value), is_valid = (case
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
       else 'x'
        end );

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Atazanavir + BostRTV  Rx  Prophy
-- site code: 95698

    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Atazanavir + BostRTV Rx Prophy', e.date_created,
           case
                 when o.value_coded =138405 then 'Atazanavir + BostRTV RX'
                 when o.value_coded =163768 then 'Atazanavir + BostRTV Prophy'
            end  as 'atazanavir_bostRTV_rx_prophy', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
     inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id
     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160742 and o.encounter_id = e.encounter_id and o.voided =0
     inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
     inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 159809
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param:Lopinavir + BostRTV RX Prophy
-- site code: 95698
<<<<<<< HEAD:src/main/resources/data_bkp.sql

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Lopinavir + BostRTV RX Prophy', e.createDate,
       case
             when pr.forPepPmtct=1 then 'Lopinavir + BostRTV Prophy'
             when pr.forPepPmtct=2 then 'Lopinavir + BostRTV RX'
        end  , 'x', date(now())
       from itech.encounter e, itech.patient p, itech.prescriptions pr
      where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=21
        and e.encounterType=11
        and e.encStatus < 255
        and e.siteCode =95698
        and pr.forPepPmtct in (1,2)
         on duplicate key update isante_value = values(isante_value), is_valid = (case
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
       else 'x'
        end );

=======

delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Lopinavir + BostRTV RX Prophy' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Lopinavir + BostRTV RX Prophy', e.createDate,
       case
             when pr.forPepPmtct=1 then 'Lopinavir + BostRTV Prophy'
             when pr.forPepPmtct=2 then 'Lopinavir + BostRTV RX'
        end  , 'x', date(now())
       from itech.encounter e, itech.patient p, itech.prescriptions pr
      where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=21
        and e.encounterType=11
        and e.encStatus < 255
        and e.siteCode =95698
        and pr.forPepPmtct in (1,2)
         on duplicate key update isante_value = values(isante_value), is_valid = (case
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
       else 'x'
        end );

>>>>>>> d5ac40cc26be941c3f6dfa7f261bde22667bda1c:src/main/resources/backup/data_bkp.sql
-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Lopinavir + BostRTV Rx  Prophy
-- site code: 95698

    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Lopinavir + BostRTV RX Prophy', e.date_created,
           case
                  when o.value_coded =138405 then 'Lopinavir + BostRTV RX'
                  when o.value_coded =163768 then 'Lopinavir + BostRTV Prophy'
            end   as 'lopinavir_bostRTV_rx_prophy', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
     inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id
     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160742 and o.encounter_id = e.encounter_id and o.voided =0
     inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
     inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 794
          where p.voided =0
            on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
          when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
          when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
          else 'x'
           end );

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Raltegravir RX Prophy
-- site code: 95698

<<<<<<< HEAD:src/main/resources/data_bkp.sql
=======
delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Raltegravir RX Prophy' and site_id=95698;

>>>>>>> d5ac40cc26be941c3f6dfa7f261bde22667bda1c:src/main/resources/backup/data_bkp.sql
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Raltegravir RX Prophy', e.createDate,
       case
             when pr.forPepPmtct=1 then 'Raltegravir Prophy'
             when pr.forPepPmtct=2 then 'Raltegravir RX'
        end  , 'x', date(now())
       from itech.encounter e, itech.patient p,itech.prescriptions  pr
      where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=87
        and e.encounterType=11
        and e.encStatus < 255
        and e.siteCode =95698
        and pr.forPepPmtct in (1,2)
         on duplicate key update isante_value = values(isante_value), is_valid = (case
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
       else 'x'
        end );

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Raltegravir Rx  Prophy
-- site code: 95698

    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Raltegravir RX Prophy', e.date_created,
           case
                 when o.value_coded =138405 then 'Raltegravir RX'
                 when o.value_coded =163768 then  'Raltegravir Prophy'
            end  as 'raltegravir_rx_prophy', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
     inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id
     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160742 and o.encounter_id = e.encounter_id and o.voided =0
     inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
     inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 154378
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Dolutegravir RX Prophy
-- site code: 95698
delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Dolutegravir RX Prophy' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Dolutegravir RX Prophy', e.createDate,
       case
             when pr.forPepPmtct=1 then 'Dolutegravir Prophy'
             when pr.forPepPmtct=2 then 'Dolutegravir RX'
        end  , 'x', date(now())
       from itech.encounter e, itech.patient p, itech.prescriptions pr
      where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateyy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateyy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=89
        and e.encounterType=11
        and e.encStatus < 255
        and e.siteCode =95698
        and pr.forPepPmtct in (1,2)
         on duplicate key update isante_value = values(isante_value), is_valid = (case
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
       else 'x'
        end );

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Dolutegravir Rx  Prophy
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Dolutegravir RX Prophy', e.date_created,
           case
                 when o.value_coded =138405 then 'Dolutegravir RX'
                 when o.value_coded =163768 then  'Dolutegravir Prophy'
            end  as 'dolutegravir_rx_prophy', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
     inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id
     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160742 and o.encounter_id = e.encounter_id and o.voided =0
     inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
     inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 165085
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Cotrimoxazole RX Prophy
-- site code: 95698

delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Cotrimoxazole RX Prophy' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Cotrimoxazole RX Prophy', e.createDate,
       case
             when pr.forPepPmtct=1 then 'Cotrimoxazole Prophy'
             when pr.forPepPmtct=2 then 'Cotrimoxazole RX'
        end  , 'x', date(now())
       from itech.encounter e, itech.patient p, itech.prescriptions pr
      where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateyy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateyy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=9
        and e.encounterType=11
        and e.encStatus < 255
        and e.siteCode =95698
        and pr.forPepPmtct in (1,2)
         on duplicate key update isante_value = values(isante_value), is_valid = (case
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
       else 'x'
        end );

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Cotrimoxazole Rx Prophy
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Cotrimoxazole RX Prophy', e.date_created,
           case
                 when o.value_coded =138405 then 'Cotrimoxazole RX'
                 when o.value_coded =163768 then  'Cotrimoxazole Prophy'
            end  as 'cotrimoxazole_rx_prophy', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
     inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id
     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160742 and o.encounter_id = e.encounter_id and o.voided =0
     inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
     inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 105281
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Isoniazide (INH) RX Prophy
-- site code: 95698

delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Isoniazide (INH) RX Prophy' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Isoniazide (INH) RX Prophy', e.createDate,
       case
             when pr.forPepPmtct=1 then 'Isoniazide (INH) Prophy'
             when pr.forPepPmtct=2 then 'Isoniazide (INH) RX'
        end  , 'x', date(now())
       from itech.encounter e, itech.patient p, itech.prescriptions pr
      where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateyy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateyy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=18
        and e.encounterType=11
        and e.encStatus < 255
        and e.siteCode =95698
        and pr.forPepPmtct in (1,2)
         on duplicate key update isante_value = values(isante_value), is_valid = (case
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
       else 'x'
        end );

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Isoniazide (INH) Rx  Prophy
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Isoniazide (INH) RX Prophy', e.date_created,
           case
                 when o.value_coded =138405 then 'Isoniazide (INH) RX'
                 when o.value_coded =163768 then 'Isoniazide (INH) Prophy'
            end  as 'inh_rx_prophy', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
     inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id
     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160742 and o.encounter_id = e.encounter_id and o.voided =0
     inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
     inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 78280
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: INH nombre de jours
-- site code: 95698

delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'INH nombre de jours' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'INH nombre de jours', e.createDate, pr.numDaysDesc, 'x', date(now())
       from itech.encounter e, itech.patient p, itech.prescriptions pr
      where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateyy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateyy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=18
        and e.encounterType=11
        and e.encStatus < 255
        and e.siteCode =95698
        and ifnull(pr.numDaysDesc,0)>0
         on duplicate key update isante_value = values(isante_value), is_valid = (case
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
       else 'x'
        end );

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: INH nombre de jours
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'INH nombre de jours', e.date_created, o.value_numeric as 'nbre_jrs_inh', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
     inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id
     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =159368 and o.encounter_id = e.encounter_id and o.voided =0
     inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =1442
     inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 78280
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Medicament ABC dispense
-- site code: 95698
delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Medicament ABC dispense' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Medicament ABC dispense', e.createDate,
       case
            when pr.dispensed=1 then 'oui'
            when pr.dispensed=2 then 'Non'
        end  , 'x', date(now())
       from itech.encounter e, itech.patient p, itech.prescriptions pr
      where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateyy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateyy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=1
        and e.encounterType=11
        and e.encStatus < 255
        and e.siteCode =95698
        and pr.dispensed in (1,2)
         on duplicate key update isante_value = values(isante_value), is_valid = (case
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
       else 'x'
        end );

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Medicament ABC dispense
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Medicament ABC dispense', e.date_created,
           case
                 when o.value_coded =1 then 'oui'
            end  as 'medicament_dispense_abc', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
     inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id
     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
     inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
     inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 70056
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Medicament 3TC dispense
-- site code: 95698

delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Medicament 3TC dispense' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Medicament 3TC dispense',  e.createDate,
       case
             when pr.dispensed=1 then 'oui'
             when pr.dispensed=2 then 'Non'
        end  , 'x', date(now())
       from itech.encounter e, itech.patient p, itech.prescriptions pr
      where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateyy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateyy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=20
        and e.encounterType=11
        and e.encStatus < 255
        and e.siteCode =95698
        and pr.dispensed in (1,2)
         on duplicate key update isante_value = values(isante_value), is_valid = (case
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
       else 'x'
        end );

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Medicament 3TC dispense
-- site code: 95698
   insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
        select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Medicament 3TC dispense', e.date_created,
          case
                 when o.value_coded =1 then 'oui'
           end   as 'medicament_dispense_3tc', 'x', date(now())
from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
     inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id
     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
     inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
     inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 78643
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Medicament FTC dispense
-- site code: 95698

delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Medicament FTC dispense' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698,"Pediatric Prescription", e.patientID, p.clinicPatientID, 'Medicament FTC dispense', e.createDate,
       case
             when pr.dispensed=1 then 'oui'
             when pr.dispensed=2 then 'Non'
        end   , 'x', date(now())
       from itech.encounter e, itech.patient p, itech.prescriptions pr
      where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateyy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateyy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=12
        and e.encounterType=11
        and e.encStatus < 255
        and e.siteCode =95698
        and pr.dispensed in (1,2)
         on duplicate key update isante_value = values(isante_value), is_valid = (case
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
       else 'x'
        end );

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Medicament FTC dispense
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Medicament FTC dispense', e.date_created,
           case
                 when o.value_coded =1 then 'oui'
            end  as 'medicament_dispense_ftc', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
     inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id
     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
     inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
     inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 75628
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Medicament TNF dispense
-- site code: 95698

delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Medicament TNF dispense' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Medicament TNF dispense', e.createDate,
       case
             when pr.dispensed=1 then 'oui'
             when pr.dispensed=2 then 'Non'
        end  , 'x', date(now())
       from itech.encounter e, itech.patient p, itech.prescriptions pr
      where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateyy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateyy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=31
       and e.encounterType=11
       and e.encStatus < 255
       and e.siteCode =95698
       and pr.dispensed in (1,2)
        on duplicate key update isante_value = values(isante_value), is_valid = (case
      when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
      when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
      else 'x'
       end );

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Medicament TNF dispense
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Medicament TNF dispense', e.date_created,
           case
                 when o.value_coded =1 then 'oui'
            end  as 'medicament_dispense_tnf', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
     inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id
     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
     inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
     inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 84795
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Medicament AZT dispense
-- site code: 95698

delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Medicament AZT dispense' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Medicament AZT dispense', e.createDate,
       case
            when pr.dispensed=1 then 'oui'
            when pr.dispensed=2 then 'Non'
            else ''
       end  , 'x', date(now())
      from itech.encounter e, itech.patient p, itech.prescriptions pr
     where e.patientID = p.patientID
       and pr.patientID=e.patientID
       and concat(e.visitDateyy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateyy,'-',pr.visitDateMm,'-',pr.visitDateDd)
       and e.seqnum=pr.seqnum
       and pr.drugID=34
       and e.encounterType=11
       and e.encStatus < 255
       and e.siteCode =95698
       and pr.dispensed in (1,2)
        on duplicate key update isante_value = values(isante_value), is_valid = (case
      when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
      when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
      else 'x'
       end );

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Medicament AZT dispense
-- site code: 95698
   insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
        select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Medicament AZT dispense', e.date_created,
          case
                when o.value_coded =1 then 'oui'
           end  as 'medicament_dispense_azt', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
     inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id
     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
     inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
     inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 86663
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Medicament EFV dispense
-- site code: 95698
delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Medicament EFV dispense' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Medicament EFV dispense', e.createDate,
       case
             when pr.dispensed=1 then 'oui'
             when pr.dispensed=2 then 'Non'
       end   , 'x', date(now())
      from itech.encounter e, itech.patient p, itech.prescriptions pr
     where e.patientID = p.patientID
       and pr.patientID=e.patientID
       and concat(e.visitDateyy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateyy,'-',pr.visitDateMm,'-',pr.visitDateDd)
       and e.seqnum=pr.seqnum
       and pr.drugID=11
       and e.encounterType=11
       and e.encStatus < 255
       and e.siteCode =95698
       and pr.dispensed in (1,2)
        on duplicate key update isante_value = values(isante_value), is_valid = (case
      when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
      when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
      else 'x'
       end );

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Medicament dispense EFV
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Medicament EFV dispense', e.date_created,
           case
                 when o.value_coded =1 then 'oui'
            end  as 'medicament_dispense_efv', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
     inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id
     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
     inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
     inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 75523
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Medicament Atazanavir + BostRTV dispense
-- site code: 95698
delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Medicament Atazanavir + BostRTV dispense' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Medicament Atazanavir + BostRTV dispense', e.createDate,
       case
             when pr.dispensed=1 then 'oui'
             when pr.dispensed=2 then 'Non'
        end  , 'x', date(now())
       from itech.encounter e, itech.patient p, itech.prescriptions pr
      where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateyy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateyy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=6
        and e.encounterType=11
        and e.encStatus < 255
        and e.siteCode =95698
        and pr.dispensed in (1,2)
         on duplicate key update isante_value = values(isante_value), is_valid = (case
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
       else 'x'
        end );

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Medicament Atazanavir + BostRTV dispense
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Medicament Atazanavir + BostRTV dispense', e.date_created,
           case
                when o.value_coded =1 then 'oui'
            end  as 'medicament_dispense_atazanavir_bostRTV', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
     inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id
     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
     inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
     inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 159809
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: lopinavir + bostrtv dispense
-- site code: 95698
<<<<<<< HEAD:src/main/resources/data_bkp.sql
=======
delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Medicament Lopinavir + BostRTV dispense' and site_id=95698;

>>>>>>> d5ac40cc26be941c3f6dfa7f261bde22667bda1c:src/main/resources/backup/data_bkp.sql
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
    select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Medicament Lopinavir + BostRTV dispense', e.createDate,
      case
            when pr.dispensed=1 then 'oui'
      when pr.dispensed=2 then 'Non' end, 'x', date(now())
      from itech.encounter e, itech.patient p, itech.prescriptions pr
     where e.patientID = p.patientID
       and pr.patientID=e.patientID
       and concat(e.visitDateyy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateyy,'-',pr.visitDateMm,'-',pr.visitDateDd)
       and e.seqnum=pr.seqnum
       and pr.drugID=21
        and e.encounterType=11
        and e.encStatus < 255
        and e.siteCode =95698
        and pr.dispensed in (1,2)
        on duplicate key update isante_value = values(isante_value), is_valid = (case
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
        else 'x'
        end);

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Medicament Lopinavir + BostRTV dispense
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Medicament Lopinavir + BostRTV dispense', e.date_created,
           case
                 when o.value_coded =1 then 'oui'
            end  as 'medicament_dispense_lopinavir_bostRTV', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
     inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id
     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
     inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
     inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 794
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Medicament Raltegravir dispense
-- site code: 95698

delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Medicament Raltegravir dispense' and site_id=95698;


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Medicament Raltegravir dispense', e.createDate,
       case
            when pr.dispensed=1 then 'oui'
            when pr.dispensed=2 then 'Non'
        end , 'x', date(now())
       from itech.encounter e, itech.patient p, itech.prescriptions pr
      where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=87
        and e.encounterType=11
        and e.siteCode=95698
        and e.encStatus < 255
        and pr.dispensed in (1,2)
         on duplicate key update isante_value = values(isante_value), is_valid = (case
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
       else 'x'
        end );

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Medicament Raltegravir dispense
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Medicament Raltegravir dispense', e.date_created,
           case
                when o.value_coded =1 then 'oui'
           end  as 'medicament_dispense_raltegravir', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
     inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id
     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
     inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
     inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 154378
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Medicament Dolutegravir dispense
-- site code: 95698
delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Medicament Dolutegravir dispense' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Medicament Dolutegravir dispense', e.createDate,
       case
           when pr.dispensed=1 then 'oui'
           when pr.dispensed=2 then 'non'
           end, 'x', date(now())
from itech.encounter e, itech.patient p, itech.prescriptions pr
where e.patientID = p.patientID
  and pr.patientID=e.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
  and e.seqnum=pr.seqnum
  and pr.drugID=89
  and e.encounterType=11
  and e.siteCode=95698
  and e.encStatus < 255
  and pr.dispensed in (1,2)
  on duplicate key update isante_value = values(isante_value), is_valid = (case
when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
else 'x'
end);

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Medicament Dolutegravir dispense
-- site code: 95698
<<<<<<< HEAD:src/main/resources/data_bkp.sql
=======


>>>>>>> d5ac40cc26be941c3f6dfa7f261bde22667bda1c:src/main/resources/backup/data_bkp.sql
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Medicament Dolutegravir dispense', e.date_created,
           case
                 when o.value_coded =1 then 'oui'
            end  as 'medicament_dispense_dolutegravir', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
     inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id
     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
     inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
     inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 165085
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Medicament Cotrimoxazole dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Medicament Cotrimoxazole dispense', e.createDate,
       case
             when pr.dispensed=1 then 'oui'
             when pr.dispensed=2 then 'non'
        end  , 'x', date(now())
       from itech.encounter e, itech.patient p, itech.prescriptions pr
      where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=9
        and e.encounterType=11
        and e.encStatus < 255
        and e.siteCode =95698
        and pr.dispensed in (1,2)
         on duplicate key update isante_value = values(isante_value), is_valid = (case
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
       else 'x'
        end );

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Medicament Cotrimoxazole dispense
-- site code: 95698
<<<<<<< HEAD:src/main/resources/data_bkp.sql
=======
    delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Medicament Cotrimoxazole dispense' and site_id=95698;

>>>>>>> d5ac40cc26be941c3f6dfa7f261bde22667bda1c:src/main/resources/backup/data_bkp.sql
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Medicament Cotrimoxazole dispense', e.date_created,
           case
                when o.value_coded =1 then 'oui'
           end  as 'medicament_dispense_cotrimoxazole', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
     inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id
     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
     inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
     inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 105281
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Medicament Isoniazide(INH) dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Medicament Isoniazide(INH) dispense', e.createDate,
       case
             when pr.dispensed=1 then 'oui'
             when pr.dispensed=2 then 'non'
        end  , 'x', date(now())
       from itech.encounter e, itech.patient p, itech.prescriptions pr
      where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=18
        and e.encounterType=11
        and e.encStatus < 255
        and e.siteCode =95698
        and pr.dispensed in (1,2)
         on duplicate key update isante_value = values(isante_value), is_valid = (case
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
       else 'x'
        end );

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Medicament Isoniazide(INH) dispense
-- site code: 95698
<<<<<<< HEAD:src/main/resources/data_bkp.sql
=======

delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Medicament Isoniazide(INH) dispense' and site_id=95698;

>>>>>>> d5ac40cc26be941c3f6dfa7f261bde22667bda1c:src/main/resources/backup/data_bkp.sql
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Medicament Isoniazide(INH) dispense', e.date_created,
           case
                 when o.value_coded =1 then 'oui'
            end  as 'medicament_dispense_isoniazide' ,'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
     inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id
     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
     inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
     inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 78280
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Date ABC dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698,"Pediatric Prescription", e.patientID, p.clinicPatientID, 'Date ABC dispense', e.createDate, date(concat(pr.dispDateYy,'-', pr.dispDateMm,'-', pr.dispDateDd)), 'x', date(now())
       from itech.encounter e, itech.patient p, itech.prescriptions pr
      where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=1
        and e.encounterType=11
        and e.siteCode=95698
        and e.encStatus < 255
        and ifnull(pr.dispDateYy,0)>0
         on duplicate key update isante_value = values(isante_value), is_valid = (case
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
       else 'x'
        end );

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Date ABC dispense
-- site code: 95698
<<<<<<< HEAD:src/main/resources/data_bkp.sql
=======

delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Date ABC dispense' and site_id=95698;

>>>>>>> d5ac40cc26be941c3f6dfa7f261bde22667bda1c:src/main/resources/backup/data_bkp.sql
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Date ABC dispense', e.date_created, o.obs_datetime as 'date_dispense_abc', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
     inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id
     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
     inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
     inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 70056
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Date 3TC dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Date 3TC dispense', e.createDate, date(concat(pr.dispDateYy,'-',pr.dispDateMm,'-',pr.dispDateDd)), 'x', date(now())
       from itech.encounter e, itech.patient p, itech.prescriptions pr
      where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=20
        and e.encounterType=11
        and e.siteCode=95698
        and e.encStatus < 255
        and ifnull(pr.dispDateYy,0)>0
         on duplicate key update isante_value = values(isante_value), is_valid = (case
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
       when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
       else 'x'
        end );

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Date 3TC dispense
-- site code: 95698

delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Date 3TC dispense' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Date 3TC dispense', e.date_created, date(o.obs_datetime) as 'date_dispense_3tc', 'x', date(now())
from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
     inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
     inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
     inner join cepoz.encounter e on e.patient_id = p.patient_id
     inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
     inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
     inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
     inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 78643
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
           when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
           when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
           else 'x'
            end );

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Date FTC dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Date FTC dispense', e.createDate, date(concat(pr.dispDateYy,'-',pr.dispDateMm,'-',pr.dispDateDd)), 'x', date(now())
from itech.encounter e, itech.patient p, itech.prescriptions pr
where e.patientID = p.patientID
  and pr.patientID=e.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
  and e.seqnum=pr.seqnum
  and pr.drugID=12
  and e.encounterType=11
  and e.siteCode=95698
  and e.encStatus < 255
  and ifnull(pr.dispDateYy,0)>0
  on duplicate key update isante_value = values(isante_value), is_valid = (case
when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
else 'x'
end);

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Date FTC dispense
-- site code: 95698

delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Date FTC dispense' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Date FTC dispense', e.date_created, date(o.obs_datetime) as 'date_dispense_ftc', 'x', date(now())
from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
    inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 75628
where p.voided =0
on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Date TNF dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Date TNF dispense', e.createDate,
    date(concat(pr.dispDateYy,'-',pr.dispDateMm,'-',pr.dispDateDd)),'x', date(now())
from itech.encounter e, itech.patient p, itech.prescriptions pr
where e.patientID = p.patientID
  and pr.patientID=e.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
  and e.seqnum=pr.seqnum
  and pr.drugID=31
  and e.encounterType=11
  and e.siteCode=95698
  and e.encStatus < 255
  and ifnull(pr.dispDateYy,0)>0
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Date TNF dispense
-- site code: 95698

delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Date TNF dispense' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Date TNF dispense', e.date_created, date(o.obs_datetime) as 'date_dispense_tnf', 'x', date(now())
from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
    inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 84795
         where p.voided =0
            on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
          when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
          when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
          else 'x'
          end);

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Date AZT dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Date AZT dispense', e.createDate, date(concat(pr.dispDateYy,'-',pr.dispDateMm,'-',pr.dispDateDd)), 'x', date(now())
  from itech.encounter e, itech.patient p, itech.prescriptions pr
 where e.patientID = p.patientID
   and pr.patientID=e.patientID
   and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
   and e.seqnum=pr.seqnum
   and pr.drugID=34
   and e.encounterType=11
   and e.siteCode=95698
   and e.encStatus < 255
   and ifnull(pr.dispDateYy,0)>0
    on duplicate key update isante_value = values(isante_value), is_valid = (case
  when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
  when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
  else 'x'
  end);

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Date AZT dispense
-- site code: 95698
delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Date AZT dispense' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Date AZT dispense', e.date_created, date(o.obs_datetime) as 'date_dispense_azt', 'x', date(now())
from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
    inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 86663
         where p.voided =0
            on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
          when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
          when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
          else 'x'
           end);

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Date EFV dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Date EFV dispense', e.createDate,
    date(concat(pr.dispDateYy,'-',pr.dispDateMm,'-',pr.dispDateDd)), 'x', date(now())
from itech.encounter e, itech.patient p, itech.prescriptions pr
where e.patientID = p.patientID
  and pr.patientID=e.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
  and e.seqnum=pr.seqnum
  and pr.drugID=11
  and e.encounterType=11
  and e.siteCode=95698
  and e.encStatus < 255
  and ifnull(pr.dispDateYy,0)>0
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Date EFV dispense
-- site code: 95698
delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Date EFV dispense' and site_id=95698;

insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Date EFV dispense', e.date_created, date(o.obs_datetime) as 'date_dispense_efv', 'x', date(now())
from cepoz.patient p
    left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
    left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
        inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
        inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
        inner join cepoz.encounter e on e.patient_id = p.patient_id
        inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
        inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
        inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
        inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 75523
              where p.voided =0
                 on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
                when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
                when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
                else 'x'
                end);

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param:Date Atazanavir + BostRTV dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Date Atazanavir + BostRTV dispense', e.createDate,
    date(concat(pr.dispDateYy,'-',pr.dispDateMm,'-',pr.dispDateDd)), 'x', date(now())
from itech.encounter e, itech.patient p, itech.prescriptions pr
where e.patientID = p.patientID
  and pr.patientID=e.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
  and e.seqnum=pr.seqnum
  and pr.drugID=6
  and e.encounterType=11
  and e.siteCode=95698
  and e.encStatus < 255
  and ifnull(pr.dispDateYy,0)>0
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Date Atazanavir + BostRTV dispense
-- site code: 95698
<<<<<<< HEAD:src/main/resources/data_bkp.sql
=======

delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Date Atazanavir + BostRTV dispense' and site_id=95698;

>>>>>>> d5ac40cc26be941c3f6dfa7f261bde22667bda1c:src/main/resources/backup/data_bkp.sql
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Date Atazanavir + BostRTV dispense', e.date_created, date(o.obs_datetime) as 'date_dispense_atazanavir_bostRTV', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
    inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 159809
         where p.voided =0
            on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Date Lopinavir + BostRTV dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Date Lopinavir + BostRTV dispense', e.createDate,
    date(concat(pr.dispDateYy,'-',pr.dispDateMm,'-',pr.dispDateDd)), 'x', date(now())
from itech.encounter e, itech.patient p, itech.prescriptions pr
where e.patientID = p.patientID
  and pr.patientID=e.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
  and e.seqnum=pr.seqnum
  and pr.drugID=21
  and e.encounterType=11
  and e.siteCode=95698
  and e.encStatus < 255
  and ifnull(pr.dispDateYy,0)>0
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
<<<<<<< HEAD:src/main/resources/data_bkp.sql
-- param: Date Lopinavir + BostRTV' dispense
    -- site code: 95698
=======
-- param: Date Lopinavir + BostRTV dispense
-- site code: 95698
delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Date Lopinavir + BostRTV dispense' and site_id=95698;

>>>>>>> d5ac40cc26be941c3f6dfa7f261bde22667bda1c:src/main/resources/backup/data_bkp.sql
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
        select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Date Lopinavir + BostRTV dispense', e.date_created, date(o.obs_datetime) as 'date_dispense_lopinavir_bostRTV', 'x', date(now())
            from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
    inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 794
        where p.voided =0
           on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Date Raltegravir dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Date Raltegravir dispense', e.createDate,
    date(concat(pr.dispDateYy,'-',pr.dispDateMm,'-',pr.dispDateDd)), 'x', date(now())
from itech.encounter e, itech.patient p, itech.prescriptions pr
where e.patientID = p.patientID
  and pr.patientID=e.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
  and e.seqnum=pr.seqnum
  and pr.drugID=87
  and e.encounterType=11
  and e.siteCode=95698
  and e.encStatus < 255
  and ifnull(pr.dispDateYy,0)>0
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Date Raltegravir dispense
-- site code: 95698
<<<<<<< HEAD:src/main/resources/data_bkp.sql
=======
delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Date Raltegravir dispense' and site_id=95698;


>>>>>>> d5ac40cc26be941c3f6dfa7f261bde22667bda1c:src/main/resources/backup/data_bkp.sql
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
        select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Date Raltegravir dispense', e.date_created, date(o.obs_datetime) as 'medicament_dispense_raltegravir', 'x', date(now())
          from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
    inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 154378
        where p.voided =0
           on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
        when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
        when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
        else 'x'
        end);

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Date Dolutegravir  dispense
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Date Dolutegravir dispense', e.createDate,
    date(concat(pr.dispDateYy,'-',pr.dispDateMm,'-',pr.dispDateDd)), 'x', date(now())
from itech.encounter e, itech.patient p, itech.prescriptions pr
where e.patientID = p.patientID
  and pr.patientID=e.patientID
  and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
  and e.seqnum=pr.seqnum
  and pr.drugID=89
  and e.encounterType=11
  and e.siteCode=95698
  and e.encStatus < 255
  and ifnull(pr.dispDateYy,0)>0
  on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Date Dolutegravir dispense
-- site code: 95698
<<<<<<< HEAD:src/main/resources/data_bkp.sql
=======
delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Date Dolutegravir dispense' and site_id=95698;

>>>>>>> d5ac40cc26be941c3f6dfa7f261bde22667bda1c:src/main/resources/backup/data_bkp.sql
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Date Dolutegravir dispense', e.date_created, date(o.obs_datetime) as 'date_dispense_dolutegravir', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
    inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 165085
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Date Cotrimoxazole dispense
-- site code: 95698

delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Date Cotrimoxazole dispense' and site_id=95698;

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Date Cotrimoxazole dispense', e.createDate,
       date(concat(pr.dispDateYy,'-',pr.dispDateMm,'-',pr.dispDateDd)), 'x', date(now())
       from itech.encounter e, itech.patient p, itech.prescriptions pr
      where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)=concat(pr.visitDateYy,'-',pr.visitDateMm,'-',pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=9
        and e.encounterType=11
        and e.siteCode=95698
        and e.encStatus < 255
        and ifnull(pr.dispDateYy,0)>0
           on duplicate key update isante_value = values(isante_value), is_valid = (case
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
            when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
            else 'x'
            end);

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Date Cotrimoxazole dispense
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Date Cotrimoxazole dispense', e.date_created, date(o.obs_datetime) as 'date_dispense_cotrimoxazole', 'x', date(now())
           from cepoz.patient p
left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
    inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 105281
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);

-- consolidated isante database
-- itech
-- form: Pediatric Prescription
-- param: Date Isoniazide(INH) dispense
-- site code: 95698
delete from replication.validation where form_origin = 'Pediatric Prescription' and parameter = 'Date Isoniazide(INH) dispense' and site_id=95698;


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select 95698, "Pediatric Prescription", e.patientID, p.clinicPatientID, 'Date Isoniazide(INH) dispense', e.createDate,
       date(concat(pr.dispDateYy,'-',pr.dispDateMm,'-',pr.dispDateDd)), 'x', date(now())
       from itech.encounter e, itech.patient p,itech.prescriptions  pr
      where e.patientID = p.patientID
        and pr.patientID=e.patientID
        and concat(e.visitDateYy,e.visitDateMm,e.visitDateDd)=concat(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
        and e.seqnum=pr.seqnum
        and pr.drugID=18
        and e.encounterType=11
        and e.siteCode =95698
        and e.encStatus < 255
        and ifnull(pr.dispDateYy,0)>0
       on duplicate key update isante_value = values(isante_value), is_valid = (case
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value = values(isante_value) then 'y'
        when isanteplus_value is not null and values(isante_value) is not null and isanteplus_value <> values(isante_value) then 'n'
        else 'x'
        end);

-- iSanteplus database
-- cepoz
-- form: Pediatric Prescription
-- param: Date  Isoniazide(INH) dispense
-- site code: 95698
    insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
         select 95698, "Pediatric Prescription", pid.identifier as patient_id, id.identifier, 'Date Isoniazide(INH) dispense', e.date_created, date(o.obs_datetime) as 'medicament_dispense_isoniazide', 'x', date(now())
           from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id
left outer join  cepoz.patient_identifier_type pit on pit.patient_identifier_type_id = id.identifier_type and pit.uuid = 'd059f6d0-9e42-4760-8de1-8316b48bc5f1'
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id
    inner join cepoz.patient_identifier_type pi on pi.patient_identifier_type_id = pid.identifier_type and pi.uuid = '0e0c7cc2-3491-4675-b705-746e372ff346'
    inner join cepoz.encounter e on e.patient_id = p.patient_id
    inner join cepoz.encounter_type et on et.encounter_type_id = e.encounter_type and et.uuid = 'a9392241-109f-4d67-885b-57cc4b8c638f' and e.voided =0
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1276  and o.encounter_id = e.encounter_id and o.voided =0
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o2.concept_id =163711
    inner join cepoz.obs o3 on o3.obs_group_id = o.obs_group_id and o3.concept_id = 1282 and o3.value_coded = 78280
          where p.voided =0
             on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = (case
            when isante_value is not null and values(isanteplus_value) is not null and isante_value = values(isanteplus_value) then 'y'
            when isante_value is not null and values(isanteplus_value) is not null and isante_value <> values(isanteplus_value) then 'n'
            else 'x'
            end);


