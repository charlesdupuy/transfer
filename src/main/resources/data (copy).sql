-- consolidated isante database
-- itech
-- form: Registration
-- param: Nom
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, 'Registration', e.patientID, p.clinicPatientID, 'Nom', date(e.visitDate), p.lname, 'x', date(now())
       from itech.encounter e, itech.patient p
      where e.patientID = p.patientID
        and e.encounterType in (10, 15)
        and e.siteCode = 95698
         on duplicate key update isante_value = values(isante_value), is_valid = if(isanteplus_value = values(isante_value), 'y', 'n');




-- isanteplus database
-- cepoz
-- form: Registration
-- param: Nom
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Registration', pid.identifier as patient_id, id.identifier, 'Nom', date(e.encounter_datetime), pn.family_name as 'nom', 'x', date(now())
       from cepoz.person_name pn
 inner join cepoz.patient p on p.patient_id = pn.person_id and p.voided=0
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 29 and e.voided=0
      where pn.voided=0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');
































-- Nom
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, 'Registration', e.patientID+0, p.clinicPatientID, "Nom", e.createDate, p.lname, 'x', now()
       from itech.encounter e, itech.patient p
      where e.patientID = p.patientID
        and e.encounterType in (10, 15)
        and e.siteCode = 95698
         on duplicate key update isante_value = values(isante_value), is_valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- Nom
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Registration', pn.person_id, id.identifier, 'nom', date(e.encounter_datetime), pn.family_name as 'nom', 'x', now()
       from cepoz.person_name pn
 inner join cepoz.patient p on p.patient_id = pn.person_id and p.voided=0
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 29 and e.voided=0
      where pn.voided=0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');


                                               

-- PRENom

insert into replication.validation (site, form, patient, st_code, param, collection_date, isante)

                select e.siteCode, "Registration", e.patientID, p.clinicPatientID, "PRENom", e.createDate, p.fname

                from itech.encounter e, itech.patient p

                                where e.patientID = p.patientID

                                                and e.encounterType in (10, 15)

                                                and e.siteCode = 95698

                                                on duplicate key update isante = values(isante);

                                               

--DATE DE VISITE

insert into replication.validation (site, form, patient, st_code, param, collection_date, isante)

                select e.siteCode, "Registration", e.patientID, p.clinicPatientID, "DATE DE VISITE", e.createDate, e.visitDate

                from itech.encounter e, itech.patient p

                                where e.patientID = p.patientID

                                                and e.encounterType in (10, 15)

                                                and e.siteCode = 95698

                                                on duplicate key update isante = values(isante);

                                               

insert into replication.validation (site, form, patient, st_code, param, collection_date, isante)

                select e.siteCode, "Registration", e.patientID, p.clinicPatientID, "CODE ST", e.createDate, p.clinicPatientID

                from itech.encounter e, itech.patient p

                                where e.patientID = p.patientID

                                                and e.encounterType in (10, 15)

                                                and e.siteCode = 95698

                                                on duplicate key update isante = values(isante);

                                               

-- CODE NATIONAL

insert into replication.validation (site, form, patient, st_code, param, collection_date, isante)

                select e.siteCode, "Registration", e.patientID, p.clinicPatientID, "CODE NATIONAL", e.createDate, p.nationalID

                from itech.encounter e, itech.patient p

                                where e.patientID = p.patientID

                                                and e.encounterType in (10, 15)

                                                and e.siteCode = 95698

                                                on duplicate key update isante = values(isante);

                                               

-- CODE PC

insert into replication.validation (site, form, patient, st_code, param, collection_date, isante)

                select e.siteCode, "Registration", e.patientID, p.clinicPatientID, "CODE PC", e.createDate, o.value_text

                from itech.encounter e, itech.patient p, itech.obs o

                                where e.patientID = p.patientID

                                                and e.encounterType in (10, 15)

                                                and e.encounter_id = o.encounter_id

                                                and o.concept_id = 70039

                                                and e.siteCode = 95698

                                                on duplicate key update isante = values(isante);

                                               

-- SEXE

insert into replication.validation (site, form, patient, st_code, param, collection_date, isante)

                select e.siteCode, "Registration", e.patientID, p.clinicPatientID, "SEXE", e.createDate,

                CASE WHEN p.sex = 2 then 'M'

                WHEN .sex = 1 then 'F'

                ELSE 'U'

                END

                from itech.encounter e, itech.patient p

                                where e.patientID = p.patientID

                                                and e.encounterType in (10, 15)

                                                and e.siteCode = 95698

                on duplicate key update isante = values(isante);

                                               

-- DATE DE NAISSANCE                                  

insert into replication.validation (site, form, patient, st_code, param, collection_date, isante)

                select e.siteCode, "Registration", e.patientID, p.clinicPatientID, "DATE DE NAISSANCE", e.createDate, ymdtodate(p.dobYy, p.dobMm , p.dobDd)

                from itech.encounter e, itech.patient p

                                where e.patientID = p.patientID

                                                and e.encounterType in (10, 15)

                                                and e.siteCode = 95698

                on duplicate key update isante = values(isante);

                                               

-- ADRESSE                                         

insert into replication.validation (site, form, patient, st_code, param, collection_date, isante)

                select e.siteCode, "Registration", e.pati0entID, p.clinicPatientID, "ADRESSE", e.createDate, p.addrDistrict

                from itech.encounter e, itech.patient p

                                where e.patientID = p.patientID

                                                and e.encounterType in (10, 15)

                                                and e.siteCode = 95698

                on duplicate key update isante = values(isante);

 

 

-- TELEPHONE

insert into replication.validation (site, form, patient, st_code, param, collection_date, isante)

                select e.siteCode, "Registration", e.patientID, p.clinicPatientID, "TELEPHONE", e.createDate, p.telephone

                from itech.encounter e, itech.patient p

                                where e.patientID = p.patientID

                                                and e.encounterType in (10, 15)

                                                and e.siteCode = 95698

                                                and p.telephone <> ''

                on duplicate key update isante = values(isante);

                                               

-- STATTUT MARITAL

insert into replication.validation (site, form, patient, st_code, param, collection_date, isante)

                select e.siteCode, "Registration", e.patientID, p.clinicPatientID, "STATUT MARITAL", e.createDate,

                CASE WHEN p.maritalStatus = 1 then 'Marié (e)'

                                WHEN p.maritalStatus = 2 then 'Concubinage'

                                WHEN p.maritalStatus = 4 then 'Veuf (ve)'

                                WHEN p.maritalStatus = 8 then 'Séparé (e)'

                                WHEN p.maritalStatus = 16 then 'Célibataire'

                                WHEN p.maritalStatus = 32 then 'Inconnu'

                                ELSE ''

                                END as 'STATUT MARITAL'

                from itech.encounter e, itech.patient p

                                where e.patientID = p.patientID

                                                and e.encounterType in (10, 15)

                                                and e.siteCode = 95698

                                                and p.maritalStatus is not NULL

                on duplicate key update isante = values(isante);

 ' ||
  '' ||
   '



-- itech database (consolidated isante)
-- VIH Premiere visite adult
-- SIGNES VITAUX--------------------------------------------------------------------------------------------------------------------------------------
-- TEMP
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "TEMP", e.createDate, now(), 'x',
       CASE WHEN vitalTempUnits =1
                THEN vitalTemp
            WHEN vitalTempUnits =2
                THEN round((vitalTemp-32)*5/9,2)
            ELSE vitalTemp
           END
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.vitalTemp,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');

-- TA SYSTOLIQUE
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "TA SYSTOLIQUE", e.createDate, now(), 'x',
       CASE WHEN vitalBPUnits = 1
                THEN (vitalBp1*10 and vitalBp2*10)
           END
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.vitalBp1,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- TA DIASTOLIQUE
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "TA DIASTOLIQUE", e.createDate, now(), 'x',
       CASE WHEN vitalBPUnits =2
                THEN (vitalBp1 and vitalBp2)
           END
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.vitalBp1,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- POUL
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "POUL", e.createDate, now(), 'x', vitalHr
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.vitalHr,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- FR
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "FR", e.createDate, now(), 'x', vitalRr
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.vitalRr,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- POIDS
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "POIDS", e.createDate, now(), 'x',
       CASE WHEN vitalWeightUnits =1
                THEN vitalWeight
            WHEN vitalWeightUnits =2
                THEN round(vitalWeight*0.453592,2)
            ELSE vitalWeight
           END
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.vitalWeight,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- POIDS IL Y A 1 AN
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "POIDS IL Y A 1 AN", e.createDate, now(), 'x',
       CASE WHEN vitalPrevWtUnits =1
                THEN vitalPrevWt
            WHEN vitalPrevWtUnits =2
                THEN round(vitalPrevWt*0.453592,2)
            ELSE vitalPrevWt
           END
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.vitalPrevWt,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- TAILLE
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "TAILLE", e.createDate, now(), 'x',
       CASE WHEN vitalHeight IS NOT NULL and vitalHeightCm IS NULL
                THEN vitalHeight
            WHEN vitalHeight IS NOT NULL and vitalHeightCm IS NOT NULL
                THEN (vitalHeight + vitalHeightCm)
            WHEN vitalHeightCm IS NOT NULL and vitalHeight IS NULL
                THEN vitalHeightCm
            ELSE vitalHeightCm
           END
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.vitalHeightCm,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- TEST ANTICORPS VIH----------------------------------------------------------------------

-- DATE DU PREMIER TEST (ANTICORPS) VIH POSITIF
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "DATE DU PREMIER TEST (ANTICORPS) VIH POSITIF", e.createDate, now(), 'x', concat(firstTestYy,' ',firstTestMm)
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType =1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.firstTestYy,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- RESULTAT RECU PAR LE PATIENT
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "RESULTAT RECU PAR LE PATIENT", e.createDate, now(), 'x', firstTestResultsReceived
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.firstTestResultsReceived,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- OBSTETRIQUE ET GYNECOLOGIE--------------------------------------------------------------------------------------------------------------------------

-- GROSSESSE
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "GROSSESSE", e.createDate, now(), 'x', pregnant
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.pregnant,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- DATE DERNIERE REGLES
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "DATE DERNIERE REGLES", e.createDate, now(), 'x', concat(firstTestYy,' ',firstTestMm)
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType =1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.firstTestYy,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');



--
-- DEPISTAGE DU CANCER DU COL
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "DEPISTAGE DU CANCER DU COL", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id='146602'
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- DATE DU DEPISTAGE DU CANCER DU COL
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "DATE DU DEPISTAGE DU CANCER DU COL", e.createDate, now(), 'x', concat(firstTestYy,' ',firstTestMm)
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.firstTestYy,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- METHODE UTILISE
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "METHODE UTILISE", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id='163589'
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- RESULTAT DU TEST
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "RESULTAT DU TEST", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id='70029'
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');



-- ACTIVITÉ SEXUELLE-----------------------------------------------------------------------------------------------------------------------------------------

-- HARSAH
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "HARSAH", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id='163593'
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- TRANGENRE
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "TRANGENRE", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id='163596'
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- PS
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "PS", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id='163594'
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- UTILISATEUR DROGUE INJECTABLES
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "UTILISATEUR DROGUE INJECTABLES", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id='163597'
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- PRISONNIERS
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "PRISONNIERS", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id='163595'
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- STATUT TB--------------------------------------------------------------------------------------------------------------------------------------

-- PAS DE SIGNES OU SYMPTOMES SUGGESTIFS DE TB
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "PAS DE SIGNES OU SYMPTOMES SUGGESTIFS DE TB", e.createDate, now(), 'x', asymptomaticTb
from itech.encounter e, itech.patient p, itech.tbStatus t
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = t.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(t.visitDateYy,t.visitDateMm,t.visitDateDd)
  and e.seqnum=t.seqnum
  and ifnull(t.asymptomaticTb,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- TRAITEMENT TB COMPLETE
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "TRAITEMENT TB COMPLETE", e.createDate, now(), 'x', completeTreat
from itech.encounter e, itech.patient p, itech.tbStatus t
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = t.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(t.visitDateYy,t.visitDateMm,t.visitDateDd)
  and e.seqnum=t.seqnum
  and ifnull(t.completeTreat,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "ACTUELLEMENT SOUS TRAITEMENT TB", e.createDate, now(), 'x', currentTreat
from itech.encounter e, itech.patient p, itech.tbStatus t
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = t.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(t.visitDateYy,t.visitDateMm,t.visitDateDd)
  and e.seqnum=t.seqnum
  and ifnull(t.currentTreat,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- DATE DEBUT TRAITEMENT
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "DATE DEBUT TRAITEMENT", e.createDate, now(), 'x', concat(debutINHYy,' ',debutINHMm)
from itech.encounter e, itech.patient p, itech.tbStatus t
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = t.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(t.visitDateYy,t.visitDateMm,t.visitDateDd)
  and e.seqnum=t.seqnum
  and ifnull(t.debutINHYy,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- DATE  OU LE TRAIEMENT A ETE COMPLETE
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "DATE  OU LE TRAIEMENT A ETE COMPLETE", e.createDate, now(), 'x', concat(arretINHYy,' ',arretINHMm)
from itech.encounter e, itech.patient p, itech.tbStatus t
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = t.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(t.visitDateYy,t.visitDateMm,t.visitDateDd)
  and e.seqnum=t.seqnum
  and ifnull(t.arretINHYy,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- PLANNING FAMILIAL--------------------------------------------------------------------------------------------------------------------------------------------------------

-- METHODES PF
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "METHODES PF", e.createDate, now(), 'x', famPlan
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.famPlan,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- PRESERVATIFS
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "PRESERVATIFS", e.createDate, now(), 'x', famPlanMethodCondom
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.famPlanMethodCondom,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- DMPA
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "DMPA", e.createDate, now(), 'x', famPlanMethodDmpa
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.famPlanMethodDmpa,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- PILULES
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "PILULES", e.createDate, now(), 'x', famPlanMethodOcPills
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.famPlanMethodOcPills,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- LIGATURE DES TROMPES
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "LIGATURE DES TROMPES", e.createDate, now(), 'x', famPlanMethodTubalLig
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.famPlanMethodTubalLig,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- AUTRES
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "AUTRES", e.createDate, now(), 'x', famPlanOther
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.famPlanOther,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- ÉVALUATION TB-----------------------------------------------------------------------------------------------------------------------------------

-- SUSPICION DE TB SELON LES SYMPTOMES
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "SUSPICION DE TB SELON LES SYMPTOMES", e.createDate, now(), 'x', suspicionTBwSymptoms
from itech.encounter e, itech.patient p, itech.tbStatus t
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = t.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(t.visitDateYy,t.visitDateMm,t.visitDateDd)
  and e.seqnum=t.seqnum
  and ifnull(t.suspicionTBwSymptoms,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- PAS DE SIGNES OU SYMPTOMES SUGGESTIFS DE TB
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "PAS DE SIGNES OU SYMPTOMES SUGGESTIFS DE TB", e.createDate, now(), 'x', noTBsymptoms
from itech.encounter e, itech.patient p, itech.tbStatus t
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = t.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(t.visitDateYy,t.visitDateMm,t.visitDateDd)
  and e.seqnum=t.seqnum
  and ifnull(t.noTBsymptoms,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- PROPHYLAXIE A L'INH
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "PROPHYLAXIE A L''INH", e.createDate, now(), 'x', propINH
from itech.encounter e, itech.patient p, itech.tbStatus t
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = t.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(t.visitDateYy,t.visitDateMm,t.visitDateDd)
  and e.seqnum=t.seqnum
  and ifnull(t.propINH,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- DATE DEBUT INH
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "DATE DEBUT INH", e.createDate, now(), 'x', concat(debutINHYy' ',debutINHMm)
from itech.encounter e, itech.patient p, itech.tbStatus t
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = t.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(t.visitDateYy,t.visitDateMm,t.visitDateDd)
  and e.seqnum=t.seqnum
  and ifnull(t.debutINHYy,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- DATE ARRET INH
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "DATE ARRET INH", e.createDate, now(), 'x', concat(arretINHYy' ',arretINHMm)
from itech.encounter e, itech.patient p, itech.tbStatus t
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = t.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(t.visitDateYy,t.visitDateMm,t.visitDateDd)
  and e.seqnum=t.seqnum
  and ifnull(t.arretINHYy,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- GROSSESSE ET ALLAITEMENT-----------------------------------------------------------------------------------------------------------------------------

-- GROSSESSE
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "GROSSESSE", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id='163590'
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "ALAITEMENT", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id='163620'
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- SURVEILLANCE DU TRAITEMENT TB----------------------------------------------------------------------------------------------------------------

-- BACIILOCCOPIE
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "BACIILOCCOPIE", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id in ('163625','163632','163639','163646','163653','163660','163667','163674')
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- GENXPERT
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "GENXPERT", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and (o.concept_id in ('163626','163632','163639','163646','163653','163660','163667','163674')
    OR o.concept_id in('163627','163634','163641','163648','163653','163660','163667','163674'))
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- CULTURE
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "CULTURE", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id in ('163628','163635','163642','163649','163656','163663','163670','163677')
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- DST
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "DST", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id in ('163629','163636','163643','163650','163657','163664','163671','163678')
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- POIDS
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "POIDS", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id in ('163630','163637','163644','163651','163658','163665','163672','163679')
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- MÉDICAMENTS : ALLERGIES---------------------------------------------------------------------------------------------------------------------------------------------

-- AUCUNE
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "AUCUNE", e.createDate, now(), 'x', noneTreatments
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.noneTreatments,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- MEDICAMENTS
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "MEDICAMENTS", e.createDate, now(), 'x', allergyName
from itech.encounter e, itech.patient p, itech.allergies a
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = a.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(a.visitDateYy,a.visitDateMm,a.visitDateDd)
  and e.seqnum=a.seqnum
  and ifnull(a.allergyName,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- DEBUT
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "DEBUT", e.createDate, now(), 'x', concat(allergyStartYy,' ',allergyStartMm)
from itech.encounter e, itech.patient p, itech.allergies a
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = a.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(a.visitDateYy,a.visitDateMm,a.visitDateDd)
  and e.seqnum=a.seqnum
  and ifnull(a.allergyStartYy,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- FIN
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "FIN", e.createDate, now(), 'x', concat(allergyStopYy,' ',allergyStopMm)
from itech.encounter e, itech.patient p, itech.allergies a
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = a.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(a.visitDateYy,a.visitDateMm,a.visitDateDd)
  and e.seqnum=a.seqnum
  and ifnull(a.allergyStopYy,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- TYPE DE RÉACTION
-- Erup
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "Erup", e.createDate, now(), 'x', rash
from itech.encounter e, itech.patient p, itech.allergies a
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = a.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(a.visitDateYy,a.visitDateMm,a.visitDateDd)
  and e.seqnum=a.seqnum
  and ifnull(a.rash,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- ErupF
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "ErupF", e.createDate, now(), 'x', rashF
from itech.encounter e, itech.patient p, itech.allergies a
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = a.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(a.visitDateYy,a.visitDateMm,a.visitDateDd)
  and e.seqnum=a.seqnum
  and ifnull(a.rashF,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- ABC
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "ABC", e.createDate, now(), 'x', ABC
from itech.encounter e, itech.patient p, itech.allergies a
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = a.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(a.visitDateYy,a.visitDateMm,a.visitDateDd)
  and e.seqnum=a.seqnum
  and ifnull(a.ABC,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- Pap
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "Pap", e.createDate, now(), 'x', hives
from itech.encounter e, itech.patient p, itech.allergies a
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = a.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(a.visitDateYy,a.visitDateMm,a.visitDateDd)
  and e.seqnum=a.seqnum
  and ifnull(a.hives,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- SJ
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "SJ", e.createDate, now(), 'x', SJ
from itech.encounter e, itech.patient p, itech.allergies a
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = a.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(a.visitDateYy,a.visitDateMm,a.visitDateDd)
  and e.seqnum=a.seqnum
  and ifnull(a.SJ,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- Anaph
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "Anaph", e.createDate, now(), 'x', anaph
from itech.encounter e, itech.patient p, itech.allergies a
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = a.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(a.visitDateYy,a.visitDateMm,a.visitDateDd)
  and e.seqnum=a.seqnum
  and ifnull(a.anaph,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

-- Autre
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "Autre", e.createDate, now(), 'x', allergyOther
from itech.encounter e, itech.patient p, itech.allergies a
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = a.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(a.visitDateYy,a.visitDateMm,a.visitDateDd)
  and e.seqnum=a.seqnum
  and ifnull(a.allergyOther,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- ÉLIGIBILITÉ MÉDICALE AUX ARV---------------------------------------------------------------------------------------------------------------

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "STADE I", e.createDate, now(), 'x', currentHivStage
from itech.encounter e, itech.patient p, itech.medicalEligARVs m
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = m.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(m.visitDateYy,m.visitDateMm,m.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(m.currentHivStage,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "STADE II", e.createDate, now(), 'x', currentHivStage
from itech.encounter e, itech.patient p, itech.medicalEligARVs m
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = m.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(m.visitDateYy,m.visitDateMm,m.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(m.currentHivStage,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "POIDS", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id in ('163630','163637','163644','163651','163658','163665','163672','163679')
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- VISIT SUIVI--------------------

-- SIGNES VITAUX------------------------------------------------------------------------------------------------------------------------

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "TEMP", e.createDate, now(), 'x',
       CASE WHEN vitalTempUnits =1
                THEN vitalTemp
            WHEN vitalTempUnits =2
                THEN round((vitalTemp-32)*5/9,2)
            ELSE vitalTemp
           END
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.vitalTemp,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "TA SYSTOLIQUE", e.createDate, now(), 'x',
       CASE WHEN vitalBPUnits = 1
                THEN (vitalBp1*10 and vitalBp2*10)
           END
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.vitalBp1,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "TA DIASTOLIQUE", e.createDate, now(), 'x',
       CASE WHEN vitalBPUnits =2
                THEN (vitalBp1 and vitalBp2)
           END
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.vitalBp1,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "POUL", e.createDate, now(), 'x', vitalHr
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.vitalHr,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "FR", e.createDate, now(), 'x', vitalRr
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.vitalRr,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- test anticorps vih----------------------------------------------------------------------

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "DATE DU PREMIER TEST (ANTICORPS) VIH POSITIF", e.createDate, now(), 'x', concat(firstTestYy,' ',firstTestMm)
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType = 1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.firstTestYy,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "RESULTAT RECU PAR LE PATIENT", e.createDate, now(), 'x', firstTestResultsReceived
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.firstTestResultsReceived,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- OBSTETRIQUE ET GYNECOLOGIE--------------------------------------------------------------------------------------------------------------------------


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "GROSSESSE", e.createDate, now(), 'x', pregnant
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.pregnant,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "DATE DERNIERE REGLES", e.createDate, now(), 'x', concat(firstTestYy,' ',firstTestMm)
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType =1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.firstTestYy,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "DATE VISITE", e.createDate, now(), 'x', concat(pregnantPrenatalLastDd, pregnantPrenatalLastMm, pregnantPrenatalLastYy)
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType =1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.pregnantPrenatalLastYy,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "DATE DERNIERE VISITE", e.createDate, now(), 'x', concat(pregnantPrenatalLastDd, pregnantPrenatalLastMm, pregnantPrenatalLastYy)
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType =1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.pregnantPrenatalLastYy,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');

--
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "DEPISTAGE DU CANCER DU COL", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id='146602'
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "DATE DU DEPISTAGE DU CANCER DU COL", e.createDate, now(), 'x', concat(firstTestYy,' ',firstTestMm)
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType = 1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.firstTestYy,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "METHODE UTILISE", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id='163589'
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "RESULTAT DU TEST", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id='70029'
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- ACTIVITÉ SEXUELLE-----------------------------------------------------------------------------------------------------------------------------------------


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "HARSAH", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id='163593'
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');



insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "TRANGENRE", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id='163596'
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "PS", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id='163594'
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "UTILISATEUR DROGUE INJECTABLES", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id='163597'
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "PRISONNIERS", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id='163595'
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- STATUT TB--------------------------------------------------------------------------------------------------------------------------------------


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "PAS DE SIGNES OU SYMPTOMES SUGGESTIFS DE TB", e.createDate, now(), 'x', asymptomaticTb
from itech.encounter e, itech.patient p, itech.tbStatus t
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = t.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(t.visitDateYy,t.visitDateMm,t.visitDateDd)
  and e.seqnum=t.seqnum
  and ifnull(t.asymptomaticTb,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "TRAITEMENT TB COMPLETE", e.createDate, now(), 'x', completeTreat
from itech.encounter e, itech.patient p, itech.tbStatus t
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = t.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(t.visitDateYy,t.visitDateMm,t.visitDateDd)
  and e.seqnum=t.seqnum
  and ifnull(t.completeTreat,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "ACTUELLEMENT SOUS TRAITEMENT TB", e.createDate, now(), 'x', currentTreat
from itech.encounter e, itech.patient p, itech.tbStatus t
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = t.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(t.visitDateYy,t.visitDateMm,t.visitDateDd)
  and e.seqnum=t.seqnum
  and ifnull(t.currentTreat,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "DATE DEBUT TRAITEMENT", e.createDate, now(), 'x', concat(debutINHYy,' ',debutINHMm)
from itech.encounter e, itech.patient p, itech.tbStatus t
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = t.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(t.visitDateYy,t.visitDateMm,t.visitDateDd)
  and e.seqnum=t.seqnum
  and ifnull(t.debutINHYy,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "DATE  OU LE TRAIEMENT A ETE COMPLETE", e.createDate, now(), 'x', concat(arretINHYy,' ',arretINHMm)
from itech.encounter e, itech.patient p, itech.tbStatus t
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = t.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(t.visitDateYy,t.visitDateMm,t.visitDateDd)
  and e.seqnum=t.seqnum
  and ifnull(t.arretINHYy,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- PLANNING FAMILIAL--------------------------------------------------------------------------------------------------------------------------------------------------------


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "METHODES PF", e.createDate, now(), 'x', famPlan
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.famPlan,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "PRESERVATIFS", e.createDate, now(), 'x', famPlanMethodCondom
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.famPlanMethodCondom,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "DMPA", e.createDate, now(), 'x', famPlanMethodDmpa
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.famPlanMethodDmpa,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "PILULES", e.createDate, now(), 'x', famPlanMethodOcPills
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.famPlanMethodOcPills,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "LIGATURE DES TROMPES", e.createDate, now(), 'x', famPlanMethodTubalLig
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.famPlanMethodTubalLig,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "AUTRES", e.createDate, now(), 'x', famPlanOther
from itech.encounter e, itech.patient p, itech.vitals v
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = v.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(v.visitDateYy,v.visitDateMm,v.visitDateDd)
  and e.seqnum=v.seqnum
  and ifnull(v.famPlanOther,0)>0
    on duplicate key update isante_value = VALUES (isante), valid = if(isanteplus_value = values(isante), 'y', 'n');


-- ÉVALUATION TB-----------------------------------------------------------------------------------------------------------------------------------


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "SUSPICION DE TB SELON LES SYMPTOMES", e.createDate, now(), 'x', suspicionTBwSymptoms
from itech.encounter e, itech.patient p, itech.tbStatus t
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = t.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(t.visitDateYy,t.visitDateMm,t.visitDateDd)
  and e.seqnum=t.seqnum
  and ifnull(t.suspicionTBwSymptoms,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "PAS DE SIGNES OU SYMPTOMES SUGGESTIFS DE TB", e.createDate, now(), 'x', noTBsymptoms
from itech.encounter e, itech.patient p, itech.tbStatus t
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = t.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(t.visitDateYy,t.visitDateMm,t.visitDateDd)
  and e.seqnum=t.seqnum
  and ifnull(t.noTBsymptoms,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "PROPHYLAXIE A L''INH", e.createDate, now(), 'x', propINH
from itech.encounter e, itech.patient p, itech.tbStatus t
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = t.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(t.visitDateYy,t.visitDateMm,t.visitDateDd)
  and e.seqnum=t.seqnum
  and ifnull(t.propINH,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "DATE DEBUT INH", e.createDate, now(), 'x', concat(debutINHYy' ',debutINHMm)
from itech.encounter e, itech.patient p, itech.tbStatus t
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = t.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(t.visitDateYy,t.visitDateMm,t.visitDateDd)
  and e.seqnum=t.seqnum
  and ifnull(t.debutINHYy,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "DATE ARRET INH", e.createDate, now(), 'x', concat(arretINHYy' ',arretINHMm)
from itech.encounter e, itech.patient p, itech.tbStatus t
where e.patientID = p.patientID
  and e.encounterType=1
  and e.patientID = t.patientID
  and ymdtodate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtodate(t.visitDateYy,t.visitDateMm,t.visitDateDd)
  and e.seqnum=t.seqnum
  and ifnull(t.arretINHYy,0)>0
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- GROSSESSE ET ALLAITEMENT-----------------------------------------------------------------------------------------------------------------------------


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "GROSSESSE", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id='163590'
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "ALAITEMENT", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id='163620'
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- SURVEILLANCE DU TRAITEMENT TB----------------------------------------------------------------------------------------------------------------


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "BACIILOCCOPIE", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id in ('163625','163632','163639','163646','163653','163660','163667','163674')
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "INTAKE", e.patientID, p.clinicPatientID, "GENXPERT", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and (o.concept_id in ('163626','163632','163639','163646','163653','163660','163667','163674')
    OR o.concept_id in('163627','163634','163641','163648','163653','163660','163667','163674'))
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "CULTURE", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id in ('163628','163635','163642','163649','163656','163663','163670','163677')
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');


insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, version, is_valid, isante_value)
select e.siteCode, "FOLLOWUP", e.patientID, p.clinicPatientID, "DST", e.createDate, now(), 'x', o.value_numeric
from itech.encounter e, itech.patient p, itech.obs o
where e.patientID = p.patientID
  and e.encounterType=1
  and o.encounter_id=e.encounter_id
  and o.concept_id in ('163629','163636','163643','163650','163657','163664','163671','163678')
    on duplicate key update isante_value = VALUES (isante_value), valid = if(isanteplus_value = values(isante_value), 'y', 'n');





--
-- isanteplus database (database of any sites)
-- Registration-
-- prenom
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, 'Registration', pid.identifier as patient_id, id.identifier, 'Prenom', date(e.encounter_datetime), pn.given_name as 'prenom', 'x', date(now())
from cepoz.person_name pn
    inner join cepoz.patient p on p.patient_id = pn.person_id and p.voided=0
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 29 and e.voided=0
where pn.voided=0
on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- nom
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, 'Registration', pid.identifier as patient_id, id.identifier, 'Nom', date(e.encounter_datetime), pn.family_name as 'nom', 'x', now()
from cepoz.person_name pn
    inner join cepoz.patient p on p.patient_id = pn.person_id and p.voided=0
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 29 and e.voided=0
where pn.voided=0
on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- st_code
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, 'Registration', pid.identifier as patient_id, id.identifier, 'St_code', date(e.encounter_datetime), id.identifier as 'st_code', 'x', now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 and id.voided=0
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 29 and e.voided=0
where p.voided=0
on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- code_nationalidentifier, 'code_national' as param, date(e.encounter_datetime), pid.identifier as 'code_national', 'x', now()
--        from cepoz.patient p
--  inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 and id.voided=0
--  inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 4 and pid.voided=0
--  inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 29
--       where e.voided=0
--          on duplicate key update isanteplus_value =
-- insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
--      select 95698, 'Registration', p.patient_id, id.values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- code_pc
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, 'Registration', pid.identifier as patient_id, id.identifier, 'Code_pc', date(e.encounter_datetime), cp.identifier as 'code_pc', 'x', now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 and id.voided=0
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
    inner join cepoz.patient_identifier cp on cp.patient_id = p.patient_id and cp.identifier_type = 8 and cp.voided=0
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 29 and e.voided=0
where e.voided=0
on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- Sexe
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, 'Registration', pid.identifier as patient_id, id.identifier, 'Sexe' as param, date(e.encounter_datetime), pers.gender as 'sexe', 'x', now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3 and id.voided =0
    inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
    inner join cepoz.person pers on pers.person_id = p.patient_id
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 29 and e.voided =0
where e.voided =0
on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');


___________________________________________________Fiche vih premiere visite_____________________________________________________________________________
-- SIGNES VITAUX
-- temperature
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'temperature', date(e.encounter_datetime), o.value_numeric,'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5088 and o.encounter_id = e.encounter_id
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- ta_systolique
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'ta_systolique', date(e.encounter_datetime), o.value_numeric,'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5085 and o.encounter_id = e.encounter_id
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- ta_diastolique
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'ta_diastolique', date(e.encounter_datetime), o.value_numeric,'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5086 and o.encounter_id = e.encounter_id
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- pools
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'pools', date(e.encounter_datetime), o.value_numeric,'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5087 and o.encounter_id = e.encounter_id
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- fr
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'fr', date(e.encounter_datetime), o.value_numeric,'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5242 and o.encounter_id = e.encounter_id
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- taille
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'taille', date(e.encounter_datetime), o.value_numeric,'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5090 and o.encounter_id = e.encounter_id
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- poids
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'poids', date(e.encounter_datetime), o.value_numeric,'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5089 and o.encounter_id = e.encounter_id
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- Test Anticorps VIH
-- date_premier_test
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'date_premier_test', date(e.encounter_datetime), o.value_datetime ,'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160082 and o.encounter_id = e.encounter_id
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- resultat_recu_par_patient
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'resultat_recu_par_patient', date(e.encounter_datetime),
  case
      when o.value_coded =1065 then 'oui'
      when o.value_coded =1066 then 'non'
      else ''
  end,'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =164848 and o.encounter_id = e.encounter_id
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- OBSTETRIQUE ET GYNECOLOGIE
-- grossesse
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'grossesse', date(e.encounter_datetime),
  case
       when o.value_coded =1065 then 'oui'
       when o.value_coded =1066 then 'non'
       when o.value_coded =134346 then 'Menopause'
       when o.value_coded =1067 then 'inconnu'
       else ''
  end as 'grossesse','x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5272 and o.encounter_id = e.encounter_id
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- date_derniere_regles
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'date_derniere_regles', date(e.encounter_datetime), o.value_datetime,'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1427 and o.encounter_id = e.encounter_id
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- depistage_cancer_col***********************a revoir***

-- date_depistage_cancer_col
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'date_depistage_cancer_col', date(e.encounter_datetime), o.value_datetime,'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =165429 and o.encounter_id = e.encounter_id
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- methode_utilisee
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'methode_utilisee_depistage_col', date(e.encounter_datetime),
  case
    when o.value_coded =885 then 'Pap test'
    when o.value_coded =162816 then 'VIA'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =163589 and o.encounter_id = e.encounter_id
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- resultat_test
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'resultat_test_cancer_col', date(e.encounter_datetime),
  case
       when o.value_coded =1115 then 'Normal'
       when o.value_coded =1116 then 'Anormale'
       when o.value_coded =1067 then 'Inconnu'
       else ''
  end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160704 and o.encounter_id = e.encounter_id 
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- ACTIVITÉ SEXUELLE
-- POPULATION CLES
-- harsah
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'harsah', date(e.encounter_datetime), o.value_coded, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =509165897 and o.encounter_id = e.encounter_id and o.value_coded =160578
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- transgenre
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'transgenre', date(e.encounter_datetime), o.value_coded, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =509165897 and o.encounter_id = e.encounter_id and o.value_coded =124275
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- ps
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'ps', date(e.encounter_datetime), o.value_coded, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =509165897 and o.encounter_id = e.encounter_id and o.value_coded =160579
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- utilisateur_drogue_injectables
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'utilisateur_drogue_injectables', date(e.encounter_datetime), o.value_coded, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =509165897 and o.encounter_id = e.encounter_id and o.value_coded =105
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- prisonniers
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'prisonniers', date(e.encounter_datetime), o.value_coded, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =509165897 and o.encounter_id = e.encounter_id and o.value_coded =162277
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- STATUT TB
-- statut_tb
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'statut_tb', date(e.encounter_datetime),
  case
       when o.value_coded =1660 then 'Pas de signes ou symptômes suggestifs de la TB'
       when o.value_coded =1663 then ' Traitement contre la TB complété'
       when o.value_coded =1662 then ' Actuellement sous traitement TB  '
       else ''
   end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1659 and o.encounter_id = e.encounter_id 
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- date_debut_traitement
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'date_debut_traitement', date(e.encounter_datetime), o.value_datetime, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1113 and o.encounter_id = e.encounter_id
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- date_traitement_complete
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'date_traitement_complete', date(e.encounter_datetime), o.value_datetime, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =159431 and o.encounter_id = e.encounter_id
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- PLANNING FAMILIAL
-- planning_familial
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'planning_familial', date(e.encounter_datetime),
 case
       when o.value_coded =1 then 'oui'
       when o.value_coded =2 then 'non'
       else ''
 end, 'x',now()
        from cepoz.patient p
  inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
  inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
  inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =965 and o.encounter_id = e.encounter_id 
          on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- methodes_pf
-- preservatifs
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'preservatifs', date(e.encounter_datetime), o.value_coded, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =374 and o.encounter_id = e.encounter_id and o.value_coded =190
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- dmpa
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'dmpa', date(e.encounter_datetime), o.value_coded, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =374 and o.encounter_id = e.encounter_id and o.value_coded =5279
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- pilules
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'pilules', date(e.encounter_datetime), o.value_coded, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =374 and o.encounter_id = e.encounter_id and o.value_coded =780
          on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- ligatures_trompes
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'ligatures_trompes', date(e.encounter_datetime), o.value_coded, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =374 and o.encounter_id = e.encounter_id and o.value_coded =1472
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- autres
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'autres', date(e.encounter_datetime), o.value_coded, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =374 and o.encounter_id = e.encounter_id and o.value_coded =5622
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- ÉVALUATION TB
-- suspicion_tb_selon_symptomes
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'suspicion_tb_selon_symptomes', date(e.encounter_datetime), o.value_coded, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1659 and o.encounter_id = e.encounter_id and o.value_coded =142177
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- pas_de_signes_ou_symptomes_suggestifs_tb
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'pas_de_signes_ou_symptomes_suggestifs_tb', date(e.encounter_datetime), o.value_coded, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1659 and o.encounter_id = e.encounter_id and o.value_coded =1660
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- prophylaxie_inh
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'prophylaxie_inh', date(e.encounter_datetime), o.value_coded, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1110 and o.encounter_id = e.encounter_id and o.value_coded =1679
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- date_debut_inh
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'date_debut_inh', date(e.encounter_datetime), o.value_datetime, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =162320 and o.encounter_id = e.encounter_id
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- date_arret_inh
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'date_arret_inh', date(e.encounter_datetime), o.value_datetime, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =163284 and o.encounter_id = e.encounter_id
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- GROSSESSE ET ALLAITEMENT
-- grossesse
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'grossesse', date(e.encounter_datetime),
  case
       when o.value_coded = 1065 then 'oui'
       when o.value_coded =1066 then 'non'
       else ''
   end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1434 and o.encounter_id = e.encounter_id
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- allaitement
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'allaitement', date(e.encounter_datetime),
  case
       when o.value_coded =1065 then 'oui'
       when o.value_coded =1066 then 'non'
       else ''
   end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5632 and o.encounter_id = e.encounter_id
         on du plicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- SURVEILLANCE DU TRAITEMENT TB
-- Obs: c2c7db51-d427-4c21-9655-ae9ee604c81c
-- bacilloccopie
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'bacilloccopie', date(e.encounter_datetime),
  case
       when o.value_coded =664 then 'Négatif'
       when o.value_coded =1362 then 'Positif+'
       when o.value_coded =1363 then 'Positif++'
       when o.value_coded =1364 then 'Positif+++'
       else ''
   end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =307
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'c2c7db51-d427-4c21-9655-ae9ee604c81c'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'genxpert_bk', date(e.encounter_datetime),
  case
       when o.value_coded =1301 then 'Positif'
       when o.value_coded =1302 then 'Négatif'
       when o.value_coded =1138 then 'Indéterminé'
       else ''
  end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'c2c7db51-d427-4c21-9655-ae9ee604c81c'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- genxpert_rif -- ps: Meme uuid que genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'genxpert_rif', date(e.encounter_datetime),
  case
       when o.value_coded =162204 then 'Sensible'
       when o.value_coded =162203 then 'Resistant'
       else ''
  end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'c2c7db51-d427-4c21-9655-ae9ee604c81c'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- culture -- -- -A revoir: deuxieme uuid inexistant
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'culture', date(e.encounter_datetime),
  case
       when c2.uuid ='36d6616b-8c7c-4768-9f38-2be4b704fccd' then 'Positif pour MTB'
       when c2.uuid  = 'f4ee3bcc-947c-4390-9190-a335c2cd5868' then 'Positif pour non MTB'
       when o.value_coded  =664 then 'Négatif'
       when o.value_coded  =160008 then 'Contaminé'
       else ''
   end, 'x',now()
      from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =159982
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'c2c7db51-d427-4c21-9655-ae9ee604c81c'
 inner join cepoz.concept c2 on c2.concept_id = o.value_coded and c2.uuid in ('36d6616b-8c7c-4768-9f38-2be4b704fccd', 'f4ee3bcc-947c-4390-9190-a335c2cd5868')
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- dst
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'dst', date(e.encounter_datetime),
  case
       when o.value_coded =162204 then '"Sensible à la Rifampicine'
       when o.value_coded =162203 then 'Résistant à la rifampicine'
       when o.value_coded =1138 then 'Indéterminé'
       else ''
  end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id =159984
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'c2c7db51-d427-4c21-9655-ae9ee604c81c'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- poids
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'poids', date(e.encounter_datetime), o.value_numeric, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id = 5089
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'c2c7db51-d427-4c21-9655-ae9ee604c81c'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- Obs: b05762eb-b8f5-4af2-8105-cc68a6dc9d3b
-- bacilloccopie
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'bacilloccopie', date(e.encounter_datetime),
  case
       when o.value_coded =664 then 'Négatif'
       when o.value_coded =1362 then 'Positif+'
       when o.value_coded =1363 then 'Positif++'
       when o.value_coded =1364 then 'Positif+++'
       else ''
 end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =307
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'b05762eb-b8f5-4af2-8105-cc68a6dc9d3b'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'genxpert_bk', date(e.encounter_datetime),
 case
       when o.value_coded =1301 then 'Positif'
       when o.value_coded =1302 then 'Négatif'
       when o.value_coded =1138 then 'Indéterminé'
       else ''
 end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'b05762eb-b8f5-4af2-8105-cc68a6dc9d3b'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- genxpert_rif -- ps: Meme uuid que genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'genxpert_rif', date(e.encounter_datetime),
case
       when o.value_coded =162204 then 'Sensible'
       when o.value_coded =162203 then 'Resistant'
       else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'b05762eb-b8f5-4af2-8105-cc68a6dc9d3b'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- culture -- -- -A revoir: deuxieme uuid inexistant
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'culture', date(e.encounter_datetime),
 case
       when c2.uuid ='36d6616b-8c7c-4768-9f38-2be4b704fccd' then 'Positif pour MTB'
       when c2.uuid  = 'f4ee3bcc-947c-4390-9190-a335c2cd5868' then 'Positif pour non MTB'
       when o.value_coded  =664 then 'Négatif'
       when o.value_coded  =160008 then 'Contaminé'
       else ''
 end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =159982
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'b05762eb-b8f5-4af2-8105-cc68a6dc9d3b'
 inner join cepoz.concept c2 on c2.concept_id = o.value_coded and c2.uuid in ('36d6616b-8c7c-4768-9f38-2be4b704fccd', 'f4ee3bcc-947c-4390-9190-a335c2cd5868')
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- dst
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'dst', date(e.encounter_datetime),
  case
       when o.value_coded =162204 then '"Sensible à la Rifampicine'
       when o.value_coded =162203 then 'Résistant à la rifampicine'
       when o.value_coded =1138 then 'Indéterminé'
       else ''
  end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id =159984
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'b05762eb-b8f5-4af2-8105-cc68a6dc9d3b'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- poids
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'poids', date(e.encounter_datetime), o.value_numeric, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id = 5089
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'b05762eb-b8f5-4af2-8105-cc68a6dc9d3b'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- Obs: be7a27bb-2dcd-4bff-b696-ccdbbd2d3192
-- bacilloccopie
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "intake", p.patient_id, id.identifier, 'bacilloccopie', date(e.encounter_datetime),
    case
    when o.value_coded =664 then 'Négatif'
    when o.value_coded =1362 then 'Positif+'
    when o.value_coded =1363 then 'Positif++'
    when o.value_coded =1364 then 'Positif+++'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =307
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'be7a27bb-2dcd-4bff-b696-ccdbbd2d3192'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "intake", p.patient_id, id.identifier, 'genxpert_bk', date(e.encounter_datetime),
    case
    when o.value_coded =1301 then 'Positif'
    when o.value_coded =1302 then 'Négatif'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'be7a27bb-2dcd-4bff-b696-ccdbbd2d3192'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- genxpert_rif -- ps: Meme uuid que genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "intake", p.patient_id, id.identifier, 'genxpert_rif', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then 'Sensible'
    when o.value_coded =162203 then 'Resistant'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'be7a27bb-2dcd-4bff-b696-ccdbbd2d3192'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- culture -- -- -A revoir: deuxieme uuid inexistant
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "intake", p.patient_id, id.identifier, 'culture', date(e.encounter_datetime),
    case
    when c2.uuid ='36d6616b-8c7c-4768-9f38-2be4b704fccd' then 'Positif pour MTB'
    when c2.uuid  = 'f4ee3bcc-947c-4390-9190-a335c2cd5868' then 'Positif pour non MTB'
    when o.value_coded  =664 then 'Négatif'
    when o.value_coded  =160008 then 'Contaminé'
    else ''
end, 'x',now()
      from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =159982
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'be7a27bb-2dcd-4bff-b696-ccdbbd2d3192'
 inner join cepoz.concept c2 on c2.concept_id = o.value_coded and c2.uuid in ('36d6616b-8c7c-4768-9f38-2be4b704fccd', 'f4ee3bcc-947c-4390-9190-a335c2cd5868')
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- dst
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "intake", p.patient_id, id.identifier, 'dst', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then '"Sensible à la Rifampicine'
    when o.value_coded =162203 then 'Résistant à la rifampicine'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id =159984
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'be7a27bb-2dcd-4bff-b696-ccdbbd2d3192'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- poids
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "intake", p.patient_id, id.identifier, 'poids', date(e.encounter_datetime), o.value_numeric, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
    inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id = 5089
    inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'be7a27bb-2dcd-4bff-b696-ccdbbd2d3192'
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- Obs: fdd9d953-004a-4e9a-ab93-9afaa0e090fa
-- bacilloccopie
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "intake", p.patient_id, id.identifier, 'bacilloccopie', date(e.encounter_datetime),
    case
    when o.value_coded =664 then 'Négatif'
    when o.value_coded =1362 then 'Positif+'
    when o.value_coded =1363 then 'Positif++'
    when o.value_coded =1364 then 'Positif+++'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =307
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'fdd9d953-004a-4e9a-ab93-9afaa0e090fa'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "intake", p.patient_id, id.identifier, 'genxpert_bk', date(e.encounter_datetime),
    case
    when o.value_coded =1301 then 'Positif'
    when o.value_coded =1302 then 'Négatif'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'fdd9d953-004a-4e9a-ab93-9afaa0e090fa'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- genxpert_rif -- ps: Meme uuid que genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "intake", p.patient_id, id.identifier, 'genxpert_rif', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then 'Sensible'
    when o.value_coded =162203 then 'Resistant'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'fdd9d953-004a-4e9a-ab93-9afaa0e090fa'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- culture -- -- -A revoir: deuxieme uuid inexistant
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "intake", p.patient_id, id.identifier, 'culture', date(e.encounter_datetime),
    case
    when c2.uuid ='36d6616b-8c7c-4768-9f38-2be4b704fccd' then 'Positif pour MTB'
    when c2.uuid  = 'f4ee3bcc-947c-4390-9190-a335c2cd5868' then 'Positif pour non MTB'
    when o.value_coded  =664 then 'Négatif'
    when o.value_coded  =160008 then 'Contaminé'
    else ''
end, 'x',now()
      from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =159982
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'fdd9d953-004a-4e9a-ab93-9afaa0e090fa'
 inner join cepoz.concept c2 on c2.concept_id = o.value_coded and c2.uuid in ('36d6616b-8c7c-4768-9f38-2be4b704fccd', 'f4ee3bcc-947c-4390-9190-a335c2cd5868')
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- dst
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "intake", p.patient_id, id.identifier, 'dst', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then '"Sensible à la Rifampicine'
    when o.value_coded =162203 then 'Résistant à la rifampicine'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id =159984
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'fdd9d953-004a-4e9a-ab93-9afaa0e090fa'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- poids
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "intake", p.patient_id, id.identifier, 'poids', date(e.encounter_datetime), o.value_numeric, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
    inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id = 5089
    inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'fdd9d953-004a-4e9a-ab93-9afaa0e090fa'
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- Obs: dc12da69-937e-4cfc-afed-a2f07b44eff4
-- bacilloccopie
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "intake", p.patient_id, id.identifier, 'bacilloccopie', date(e.encounter_datetime),
    case
    when o.value_coded =664 then 'Négatif'
    when o.value_coded =1362 then 'Positif+'
    when o.value_coded =1363 then 'Positif++'
    when o.value_coded =1364 then 'Positif+++'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =307
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'dc12da69-937e-4cfc-afed-a2f07b44eff4'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "intake", p.patient_id, id.identifier, 'genxpert_bk', date(e.encounter_datetime),
    case
    when o.value_coded =1301 then 'Positif'
    when o.value_coded =1302 then 'Négatif'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'dc12da69-937e-4cfc-afed-a2f07b44eff4'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- genxpert_rif -- ps: Meme uuid que genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "intake", p.patient_id, id.identifier, 'genxpert_rif', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then 'Sensible'
    when o.value_coded =162203 then 'Resistant'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'dc12da69-937e-4cfc-afed-a2f07b44eff4'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- culture -- -- -A revoir: deuxieme uuid inexistant
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "intake", p.patient_id, id.identifier, 'culture', date(e.encounter_datetime),
    case
    when c2.uuid ='36d6616b-8c7c-4768-9f38-2be4b704fccd' then 'Positif pour MTB'
    when c2.uuid  = 'f4ee3bcc-947c-4390-9190-a335c2cd5868' then 'Positif pour non MTB'
    when o.value_coded  =664 then 'Négatif'
    when o.value_coded  =160008 then 'Contaminé'
    else ''
end, 'x',now()
      from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =159982
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'dc12da69-937e-4cfc-afed-a2f07b44eff4'
 inner join cepoz.concept c2 on c2.concept_id = o.value_coded and c2.uuid in ('36d6616b-8c7c-4768-9f38-2be4b704fccd', 'f4ee3bcc-947c-4390-9190-a335c2cd5868')
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- dst
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "intake", p.patient_id, id.identifier, 'dst', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then '"Sensible à la Rifampicine'
    when o.value_coded =162203 then 'Résistant à la rifampicine'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id =159984
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'dc12da69-937e-4cfc-afed-a2f07b44eff4'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- poids
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'poids', date(e.encounter_datetime), o.value_numeric, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id = 5089
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'dc12da69-937e-4cfc-afed-a2f07b44eff4'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- Obs: 224546bb-03da-4793-9ef6-3d42adc5b353
-- bacilloccopie
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'bacilloccopie', date(e.encounter_datetime),
  case
       when o.value_coded =664 then 'Négatif'
       when o.value_coded =1362 then 'Positif+'
       when o.value_coded =1363 then 'Positif++'
       when o.value_coded =1364 then 'Positif+++'
       else ''
  end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =307
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '224546bb-03da-4793-9ef6-3d42adc5b353'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'genxpert_bk', date(e.encounter_datetime),
  case
       when o.value_coded =1301 then 'Positif'
       when o.value_coded =1302 then 'Négatif'
       when o.value_coded =1138 then 'Indéterminé'
       else ''
  end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '224546bb-03da-4793-9ef6-3d42adc5b353'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- genxpert_rif -- ps: Meme uuid que genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'genxpert_rif', date(e.encounter_datetime),
 case
       when o.value_coded =162204 then 'Sensible'
       when o.value_coded =162203 then 'Resistant'
       else ''
 end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '224546bb-03da-4793-9ef6-3d42adc5b353'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- culture -- -- -A revoir: deuxieme uuid inexistant
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'culture', date(e.encounter_datetime),
 case
       when c2.uuid ='36d6616b-8c7c-4768-9f38-2be4b704fccd' then 'Positif pour MTB'
       when c2.uuid  = 'f4ee3bcc-947c-4390-9190-a335c2cd5868' then 'Positif pour non MTB'
       when o.value_coded  =664 then 'Négatif'
       when o.value_coded  =160008 then 'Contaminé'
       else ''
 end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =159982
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '224546bb-03da-4793-9ef6-3d42adc5b353'
 inner join cepoz.concept c2 on c2.concept_id = o.value_coded and c2.uuid in ('36d6616b-8c7c-4768-9f38-2be4b704fccd', 'f4ee3bcc-947c-4390-9190-a335c2cd5868')
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- dst
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'dst', date(e.encounter_datetime),
 case
       when o.value_coded =162204 then '"Sensible à la Rifampicine'
       when o.value_coded =162203 then 'Résistant à la rifampicine'
       when o.value_coded =1138 then 'Indéterminé'
       else ''
 end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id =159984
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '224546bb-03da-4793-9ef6-3d42adc5b353'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- poids
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'poids', date(e.encounter_datetime), o.value_numeric, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id = 5089
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '224546bb-03da-4793-9ef6-3d42adc5b353'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- Obs: 67f6a83d-5aa2-4da5-950c-833dadfa8916
-- bacilloccopie
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'bacilloccopie', date(e.encounter_datetime),
 case
       when o.value_coded =664 then 'Négatif'
       when o.value_coded =1362 then 'Positif+'
       when o.value_coded =1363 then 'Positif++'
       when o.value_coded =1364 then 'Positif+++'
       else ''
 end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =307
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '67f6a83d-5aa2-4da5-950c-833dadfa8916'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'genxpert_bk', date(e.encounter_datetime),
  case
       when o.value_coded =1301 then 'Positif'
       when o.value_coded =1302 then 'Négatif'
       when o.value_coded =1138 then 'Indéterminé'
       else ''
  end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '67f6a83d-5aa2-4da5-950c-833dadfa8916'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- genxpert_rif -- ps: Meme uuid que genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'genxpert_rif', date(e.encounter_datetime),
  case
       when o.value_coded =162204 then 'Sensible'
       when o.value_coded =162203 then 'Resistant'
       else ''
  end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '67f6a83d-5aa2-4da5-950c-833dadfa8916'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- culture -- -- -A revoir: deuxieme uuid inexistant
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'culture', date(e.encounter_datetime),
 case
       when c2.uuid ='36d6616b-8c7c-4768-9f38-2be4b704fccd' then 'Positif pour MTB'
       when c2.uuid  = 'f4ee3bcc-947c-4390-9190-a335c2cd5868' then 'Positif pour non MTB'
       when o.value_coded  =664 then 'Négatif'
       when o.value_coded  =160008 then 'Contaminé'
       else ''
 end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =159982
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '67f6a83d-5aa2-4da5-950c-833dadfa8916'
 inner join cepoz.concept c2 on c2.concept_id = o.value_coded and c2.uuid in ('36d6616b-8c7c-4768-9f38-2be4b704fccd', 'f4ee3bcc-947c-4390-9190-a335c2cd5868')
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- dst
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'dst', date(e.encounter_datetime),
  case
       when o.value_coded =162204 then '"Sensible à la Rifampicine'
       when o.value_coded =162203 then 'Résistant à la rifampicine'
       when o.value_coded =1138 then 'Indéterminé'
       else ''
  end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id =159984
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '67f6a83d-5aa2-4da5-950c-833dadfa8916'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- poids
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'poids', date(e.encounter_datetime), o.value_numeric, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id = 5089
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '67f6a83d-5aa2-4da5-950c-833dadfa8916'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- MÉDICAMENTS : ALLERGIES  -- A revoir
-- medicaments_allergies_aucun
insert into replication.validation(site, form, patient, st_code, param, collection_date, isanteplus, is_valid, version)
     select 95698, "INTAKE", p.patient_id, id.identifier, 'medicaments_allergies_aucun', date(e.encounter_datetime), o.value_coded, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id = 160643  and o.encounter_id = e.encounter_id and o.value_coded =1107
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- ÉLIGIBILITÉ MÉDICALE AUX ARV
-- eligibilite_medicale_aux_arv
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'eligibilite_medicale_aux_arv', date(e.encounter_datetime),
  case
       when o.value_coded =1065 then 'Oui - préciser la raison'
       when o.value_coded =1066 then "Non - pas d'éligibilité médicale aujourd'hui"
       when o.value_coded =1067 then 'À déterminer'
       else ''
  end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =162703 and o.encounter_id = e.encounter_id
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- stade
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'stade', date(e.encounter_datetime),
  case
       when o.value_coded =1204 then 'Stade I (Asymptomatique'
       when o.value_coded =1205 then 'Stade II (Symptomatique)'
       when o.value_coded =1206 then 'Stade III (Symptomatique)'
       when o.value_coded =1207 then 'Stade IV (SIDA)'
       else ''
  end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5356 and o.encounter_id = e.encounter_id
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- date_prochaine_visite
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "intake", p.patient_id, id.identifier, 'date_prochaine_visite', date(e.encounter_datetime), o.value_datetime, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 1
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5096 and o.encounter_id = e.encounter_id
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- -- -- -- -- -- -- ----------------------------------------------Fiche visite suivie---------------------------------------------------------------
--SIGNES VITAUX
--temperature
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'temperarute', date(e.encounter_datetime), o.value_numeric,'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5088 and o.encounter_id = e.encounter_id
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- ta_systolique
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'ta_systolique', date(e.encounter_datetime), o.value_numeric,'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5085 and o.encounter_id = e.encounter_id
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- ta_diastolique
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'ta_diastolique', date(e.encounter_datetime), o.value_numeric,'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5086 and o.encounter_id = e.encounter_id
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- pools
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'pools', date(e.encounter_datetime), o.value_numeric,'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5087 and o.encounter_id = e.encounter_id
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- fr
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'fr', date(e.encounter_datetime), o.value_numeric,'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5242 and o.encounter_id = e.encounter_id
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- OBSTETRIQUE ET GYNECOLOGIE
-- grossesse
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'grossesse', date(e.encounter_datetime),
    CASE
    WHEN o.value_coded =1065 THEN 'oui'
    WHEN o.value_coded =1066 THEN 'non'
    WHEN o.value_coded =134346 THEN 'Menopause'
    WHEN o.value_coded =1067 THEN 'inconnu'
    ELSE ''
END as 'grossesse','x',now()
        from cepoz.patient p
  inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
  inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
  inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5272 and o.encounter_id = e.encounter_id
          on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- date_derniere_regles
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'date_derniere_regles', date(e.encounter_datetime), o.value_datetime,'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1427 and o.encounter_id = e.encounter_id
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- date visite //non fait

-- date_derniere_visite
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'date_derniere_visite', date(e.encounter_datetime), o.value_datetime ,'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =164093 and o.encounter_id = e.encounter_id
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- Despistage cancer du Col : Oui Non

-- date_depistage_cancer_col
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'date_depistage_cancer_col', date(e.encounter_datetime), o.value_datetime,'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =165429 and o.encounter_id = e.encounter_id
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- methode_utilisee
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'methode_utilisee', date(e.encounter_datetime),
    case
    when o.value_coded =885 then 'Pap test'
    when o.value_coded =162816 then 'VIA'
    ELSE ''
end, 'x',now()
        from cepoz.patient p
  inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
  inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
  inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =163589 and o.encounter_id = e.encounter_id
          on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- resultat_test
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'resultat_test', date(e.encounter_datetime),
    case
    when o.value_coded =1115 then 'Normal'
    when o.value_coded =1116 then 'Anormale'
    when o.value_coded =1067 then 'Inconnu'
    else ''
end, 'x',now()
        from cepoz.patient p
  inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
  inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
  inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160704 and o.encounter_id = e.encounter_id
          on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- POPULATION CLES
-- harsah
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'harsah', date(e.encounter_datetime), o.value_coded, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =509165897 and o.encounter_id = e.encounter_id and o.value_coded =160578
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--     transgenre
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'transgenre', date(e.encounter_datetime), o.value_coded, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =509165897 and o.encounter_id = e.encounter_id and o.value_coded =124275
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--     ps
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'ps', date(e.encounter_datetime), o.value_coded, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =509165897 and o.encounter_id = e.encounter_id and o.value_coded =160579
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--     utilisateur_drogue_injectables
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'utilisateur_drogue_injectables', date(e.encounter_datetime), o.value_coded, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =509165897 and o.encounter_id = e.encounter_id and o.value_coded =105
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--     prisonniers
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'prisonniers', date(e.encounter_datetime), o.value_coded, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 2
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 8
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =509165897 and o.encounter_id = e.encounter_id and o.value_coded =162277
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--     STATUT TB
--     statut_tb
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'statut_tb', date(e.encounter_datetime),
    case
    when o.value_coded =1660 then 'Pas de signes ou symptômes suggestifs de la TB'
    when o.value_coded =1663 then ' Traitement contre la TB complété'
    when o.value_coded =1662 then ' Actuellement sous traitement TB  '
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1659 and o.encounter_id = e.encounter_id
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--     date_debut_traitement
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'date_debut_traitement_tb', date(e.encounter_datetime), o.value_datetime, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1113 and o.encounter_id = e.encounter_id
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--     date_traitement_complete
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'date_traitement_complete_tb', date(e.encounter_datetime), o.value_datetime, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =159431 and o.encounter_id = e.encounter_id
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--    PLANNING FAMILIAL
--    planning_familial
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'planning_familial', date(e.encounter_datetime),
    case
    when o.value_coded =1 then 'oui'
    when o.value_coded =2 then 'non'
    else ''
end, 'x',now()
        from cepoz.patient p
  inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
  inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
  inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =965 and o.encounter_id = e.encounter_id
          on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--    methodes_pf
--    preservatifs
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'preservatifs', date(e.encounter_datetime), o.value_coded, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =374 and o.encounter_id = e.encounter_id and o.value_coded =190
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--    dmpa
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'dmpa', date(e.encounter_datetime), o.value_coded, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =374 and o.encounter_id = e.encounter_id and o.value_coded =5279
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--    pilules
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'pilules', date(e.encounter_datetime), o.value_coded, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =374 and o.encounter_id = e.encounter_id and o.value_coded =780
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--    ligatures_trompes
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'ligatures_trompes', date(e.encounter_datetime), o.value_coded, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =374 and o.encounter_id = e.encounter_id and o.value_coded =1472
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--    autres
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'autres', date(e.encounter_datetime), o.value_coded, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =374 and o.encounter_id = e.encounter_id and o.value_coded =5622
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--   ÉVALUATION TB
--   suspicion_tb_selon_symptomes
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'suspicion_tb_selon_symptomes', date(e.encounter_datetime), o.value_coded, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1659 and o.encounter_id = e.encounter_id and o.value_coded =142177
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--   pas_de_signes_ou_symptomes_suggestifs_tb
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'pas_de_signes_ou_symptomes_suggestifs_tb', date(e.encounter_datetime), o.value_coded, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1659 and o.encounter_id = e.encounter_id and o.value_coded =1660
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--   prophylaxie_inh
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'prophylaxie_inh', date(e.encounter_datetime), o.value_coded, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1110 and o.encounter_id = e.encounter_id and o.value_coded =1679
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--   date_debut_inh
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'date_debut_inh', date(e.encounter_datetime), o.value_datetime, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =162320 and o.encounter_id = e.encounter_id
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--   date_arret_inh
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'date_arret_inh', date(e.encounter_datetime), o.value_datetime, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =163284 and o.encounter_id = e.encounter_id
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--   GROSSESSE ET ALLAITEMENT
--   grossesse
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'grossesse', date(e.encounter_datetime),
    case
    when o.value_coded = 1065 then 'oui'
    when o.value_coded = 1066 then 'non'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1434 and o.encounter_id = e.encounter_id
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--   allaitement
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'allaitement', date(e.encounter_datetime),
    case
    when o.value_coded =1065 then 'oui'
    when o.value_coded =1066 then 'non'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5632 and o.encounter_id = e.encounter_id
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--   SURVEILLANCE DU TRAITEMENT TB
--   Obs: c2c7db51-d427-4c21-9655-ae9ee604c81c
--   bacilloccopie
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'bacilloccopie', date(e.encounter_datetime),
    case
    when o.value_coded =664 then 'Négatif'
    when o.value_coded =1362 then 'Positif+'
    when o.value_coded =1363 then 'Positif++'
    when o.value_coded =1364 then 'Positif+++'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =307
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'c2c7db51-d427-4c21-9655-ae9ee604c81c'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--  genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'genxpert_bk', date(e.encounter_datetime),
    case
    when o.value_coded =1301 then 'Positif'
    when o.value_coded =1302 then 'Négatif'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'c2c7db51-d427-4c21-9655-ae9ee604c81c'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--  genxpert_rif --  ps: Meme uuid que genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'genxpert_rif', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then 'Sensible'
    when o.value_coded =162203 then 'Resistant'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'c2c7db51-d427-4c21-9655-ae9ee604c81c'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--  culture --  --  -A revoir: deuxieme uuid inexistant
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'culture', date(e.encounter_datetime),
    case
    when c2.uuid ='36d6616b-8c7c-4768-9f38-2be4b704fccd' then 'Positif pour MTB'
    when c2.uuid  = 'f4ee3bcc-947c-4390-9190-a335c2cd5868' then 'Positif pour non MTB'
    when o.value_coded  =664 then 'Négatif'
    when o.value_coded  =160008 then 'Contaminé'
    else ''
end, 'x',now()
      from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =159982
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'c2c7db51-d427-4c21-9655-ae9ee604c81c'
 inner join cepoz.concept c2 on c2.concept_id = o.value_coded and c2.uuid in ('36d6616b-8c7c-4768-9f38-2be4b704fccd', 'f4ee3bcc-947c-4390-9190-a335c2cd5868')
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--  dst
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'dst', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then '"Sensible à la Rifampicine'
    when o.value_coded =162203 then 'Résistant à la rifampicine'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id =159984
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'c2c7db51-d427-4c21-9655-ae9ee604c81c'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--  poids
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'poids', date(e.encounter_datetime), o.value_numeric, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id = 5089
    inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'c2c7db51-d427-4c21-9655-ae9ee604c81c'
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--  Obs: b05762eb-b8f5-4af2-8105-cc68a6dc9d3b
--  bacilloccopie
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'bacilloccopie', date(e.encounter_datetime),
    case
    when o.value_coded =664 then 'Négatif'
    when o.value_coded =1362 then 'Positif+'
    when o.value_coded =1363 then 'Positif++'
    when o.value_coded =1364 then 'Positif+++'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =307
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'b05762eb-b8f5-4af2-8105-cc68a6dc9d3b'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--  genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'genxpert_bk', date(e.encounter_datetime),
    case
    when o.value_coded =1301 then 'Positif'
    when o.value_coded =1302 then 'Négatif'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'b05762eb-b8f5-4af2-8105-cc68a6dc9d3b'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--  genxpert_rif --  ps: Meme uuid que genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'genxpert_rif', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then 'Sensible'
    when o.value_coded =162203 then 'Resistant'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'b05762eb-b8f5-4af2-8105-cc68a6dc9d3b'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--  culture --  --  -A revoir: deuxieme uuid inexistant
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'culture', date(e.encounter_datetime),
    case
    when c2.uuid ='36d6616b-8c7c-4768-9f38-2be4b704fccd' then 'Positif pour MTB'
    when c2.uuid  = 'f4ee3bcc-947c-4390-9190-a335c2cd5868' then 'Positif pour non MTB'
    when o.value_coded  =664 then 'Négatif'
    when o.value_coded  =160008 then 'Contaminé'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =159982
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'b05762eb-b8f5-4af2-8105-cc68a6dc9d3b'
 inner join cepoz.concept c2 on c2.concept_id = o.value_coded and c2.uuid in ('36d6616b-8c7c-4768-9f38-2be4b704fccd', 'f4ee3bcc-947c-4390-9190-a335c2cd5868')
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--  dst
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'dst', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then '"Sensible à la Rifampicine'
    when o.value_coded =162203 then 'Résistant à la rifampicine'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id =159984
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'b05762eb-b8f5-4af2-8105-cc68a6dc9d3b'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--  poids
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'poids', date(e.encounter_datetime), o.value_numeric, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id = 5089
    inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'b05762eb-b8f5-4af2-8105-cc68a6dc9d3b'
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--  Obs: be7a27bb-2dcd-4bff-b696-ccdbbd2d3192
--  bacilloccopie
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'bacilloccopie', date(e.encounter_datetime),
    case
    when o.value_coded =664 then 'Négatif'
    when o.value_coded =1362 then 'Positif+'
    when o.value_coded =1363 then 'Positif++'
    when o.value_coded =1364 then 'Positif+++'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =307
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'be7a27bb-2dcd-4bff-b696-ccdbbd2d3192'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--  genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'genxpert_bk', date(e.encounter_datetime),
    case
    when o.value_coded =1301 then 'Positif'
    when o.value_coded =1302 then 'Négatif'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'be7a27bb-2dcd-4bff-b696-ccdbbd2d3192'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--  genxpert_rif --  ps: Meme uuid que genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'genxpert_rif', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then 'Sensible'
    when o.value_coded =162203 then 'Resistant'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'be7a27bb-2dcd-4bff-b696-ccdbbd2d3192'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--  culture --  --  -A revoir: deuxieme uuid inexistant
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'culture', date(e.encounter_datetime),
    case
    when c2.uuid ='36d6616b-8c7c-4768-9f38-2be4b704fccd' then 'Positif pour MTB'
    when c2.uuid  = 'f4ee3bcc-947c-4390-9190-a335c2cd5868' then 'Positif pour non MTB'
    when o.value_coded  =664 then 'Négatif'
    when o.value_coded  =160008 then 'Contaminé'
    else ''
end, 'x',now()
      from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =159982
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'be7a27bb-2dcd-4bff-b696-ccdbbd2d3192'
 inner join cepoz.concept c2 on c2.concept_id = o.value_coded and c2.uuid in ('36d6616b-8c7c-4768-9f38-2be4b704fccd', 'f4ee3bcc-947c-4390-9190-a335c2cd5868')
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--  dst
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'dst', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then '"Sensible à la Rifampicine'
    when o.value_coded =162203 then 'Résistant à la rifampicine'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id =159984
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'be7a27bb-2dcd-4bff-b696-ccdbbd2d3192'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--  poids
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'poids', date(e.encounter_datetime), o.value_numeric, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id = 5089
    inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'be7a27bb-2dcd-4bff-b696-ccdbbd2d3192'
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--  Obs: fdd9d953-004a-4e9a-ab93-9afaa0e090fa
--  bacilloccopie
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'bacilloccopie', date(e.encounter_datetime),
    case
    when o.value_coded =664 then 'Négatif'
    when o.value_coded =1362 then 'Positif+'
    when o.value_coded =1363 then 'Positif++'
    when o.value_coded =1364 then 'Positif+++'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =307
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'fdd9d953-004a-4e9a-ab93-9afaa0e090fa'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--  genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'genxpert_bk', date(e.encounter_datetime),
    case
    when o.value_coded =1301 then 'Positif'
    when o.value_coded =1302 then 'Négatif'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'fdd9d953-004a-4e9a-ab93-9afaa0e090fa'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--  genxpert_rif --  ps: Meme uuid que genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'genxpert_rif', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then 'Sensible'
    when o.value_coded =162203 then 'Resistant'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'fdd9d953-004a-4e9a-ab93-9afaa0e090fa'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--  culture --  --  -A revoir: deuxieme uuid inexistant
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'culture', date(e.encounter_datetime),
    case
    when c2.uuid ='36d6616b-8c7c-4768-9f38-2be4b704fccd' then 'Positif pour MTB'
    when c2.uuid  = 'f4ee3bcc-947c-4390-9190-a335c2cd5868' then 'Positif pour non MTB'
    when o.value_coded  =664 then 'Négatif'
    when o.value_coded  =160008 then 'Contaminé'
    else ''
end, 'x',now()
      from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =159982
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'fdd9d953-004a-4e9a-ab93-9afaa0e090fa'
 inner join cepoz.concept c2 on c2.concept_id = o.value_coded and c2.uuid in ('36d6616b-8c7c-4768-9f38-2be4b704fccd', 'f4ee3bcc-947c-4390-9190-a335c2cd5868')
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--  dst
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'dst', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then '"Sensible à la Rifampicine'
    when o.value_coded =162203 then 'Résistant à la rifampicine'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id =159984
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'fdd9d953-004a-4e9a-ab93-9afaa0e090fa'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--  poids
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'poids', date(e.encounter_datetime), o.value_numeric, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id = 5089
    inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'fdd9d953-004a-4e9a-ab93-9afaa0e090fa'
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--  Obs: dc12da69-937e-4cfc-afed-a2f07b44eff4
--  bacilloccopie
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'bacilloccopie', date(e.encounter_datetime),
    case
    when o.value_coded =664 then 'Négatif'
    when o.value_coded =1362 then 'Positif+'
    when o.value_coded =1363 then 'Positif++'
    when o.value_coded =1364 then 'Positif+++'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =307
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'dc12da69-937e-4cfc-afed-a2f07b44eff4'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

--  genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'genxpert_bk', date(e.encounter_datetime),
    case
    when o.value_coded =1301 then 'Positif'
    when o.value_coded =1302 then 'Négatif'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'dc12da69-937e-4cfc-afed-a2f07b44eff4'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- genxpert_rif -- ps: Meme uuid que genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'genxpert_rif', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then 'Sensible'
    when o.value_coded =162203 then 'Resistant'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'dc12da69-937e-4cfc-afed-a2f07b44eff4'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- culture -- -- -A revoir: deuxieme uuid inexistant
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'culture', date(e.encounter_datetime),
    case
    when c2.uuid ='36d6616b-8c7c-4768-9f38-2be4b704fccd' then 'Positif pour MTB'
    when c2.uuid  = 'f4ee3bcc-947c-4390-9190-a335c2cd5868' then 'Positif pour non MTB'
    when o.value_coded  =664 then 'Négatif'
    when o.value_coded  =160008 then 'Contaminé'
    else ''
end, 'x',now()
      from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =159982
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'dc12da69-937e-4cfc-afed-a2f07b44eff4'
 inner join cepoz.concept c2 on c2.concept_id = o.value_coded and c2.uuid in ('36d6616b-8c7c-4768-9f38-2be4b704fccd', 'f4ee3bcc-947c-4390-9190-a335c2cd5868')
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- dst
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'dst', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then '"Sensible à la Rifampicine'
    when o.value_coded =162203 then 'Résistant à la rifampicine'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id =159984
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'dc12da69-937e-4cfc-afed-a2f07b44eff4'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- poids
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'poids', date(e.encounter_datetime), o.value_numeric, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id = 5089
    inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = 'dc12da69-937e-4cfc-afed-a2f07b44eff4'
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- Obs: 224546bb-03da-4793-9ef6-3d42adc5b353
-- bacilloccopie
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'bacilloccopie', date(e.encounter_datetime),
    case
    when o.value_coded =664 then 'Négatif'
    when o.value_coded =1362 then 'Positif+'
    when o.value_coded =1363 then 'Positif++'
    when o.value_coded =1364 then 'Positif+++'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =307
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '224546bb-03da-4793-9ef6-3d42adc5b353'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'genxpert_bk', date(e.encounter_datetime),
    case
    when o.value_coded =1301 then 'Positif'
    when o.value_coded =1302 then 'Négatif'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '224546bb-03da-4793-9ef6-3d42adc5b353'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- genxpert_rif -- ps: Meme uuid que genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'genxpert_rif', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then 'Sensible'
    when o.value_coded =162203 then 'Resistant'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '224546bb-03da-4793-9ef6-3d42adc5b353'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- culture -- -- -A revoir: deuxieme uuid inexistant
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'culture', date(e.encounter_datetime),
    case
    when c2.uuid ='36d6616b-8c7c-4768-9f38-2be4b704fccd' then 'Positif pour MTB'
    when c2.uuid  = 'f4ee3bcc-947c-4390-9190-a335c2cd5868' then 'Positif pour non MTB'
    when o.value_coded  =664 then 'Négatif'
    when o.value_coded  =160008 then 'Contaminé'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =159982
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '224546bb-03da-4793-9ef6-3d42adc5b353'
 inner join cepoz.concept c2 on c2.concept_id = o.value_coded and c2.uuid in ('36d6616b-8c7c-4768-9f38-2be4b704fccd', 'f4ee3bcc-947c-4390-9190-a335c2cd5868')
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- dst
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'dst', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then '"Sensible à la Rifampicine'
    when o.value_coded =162203 then 'Résistant à la rifampicine'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id =159984
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '224546bb-03da-4793-9ef6-3d42adc5b353'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- poids
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'poids', date(e.encounter_datetime), o.value_numeric, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id = 5089
    inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '224546bb-03da-4793-9ef6-3d42adc5b353'
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- Obs: 67f6a83d-5aa2-4da5-950c-833dadfa8916
-- bacilloccopie
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'bacilloccopie', date(e.encounter_datetime),
    case
    when o.value_coded =664 then 'Négatif'
    when o.value_coded =1362 then 'Positif+'
    when o.value_coded =1363 then 'Positif++'
    when o.value_coded =1364 then 'Positif+++'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =307
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '67f6a83d-5aa2-4da5-950c-833dadfa8916'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'genxpert_bk', date(e.encounter_datetime),
    case
    when o.value_coded =1301 then 'Positif'
    when o.value_coded =1302 then 'Négatif'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '67f6a83d-5aa2-4da5-950c-833dadfa8916'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- genxpert_rif -- ps: Meme uuid que genxpert_bk
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'genxpert_rif', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then 'Sensible' replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'medicaments_allergies_aucun', date(e.encounter_datetime), o.value_coded, 'x',now()
from cepoz.patient p
    when o.value_coded =162203 then 'Resistant'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '67f6a83d-5aa2-4da5-950c-833dadfa8916'
 inner join cepoz.concept c2 on c2.concept_id = o.concept_id and c2.uuid = '4cbdc90a-e007-4a48-af54-5dd204edadd9'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- culture -- -- -A revoir: deuxieme uuid inexistant
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'culture', date(e.encounter_datetime),
    case
    when c2.uuid ='36d6616b-8c7c-4768-9f38-2be4b704fccd' then 'Positif pour MTB'
    when c2.uuid  = 'f4ee3bcc-947c-4390-9190-a335c2cd5868' then 'Positif pour non MTB'
    when o.value_coded  =664 then 'Négatif'
    when o.value_coded  =160008 then 'Contaminé'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id and o.concept_id =159982
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '67f6a83d-5aa2-4da5-950c-833dadfa8916'
 inner join cepoz.concept c2 on c2.concept_id = o.value_coded and c2.uuid in ('36d6616b-8c7c-4768-9f38-2be4b704fccd', 'f4ee3bcc-947c-4390-9190-a335c2cd5868')
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- dst
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'dst', date(e.encounter_datetime),
    case
    when o.value_coded =162204 then '"Sensible à la Rifampicine'
    when o.value_coded =162203 then 'Résistant à la rifampicine'
    when o.value_coded =1138 then 'Indéterminé'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
 inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id =159984
 inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '67f6a83d-5aa2-4da5-950c-833dadfa8916'
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- poids
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'poids', date(e.encounter_datetime), o.value_numeric, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.encounter_id = e.encounter_id
    inner join cepoz.obs o2 on o2.obs_id = o.obs_group_id and o.concept_id = 5089
    inner join cepoz.concept c on c.concept_id = o2.concept_id and c.uuid = '67f6a83d-5aa2-4da5-950c-833dadfa8916'
>>>>>>> 6e7d1e221fd6a5112ca7d69658e582278086de69
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- MÉDICAMENTS : ALLERGIES  -- A revoir
-- medicaments_allergies_aucun
insert into
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3rom cepoz.patient p
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id = 160643  and o.encounter_id = e.encounter_id and o.value_coded =1107
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- ÉLIGIBILITÉ MÉDICALE AUX ARV
-- eligibilite_medicale_aux_arv
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'eligibilite_medicale_aux_arv', date(e.encounter_datetime),
    case
    when o.value_coded =1065 then 'Oui - préciser la raison'
    when o.value_coded =1066 then "Non - pas d'éligibilité médicale aujourd'hui"
    when o.value_coded =1067 then 'À déterminer'
    else ''
end, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =162703 and o.encounter_id = e.encounter_id
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- stade
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'stade', date(e.encounter_datetime),
    case
    when o.value_coded =1204 then 'Stade I (Asymptomatique'
    when o.value_coded =1205 then 'Stade II (Symptomatique)'
    when o.value_coded =1206 then 'Stade III (Symptomatique)'
    when o.value_coded =1207 then 'Stade IV (SIDA)'
    else ''
end, 'x',now()
       f
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5356 and o.encounter_id = e.encounter_id
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- date_prochaine_visite
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "followup", p.patient_id, id.identifier, 'date_prochaine_visite', date(e.encounter_datetime), o.value_datetime, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 2
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =5096 and o.encounter_id = e.encounter_id
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- ---------------------------------------------------Fiche d'adherence AD------------------------------------------------------------------
-- Date visite  //non  fait

-- Quel pourcentage de doses le patient a-t-il pris le mois dernier ? : 0%,10%.....
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "adherence_counseling", p.patient_id, id.identifier, 'pourcentage_doses_mois_dernier', date(e.encounter_datetime),
    case
    when o.value_numeric =0 then '0%'
    when o.value_numeric =5 then '5%'
    when o.value_numeric =10 then '10%'
    when o.value_numeric =15 then '15%'
    when o.value_numeric =20 then '20%'
    when o.value_numeric =25 then '25%'
    when o.value_numeric =30 then '30%'
    when o.value_numeric =35 then '35%'
    when o.value_numeric =40 then '40%'
    when o.value_numeric =45 then '45%'
    when o.value_numeric =50 then '50%'
    when o.value_numeric =55 then '55%'
    when o.value_numeric =60 then '60%'
    when o.value_numeric =65 then '65%'
    when o.value_numeric =70 then '70%'
    when o.value_numeric =75 then '75%'
    when o.value_numeric =80 then '80%'
    when o.value_numeric =85 then '85%'
    when o.value_numeric =90 then '90%'
    when o.value_numeric =95 then '95%'
    when o.value_numeric =100 then '100%'
    else ''
end, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 15
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =163710 and o.encounter_id = e.encounter_id
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- Raison donnée pour avoir manqué une dose, cocher le ou les cas ci-dessous
-- Médicament non-disponible à la clinique
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "adherence_counseling", p.patient_id, id.identifier, 'medocs_non_dispo_dans_clinique', date(e.encounter_datetime), o.value_coded, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 15
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160582 and o.encounter_id = e.encounter_id and o.value_coded =1754
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- A oublié
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "adherence_counseling", p.patient_id, id.identifier, 'a_oublie', date(e.encounter_datetime), o.value_coded, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 15
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160582 and o.encounter_id = e.encounter_id and o.value_coded =160587
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- Effets secondaires, préciser ci-dessous
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "adherence_counseling", p.patient_id, id.identifier, 'effets_secondaires', date(e.encounter_datetime), o.value_coded, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 15
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160582 and o.encounter_id = e.encounter_id and o.value_coded =1778
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- Emprisonné
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "adherence_counseling", p.patient_id, id.identifier, 'Emprisonné', date(e.encounter_datetime), o.value_coded, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 15
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160582 and o.encounter_id = e.encounter_id and o.value_coded =156761
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- s'est_senti_trop_malade
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "adherence_counseling", p.patient_id, id.identifier, "S'est_senti_trop_malade", date(e.encounter_datetime), o.value_coded, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 15
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160582 and o.encounter_id = e.encounter_id and o.value_coded =160585
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- Gêné de prendre des médicaments en présence d'autres personnes
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "adherence_counseling", p.patient_id, id.identifier, "Gêné de prendre des médicaments en présence d'autres personnes", date(e.encounter_datetime), o.value_coded, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 15
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160582 and o.encounter_id = e.encounter_id and o.value_coded =160589
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- difficultés_a_avaler
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "adherence_counseling", p.patient_id, id.identifier, 'difficultés_a_avaler', date(e.encounter_datetime), o.value_coded, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 15
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160582 and o.encounter_id = e.encounter_id and o.value_coded =5954
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- En voyage
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
select 95698, "adherence_counseling", p.patient_id, id.identifier, 'en_voyage', date(e.encounter_datetime), o.value_coded, 'x',now()
from cepoz.patient p
    inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
    inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 15
    inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160582 and o.encounter_id = e.encounter_id and o.value_coded =124153
    on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- Manque de nourriture
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "adherence_counseling", p.patient_id, id.identifier, 'manque_de_nourriture', date(e.encounter_datetime), o.value_coded, 'x',now()
       from cepoz.patient p
 inner join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 15
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160582 and o.encounter_id = e.encounter_id and o.value_coded =119533
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');








-- data from iSante (CS): PRESCRIPTION AND PEDIATRIC PRESCRIPTION
-- DATE DE VISITE
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "DATE DE VISITE", e.createDate, e.visitDate 
	FROM itech.encounter e, itech.patient p
		WHERE e.patientID = p.patientID 
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');

-- DATE D'INITIATION ARV  		

INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "DATE D'INITIATION ARV", ymdtoDate(t.arvStartDateYy,t.arvStartDateMm,t.arvStartDateDd), e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptionOtherFields  t
		WHERE e.patientID = p.patientID 
            AND p.patientID=t.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(t.visitDateYy,t.visitDateMm,t.visitDateDd)
            AND e.seqnum=t.seqnum
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');
            
            
-- DISPENSATION COMMUNAUTAIRE        
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "DISPENSATION COMMUNAUTAIRE", o.value_boolean, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.obs  o
		WHERE e.patientID = p.patientID 
            AND o.encounter_id=e.encounter_id
            AND o.concept_id=71642
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');   
            
-- RÉGIME DE 1ÈRE LIGNE    
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "RÉGIME DE 1ÈRE LIGNE", o.value_numeric, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.obs  o
		WHERE e.patientID = p.patientID 
            AND o.encounter_id=e.encounter_id
            AND o.concept_id=163608
            AND o.value_numeric=1
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');       

--  RÉGIME DE 2ÈME LIGNE   
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "RÉGIME DE 2ÈME LIGNE", o.value_numeric, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.obs  o
		WHERE e.patientID = p.patientID 
            AND o.encounter_id=e.encounter_id
            AND o.concept_id=163608
            AND o.value_numeric=2
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');             
            
  -- RÉGIME DE 3ÈME LIGNE      
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "RÉGIME DE 3ÈME LIGNE", o.value_numeric, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.obs  o
		WHERE e.patientID = p.patientID 
            AND o.encounter_id=e.encounter_id
            AND o.concept_id=163608
            AND o.value_numeric=4
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');    
            
            
-- ABC RX  PROPHY
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "ABC RX  PROPHY", 
		  case when pr.forPepPmtct=1 then 'ABC-PROPHY' 
			   when pr.forPepPmtct=2 then 'ABC-RX'
			   else 'ABC'
		   end, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=1
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n'); 
            
            
            
-- 3TC  RX  PROPHY
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "3TC  RX  PROPHY", 
		  case when pr.forPepPmtct=1 then '3TC-PROPHY' 
			   when pr.forPepPmtct=2 then '3TC-RX'
			   else '3TC'
		   end, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=20
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');      
            
            
 -- FTC  RX  PROPHY
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "FTC  RX  PROPHY", 
		  case when pr.forPepPmtct=1 then 'FTC-PROPHY' 
			   when pr.forPepPmtct=2 then 'FTC-RX'
			   else 'FTC'
		   end, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=12
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');   
            
            
 -- TNF  RX  PROPHY
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "TNF  RX  PROPHY", 
		  case when pr.forPepPmtct=1 then 'TNF-PROPHY' 
			   when pr.forPepPmtct=2 then 'TNF-RX'
			   else 'TNF'
		   end, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=31
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');     
            
            
-- AZT  RX  PROPHY       
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "AZT  RX  PROPHY", 
		  case when pr.forPepPmtct=1 then 'AZT-PROPHY' 
			   when pr.forPepPmtct=2 then 'AZT-RX'
			   else 'AZT'
		   end, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=34
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');       
            
            
 -- EFV  RX  PROPHY       
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "EFV  RX  PROPHY", 
		  case when pr.forPepPmtct=1 then 'EFV-PROPHY' 
			   when pr.forPepPmtct=2 then 'EFV-RX'
			   else 'EFV'
		   end, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=11
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');    
            
            
 -- ATAZANAVIR + BOSTRTV RX  PROPHY       
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "ATAZANAVIR + BOSTRTV RX  PROPHY", 
		  case when pr.forPepPmtct=1 then 'ATAZANAVIR + BOSTRTV-PROPHY' 
			   when pr.forPepPmtct=2 then 'ATAZANAVIR + BOSTRTV-RX'
			   else 'ATAZANAVIR + BOSTRTV'
		   end, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=6
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');  
            
 -- LOPINAVIR + BOSTRTV RX  PROPHY       
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "LOPINAVIR + BOSTRTV RX  PROPHY", 
		  case when pr.forPepPmtct=1 then 'LOPINAVIR + BOSTRTV-PROPHY' 
			   when pr.forPepPmtct=2 then 'LOPINAVIR + BOSTRTV-RX'
			   else 'LOPINAVIR + BOSTRTV'
		   end, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=21
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');
            
            
 -- RALTEGRAVIR  RX  PROPHY       
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "RALTEGRAVIR  RX  PROPHY", 
		  case when pr.forPepPmtct=1 then 'RALTEGRAVIR-PROPHY' 
			   when pr.forPepPmtct=2 then 'RALTEGRAVIR-RX'
			   else 'RALTEGRAVIR'
		   end, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=87
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');    
            
 -- DOLUTEGRAVIR  RX  PROPHY       
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "DOLUTEGRAVIR  RX  PROPHY", 
		  case when pr.forPepPmtct=1 then 'DOLUTEGRAVIR-PROPHY' 
			   when pr.forPepPmtct=2 then 'DOLUTEGRAVIR-RX'
			   else 'DOLUTEGRAVIR'
		   end, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=89
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');         
            
-- COTRIMOXAZOLE  RX  PROPHY   
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "COTRIMOXAZOLE  RX  PROPHY", 
		  case when pr.forPepPmtct=1 then 'COTRIMOXAZOLE-PROPHY' 
			   when pr.forPepPmtct=2 then 'COTRIMOXAZOLE-RX'
			   else 'COTRIMOXAZOLE'
		   end, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=9
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');  
            
-- INH RX  PROPHY
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "INH RX  PROPHY", 
		  case when pr.forPepPmtct=1 then 'INH-PROPHY' 
			   when pr.forPepPmtct=2 then 'INH-RX'
			   else 'INH'
		   end, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=18
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');   
            
-- INH NOMBRE DE JOURS
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "INH NOMBRE DE JOURS", 
		  pr.numDaysDesc, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=18
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');
            
            
-- ____________________ MEDICAMENT DISPENSE ------------------------

            
-- ABC DISPENSE
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "ABC DISPENSE", 
		  case when pr.dispensed=1 then 'OUI' 
			   when pr.dispensed=2 then 'NON'
			   else ''
		   end, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=1
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n'); 
            
            
            
-- 3TC  DISPENSE
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "3TC  DISPENSE", 
		  case when pr.dispensed=1 then 'OUI' 
			   when pr.dispensed=2 then 'NON'
			   else ''
		   end, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=20
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');      
            
            
 -- FTC DISPENSE
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "FTC DISPENSE", 
		  case when pr.dispensed=1 then 'OUI' 
			   when pr.dispensed=2 then 'NON'
			   else ''
		   end, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=12
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');   
            
            
 -- TNF DISPENSE
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "TNF DISPENSE", 
		  case when pr.dispensed=1 then 'OUI' 
			   when pr.dispensed=2 then 'NON'
			   else ''
		   end, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=31
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');     
            
            
-- AZT DISPENSE       
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "AZT DISPENSE", 
		  case when pr.dispensed=1 then 'OUI' 
			   when pr.dispensed=2 then 'NON'
			   else ''
		   end, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=34
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');       
            
            
 -- EFV DISPENSE       
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "EFV DISPENSE", 
		  case when pr.dispensed=1 then 'OUI' 
			   when pr.dispensed=2 then 'NON'
			   else ''
		   end, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=11
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');    
            
            
 -- ATAZANAVIR + BOSTRTV DISPENSE       
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "ATAZANAVIR + BOSTRTV DISPENSE", 
		  case when pr.dispensed=1 then 'OUI' 
			   when pr.dispensed=2 then 'NON'
			   else ''
		   end, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=6
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n'); 
            
 -- LOPINAVIR + BOSTRTV DISPENSE       
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "LOPINAVIR + BOSTRTV DISPENSE", 
		  case when pr.dispensed=1 then 'OUI' 
			   when pr.dispensed=2 then 'NON'
			   else ''
		   end, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=21
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n'); 
            
            
 -- RALTEGRAVIR  DISPENSE       
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "RALTEGRAVIR  DISPENSE", 
		  case when pr.dispensed=1 then 'OUI' 
			   when pr.dispensed=2 then 'NON'
			   else ''
		   end, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=87
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');    
            
 -- DOLUTEGRAVIR  DISPENSE       
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "DOLUTEGRAVIR  DISPENSE", 
		  case when pr.dispensed=1 then 'OUI' 
			   when pr.dispensed=2 then 'NON'
			   else ''
		   end, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=89
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');         
            
-- COTRIMOXAZOLE  DISPENSE   
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "COTRIMOXAZOLE  DISPENSE", 
		  case when pr.dispensed=1 then 'OUI' 
			   when pr.dispensed=2 then 'NON'
			   else ''
		   end, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=9
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');  
            
-- INH DISPENSE
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "INH DISPENSE", 
		  case when pr.dispensed=1 then 'OUI' 
			   when pr.dispensed=2 then 'NON'
			   else ''
		   end, e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=18
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');  
            
            
-- ____________________ DATE DISPENSE ------------------------

            
-- ABC DISPENSE
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "ABC DATE DISPENSE", 
		  ymdtoDate(pr.dispDateYy,pr.dispDateMm,pr.dispDateMm), e.visitDate
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=1
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n'); 
            
            
            
-- 3TC  DISPENSE
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "3TC  DATE DISPENSE", 
		  ymdtoDate(pr.dispDateYy,pr.dispDateMm,pr.dispDateMm), e.visitDate
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=20
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');      
            
            
 -- FTC DISPENSE
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "FTC DATE DISPENSE", 
		  ymdtoDate(pr.dispDateYy,pr.dispDateMm,pr.dispDateMm), e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=12
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');   
            
            
 -- TNF DISPENSE
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "TNF DATE DISPENSE", 
		  ymdtoDate(pr.dispDateYy,pr.dispDateMm,pr.dispDateMm), e.visitDate
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=31
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');     
            
            
-- AZT DISPENSE       
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "AZT DATE DISPENSE", 
		  ymdtoDate(pr.dispDateYy,pr.dispDateMm,pr.dispDateMm), e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=34
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');       
            
            
 -- EFV DISPENSE       
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "EFV DATE DISPENSE", 
		 ymdtoDate(pr.dispDateYy,pr.dispDateMm,pr.dispDateMm), e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=11
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');   
            
            
 -- ATAZANAVIR + BOSTRTV DISPENSE       
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "ATAZANAVIR + BOSTRTV DATE DISPENSE", 
		 ymdtoDate(pr.dispDateYy,pr.dispDateMm,pr.dispDateMm), e.visitDate
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=6
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');  
            
 -- LOPINAVIR + BOSTRTV DISPENSE       
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "LOPINAVIR + BOSTRTV DATE DISPENSE", 
		  ymdtoDate(pr.dispDateYy,pr.dispDateMm,pr.dispDateMm), e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=21
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');
            
            
 -- RALTEGRAVIR  DISPENSE       
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "RALTEGRAVIR  DATE DISPENSE", 
		  ymdtoDate(pr.dispDateYy,pr.dispDateMm,pr.dispDateMm), e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=87
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');   
            
 -- DOLUTEGRAVIR  DISPENSE       
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "DOLUTEGRAVIR  DATE DISPENSE", 
		  ymdtoDate(pr.dispDateYy,pr.dispDateMm,pr.dispDateMm), e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=89
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n');        
            
-- COTRIMOXAZOLE  DISPENSE   
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "COTRIMOXAZOLE  DATE DISPENSE", 
		  ymdtoDate(pr.dispDateYy,pr.dispDateMm,pr.dispDateMm), e.visitDate
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=9
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n'); 
            
-- INH DISPENSE
INSERT INTO replication.validation (site, form, patient, st_code, param, collection_date, isante)
	select e.siteCode, case when e.encounterType=5 then "PRESCRIPTION"
                            when e.encounterType=18 then "PEDIATRIC PRESCRIPTION"
					   end, e.patientID, p.clinicPatientID, "INH DATE DISPENSE", 
		  ymdtoDate(pr.dispDateYy,pr.dispDateMm,pr.dispDateMm), e.visitDate 
	FROM itech.encounter e, itech.patient p,itech.prescriptions  pr
		WHERE e.patientID = p.patientID 
            AND pr.patientID=e.patientID
            AND ymdtoDate(e.visitDateYy,e.visitDateMm,e.visitDateDd)=ymdtoDate(pr.visitDateYy,pr.visitDateMm,pr.visitDateDd)
            AND e.seqnum=pr.seqnum
            AND pr.drugID=18
			AND e.encounterType in (5,18)
			ON DUPLICATE KEY UPDATE isante_value = VALUES (isante_value), valid = if(isanteplus_value_value = VALUES(isante_value), 'Y', 'n'); 
            
/* end of Ordonnance */            