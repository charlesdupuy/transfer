 -- consolidated isante database
-- itech
-- form: Adherence Counseling
-- param: Date de Visite
-- site code: 95698

insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, 'Adherence Counseling', e.patientID, p.clinicPatientID, 'Date de Visite', e.createDate, date(concat(e.visitDateYy,'-',e.visitDateMm,'-',e.visitDateDd)), 'x', date(now())
       from itech.encounter e, itech.patient p
      where e.patientID = p.patientID
        and e.encounterType in (14, 20)param
        and e.siteCode = 95698
         on duplicate key update isante_value = values(isante_value), is_valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- consolidated isante database
-- itech
-- form: Adherence Counseling
-- param: Pourcentage de doses pour le mois dernier
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, 'Adherence Counseling', e.patientID as patient_id, p.clinicPatientID, 'Pourcentage de doses pour le mois dernier',e.createDate,
 case  when ac.doseProp =1 then '0%'
       when ac.doseProp =2 then '10%'
       when ac.doseProp =4 then '20%'
       when ac.doseProp =8 then '30%'
       when ac.doseProp =16 then '40%'
       when ac.doseProp =32 then '50%'
       when ac.doseProp =64 then '60%'
       when ac.doseProp =128 then '70%'
       when ac.doseProp =256 then '80%'
       when ac.doseProp =512 then '90%'
       when ac.doseProp =1024 then '100%'
       else '' end as 'pourcentage_dose', 'x',date(now())
		from itech.patient p, itech.encounter e,itech.adherenceCounseling ac
		WHERE p.patientID = e.patientID 
		AND e.siteCode = ac.siteCode
		AND e.patientID = ac.patientID
		and e.siteCode='95698'
		and e.encounterType in (14, 20)
		AND concat(e.visitDateYy,"-",e.visitDateMm,"-",e.visitDateDd) =	concat(ac.visitDateYy,"-",ac.visitDateMm,"-",ac.visitDateDd)
		AND ac.doseProp IN(1,2,4,8,16,32,64,128,256,512,1024)
	on duplicate key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');
-- isante consolidated database
-- cepo 
-- form: Adherence Counseling
-- param: Médicament non-disponible à la clinique
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, "Adherence Counseling", e.patientID,p.clinicPatientID, 'Médicament non-disponible à la clinique', e.createDate, 'oui' as 'medocs_non_dispo_dans_clinique', 'x',date(now())
from itech.encounter e,itech.adherenceCounseling ac,patient p
		WHERE e.siteCode = ac.siteCode
		AND e.patientID = ac.patientID
		AND e.patientID = p.patientID
		AND concat(e.visitDateYy,"-",e.visitDateMm,"-",e.visitDateDd) =	concat(ac.visitDateYy,"-",ac.visitDateMm,"-",ac.visitDateDd)
		AND e.seqNum=ac.seqNum
		AND e.siteCode=95698
		AND ac.reasonNotAvail=1
on duplicate key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');
			
-- isante consolidated database
-- cepoz
-- form: Adherence Counseling
-- param: A Oublié
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, "Adherence Counseling", e.patientID, p.clinicPatientID, 'A Oublié', e.createDate, 'oui', 'x', date(now())
from patient p, itech.encounter e, itech.adherenceCounseling ac
		WHERE p.patientID = e.patientID 
		AND e.siteCode = ac.siteCode
		AND e.patientID = ac.patientID
		AND e.seqNum=ac.seqNum
		AND e.siteCode=95698
		AND concat(e.visitDateYy,"-",e.visitDateMm,"-",e.visitDateDd) =	concat(ac.visitDateYy,"-",ac.visitDateMm,"-",ac.visitDateDd)
		AND ac.reasonForgot=1
on duplicate key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');		
		
-- isante consolidated database
-- cepoz
-- form: Adherence Counseling
-- param: Effets secondaires
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, "Adherence Counseling", e.patientID, p.clinicPatientID, 'Effets Secondaires', e.createDate, 'oui', 'x', date(now())
from patient p, itech.encounter e, itech.adherenceCounseling ac
		WHERE p.patientID = e.patientID 
		AND e.siteCode = ac.siteCode
		AND e.patientID = ac.patientID
		AND e.seqNum=ac.seqNum
		AND e.siteCode=95698
		AND concat(e.visitDateYy,"-",e.visitDateMm,"-",e.visitDateDd) =	concat(ac.visitDateYy,"-",ac.visitDateMm,"-",ac.visitDateDd)
		AND ac.reasonSideEff=1
on duplicate key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');		
		


-- isante consolidated database
-- cepoz
-- form: Adherence Counseling
-- param: Emprisonné
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, "Adherence Counseling", e.patientID, p.clinicPatientID, 'Emprisonné', e.createDate, 'oui', 'x', date(now())
from patient p, itech.encounter e, itech.adherenceCounseling ac
		WHERE p.patientID = e.patientID 
		AND e.siteCode = ac.siteCode
		AND e.patientID = ac.patientID
		AND e.seqNum=ac.seqNum
		AND e.siteCode=95698
		AND concat(e.visitDateYy,"-",e.visitDateMm,"-",e.visitDateDd) =	concat(ac.visitDateYy,"-",ac.visitDateMm,"-",ac.visitDateDd)
		AND ac.reasonPrison=1
on duplicate key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');		

-- isante consolidated database
-- cepoz
-- form: Adherence Counseling
-- param: S'est senti trop malade
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, "Adherence Counseling", e.patientID, p.clinicPatientID, "S'est senti trop malade", e.createDate, 'oui', 'x', date(now())
from patient p, itech.encounter e, itech.adherenceCounseling ac
		WHERE p.patientID = e.patientID 
		AND e.siteCode = ac.siteCode
		AND e.patientID = ac.patientID
		AND e.seqNum=ac.seqNum
		AND e.siteCode=95698
		AND concat(e.visitDateYy,"-",e.visitDateMm,"-",e.visitDateDd) =	concat(ac.visitDateYy,"-",ac.visitDateMm,"-",ac.visitDateDd)
		AND ac.reasonTooSick=1
on duplicate key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');		

-- isante consolidated database
-- cepoz
-- form: Adherence Counseling
-- param: Gêné de prendre des médicaments en présence d'autres personnes
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, "Adherence Counseling", e.patientID, p.clinicPatientID, "Gêné de prendre des médicaments en présence d'autres personnes", e.createDate, 'oui', 'x', date(now())
from patient p, itech.encounter e, itech.adherenceCounseling ac
		WHERE p.patientID = e.patientID 
		AND e.siteCode = ac.siteCode
		AND e.patientID = ac.patientID
		AND e.seqNum=ac.seqNum
		AND e.siteCode=95698
		AND concat(e.visitDateYy,"-",e.visitDateMm,"-",e.visitDateDd) =	concat(ac.visitDateYy,"-",ac.visitDateMm,"-",ac.visitDateDd)
		AND ac.reasonNotComf=1
on duplicate key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');		

-- isante consolidated database
-- cepoz
-- form: Adherence Counseling
-- param: Difficultés à avaler
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, "Adherence Counseling", e.patientID, p.clinicPatientID, 'Difficultés à Avaler', e.createDate, 'oui', 'x', date(now())
from patient p, itech.encounter e, itech.adherenceCounseling ac
		WHERE p.patientID = e.patientID 
		AND e.siteCode = ac.siteCode
		AND e.patientID = ac.patientID
		AND e.seqNum=ac.seqNum
		AND e.siteCode=95698
		AND concat(e.visitDateYy,"-",e.visitDateMm,"-",e.visitDateDd) =	concat(ac.visitDateYy,"-",ac.visitDateMm,"-",ac.visitDateDd)
		AND ac.reasonNoSwallow=1
on duplicate key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');		

-- isante consolidated database
-- cepoz
-- form: Adherence Counseling
-- param: En voyage
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, "Adherence Counseling", e.patientID, p.clinicPatientID, 'En Voyage', e.createDate, 'oui', 'x', date(now())
from patient p, itech.encounter e, itech.adherenceCounseling ac
		WHERE p.patientID = e.patientID 
		AND e.siteCode = ac.siteCode
		AND e.patientID = ac.patientID
		AND e.seqNum=ac.seqNum
		AND e.siteCode=95698
		AND concat(e.visitDateYy,"-",e.visitDateMm,"-",e.visitDateDd) =	concat(ac.visitDateYy,"-",ac.visitDateMm,"-",ac.visitDateDd)
		AND ac.reasonTravel=1
on duplicate key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');		

-- isante consolidated database
-- cepoz
-- form: Adherence Counseling
-- param: Manque de nourriture
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, "Adherence Counseling", e.patientID, p.clinicPatientID, 'Manque de Nourriture', e.createDate, 'oui', 'x', date(now())
from patient p, itech.encounter e, itech.adherenceCounseling ac
		WHERE p.patientID = e.patientID 
		AND e.siteCode = ac.siteCode
		AND e.patientID = ac.patientID
		AND e.seqNum=ac.seqNum
		AND e.siteCode=95698
		AND concat(e.visitDateYy,"-",e.visitDateMm,"-",e.visitDateDd) =	concat(ac.visitDateYy,"-",ac.visitDateMm,"-",ac.visitDateDd)
		AND ac.reasonNoFood=1
on duplicate key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');		






	

-- Fiche d'adherence et de discontinuation
-- isanteplus database
-- cepoz
-- form: Adherence Counseling
-- param: Date de Visite
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Adherence Counseling', pid.identifier as patient_id, id.identifier, 'Date de Visite', e.date_created, date(e.encounter_datetime), 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 8 and e.voided =0
      where  p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');
   
-- isanteplus database
-- cepoz 
-- form: Adherence Counseling
-- param: Pourcentage de doses pour le mois dernier
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Adherence Counseling', pid.identifier as patient_id, id.identifier, 'Pourcentage de doses pour le mois dernier', e.date_created,
 case
       when o.value_numeric =0 then '0%'
       when o.value_numeric =5 then '5%'
       when o.value_numeric =10 then '10%'
       when o.value_numeric =15 then '15%'
       when o.value_numeric =20 then '20%'
       when o.value_numeric =25 then '25%'
       when o.value_numeric =30 then '30%'
       when o.value_numeric =35 then '35%'
       when o.value_numeric =40 then '40%'
       when o.value_numeric =45 then '45%'
       when o.value_numeric =50 then '50%'
       when o.value_numeric =55 then '55%'
       when o.value_numeric =60 then '60%'
       when o.value_numeric =65 then '65%'
       when o.value_numeric =70 then '70%'
       when o.value_numeric =75 then '75%'
       when o.value_numeric =80 then '80%'
       when o.value_numeric =85 then '85%'
       when o.value_numeric =90 then '90%'
       when o.value_numeric =95 then '95%'
       when o.value_numeric =100 then '100%'
       else ''
end as 'pourcentage_dose', 'x',date(now())
      from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 8 and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =163710 and o.encounter_id = e.encounter_id and o.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');
   
-- isanteplus database
-- cepoz
-- form: Adherence Counseling
-- param: Médicament non-disponible à la clinique
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Adherence Counseling", pid.identifier as patient_id, id.identifier, 'Médicament non-disponible à la clinique', e.date_created, o.value_coded as 'medocs_non_dispo_dans_clinique', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 8 and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160582 and o.encounter_id = e.encounter_id and o.value_coded =1754 and o.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- isanteplus database
-- cepoz
-- form: Adherence Counseling
-- param: A Oublié
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Adherence Counseling", pid.identifier as patient_id, id.identifier, 'A Oublié', e.date_created, 'oui', 'x', date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 8 and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160582 and o.encounter_id = e.encounter_id and o.value_coded =160587 and o.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- isanteplus database
-- cepoz
-- form: Adherence Counseling
-- param: Effets secondaires
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Adherence Counseling", pid.identifier as patient_id, id.identifier, 'Effets Secondaires', e.date_created,'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 8 and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160582 and o.encounter_id = e.encounter_id and o.value_coded =1778 and o.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- isanteplus database
-- cepoz
-- form: Adherence Counseling
-- param: Emprisonné
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Adherence Counseling", pid.identifier as patient_id, id.identifier, 'Emprisonné',e.date_created,'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 8 and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160582 and o.encounter_id = e.encounter_id and o.value_coded =156761 and o.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- isanteplus database
-- cepoz
-- form: Adherence Counseling
-- param: S'est senti trop malade
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Adherence Counseling", pid.identifier as patient_id, id.identifier, "S'est senti trop malade", e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 8 and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160582 and o.encounter_id = e.encounter_id and o.value_coded =160585 and o.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- isanteplus database
-- cepoz
-- form: Adherence Counseling
-- param: Gêné de prendre des médicaments en présence d'autres personnes
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Adherence Counseling", pid.identifier as patient_id, id.identifier, "Gêné de prendre des médicaments en présence d'autres personnes", e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 8 and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160582 and o.encounter_id = e.encounter_id and o.value_coded =160589 and o.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- isanteplus database
-- cepoz
-- form: Adherence Counseling
-- param: Difficultés à avaler
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Adherence Counseling", pid.identifier as patient_id, id.identifier, 'Difficultés à Avaler', e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 8 and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160582 and o.encounter_id = e.encounter_id and o.value_coded =5954 and o.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- isanteplus database
-- cepoz
-- form: Adherence Counseling
-- param: En voyage
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Adherence Counseling", pid.identifier as patient_id, id.identifier, 'En Voyage', e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 8 and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160582 and o.encounter_id = e.encounter_id and o.value_coded =124153 and o.voided =0
      where p.voided =0
         on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- isanteplus database
-- cepoz
-- form: Adherence Counseling
-- param: Manque de nourriture
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, "Adherence Counseling", pid.identifier as patient_id, id.identifier, 'Manque de Nourriture',e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 8 and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =160582 and o.encounter_id = e.encounter_id and o.value_coded =119533 and o.voided =0
      where p.voided =0 on duplicate key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');


-- ------------------------Fiche de discontinuation
-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Date de Visite
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Date de Visite', e.date_created, date(e.encounter_datetime), 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 7 and e.voided =0
      where  p.voided =0
         on duplicate  key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Date d'arret de traitement VIH
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, "Date d'arret de traitement VIH", e.date_created, date(o.obs_datetime), 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 7 and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =164094 and o.encounter_id = e.encounter_id and o.voided =0
      where  p.voided =0
         on duplicate  key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Date du dernier contact avec le patient
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Date du dernier contact avec le patient', e.date_created, date(o.obs_datetime), 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 7 and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =164093 and o.encounter_id = e.encounter_id and o.voided =0
      where  p.voided =0
         on duplicate  key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Perdu de Vue de plus de trois ans
-- site code: 95698		
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Raison: Perdu de Vue de plus de trois ans', e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 7 and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =161555 and o.encounter_id = e.encounter_id and o.value_coded =5240 and o.voided =0
      where  p.voided =0
         on duplicate  key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Émigration
-- site code: 95698	
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Raison: Émigration', e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 7 and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =161555 and o.encounter_id = e.encounter_id and o.value_coded =160415 and o.voided =0
      where  p.voided =0
         on duplicate  key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Transfert vers un autre établissement
-- site code: 95698		
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Raison: Transfert vers un autre établissement',e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 7 and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =161555 and o.encounter_id = e.encounter_id and o.value_coded =159492  and o.voided =0
      where  p.voided =0
         on duplicate  key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Préférence du patient
-- site code: 95698			
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Raison: Préférence du patient', e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 7 and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =161555 and o.encounter_id = e.encounter_id and o.value_coded =162571 and o.voided =0
      where  p.voided =0
         on duplicate  key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param:Référence du médecin	
-- site code: 95698	
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Raison: Référence du médecin', e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 7 and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =161555 and o.encounter_id = e.encounter_id and o.value_coded =162591
      where p.voided =0
         on duplicate  key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');
         

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Tuberculose	
-- site code: 95698	
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Décès: Tuberculose',e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 7 and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1748 and o.encounter_id = e.encounter_id and o.value_coded =112141 and o.voided =0
      where  p.voided =0
         on duplicate  key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Maladies infectieuses et/ou parasitaires liées au VIH	
-- site code: 95698	
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Décès: Maladies infectieuses et/ou parasitaires liées au VIH',e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 7 and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1748 and o.encounter_id = e.encounter_id and o.value_coded =112141 and o.voided =0
      where  p.voided =0
         on duplicate  key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');
         	
-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Cancer lié au VIH
-- site code: 95698	
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Décès: Cancer lié au VIH', e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 7 and e.voided =0
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1748 and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.concept c on c.concept_id=o.concept_id and  c.uuid='8efa7daf-0fb1-49bd-8e1e-1702334246ba'
      where  p.voided =0
         on duplicate  key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');


-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Autres maladies ou conditions liées au VIH
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Décès: Autres maladies ou conditions liées au VIH', e.date_created, 'oui' , 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 7 and e.voided =0 
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1748 and o.encounter_id = e.encounter_id and o.value_coded= 5622 and o.voided =0
      where  p.voided =0
         on duplicate  key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Causes naturelles (cancer et infections, etc ) non liées au VIH	
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Décès: Causes naturelles (cancer et infections, etc ) non liées au VIH', e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 7 and e.voided =0 
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1748 and o.encounter_id = e.encounter_id and o.value_coded= 133481 and o.voided =0
      where  p.voided =0
         on duplicate  key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');
		
-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Causes non naturelles (traumatisme, accident, suicide, homicide, guerre, etc)
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Décès: Causes non naturelles (traumatisme, accident, suicide, homicide, guerre, etc)', e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 7 and e.voided =0 
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1748 and o.encounter_id = e.encounter_id and o.voided =0
 inner join cepoz.concept c on c.concept_id=o.concept_id and select * from cepoz.concept c where  c.uuid='768778dc-ce6a-47a0-8f78-b0777c8cf081'
      where  p.voided =0
         on duplicate  key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Inconnu
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Décès: Inconnu', e.date_created,'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 7 and e.voided =0 
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1748 and o.encounter_id = e.encounter_id and o.voided =0 and o.value_coded= 1067
      where  p.voided =0
         on duplicate  key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Préférence du patient
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Arrêt: Préférence du patient ou de la personne responsable',e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 7 and e.voided =0 
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1667 and o.encounter_id = e.encounter_id and o.value_coded= 159737 and o.voided =0
      where  p.voided =0
         on duplicate  key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');
	
-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Decision du prestataire
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Arrêt: Décision du prestataire',e.date_created,'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 7 and e.voided =0 
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1667 and o.encounter_id = e.encounter_id and o.value_coded= 162591 and o.voided =0
      where  p.voided =0
         on duplicate  key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');


-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Adhérence inadéquate
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Adhérence inadéquate', e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 7 and e.voided =0 
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1667 and o.encounter_id = e.encounter_id and o.value_coded= 115198 and o.voided =0
      where  p.voided =0
         on duplicate  key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');
	
-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Déni
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Déni',e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 7 and e.voided =0 
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1667 and o.encounter_id = e.encounter_id and o.value_coded= 155891 and o.voided =0
      where  p.voided =0
         on duplicate  key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');
	
-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Troubles psychiatriques et/ou psychologiques
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Troubles psychiatriques et/ou psychologiques',e.date_created,'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 7 and e.voided =0 
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1667 and o.encounter_id = e.encounter_id and o.value_coded= 134337 and o.voided =0
      where  p.voided =0
         on duplicate  key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Autre raison
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isanteplus_value, is_valid, version)
     select 95698, 'Discontinuation', pid.identifier as patient_id, id.identifier, 'Autre raison',e.date_created, 'oui', 'x',date(now())
       from cepoz.patient p
 left outer join cepoz.patient_identifier id on id.patient_id = p.patient_id and id.identifier_type = 3
 inner join cepoz.patient_identifier pid on pid.patient_id = p.patient_id and pid.identifier_type = 6
 inner join cepoz.encounter e on e.patient_id = p.patient_id and e.encounter_type = 7 and e.voided =0 and o.value_coded= 5622
 inner join cepoz.obs o on o.person_id = p.patient_id and o.concept_id =1667 and o.encounter_id = e.encounter_id and o.voided =0
      where  p.voided =0
         on duplicate  key update isanteplus_value = values(isanteplus_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');




-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Date de Visite
-- site code: 95698
insert into replication.validation (site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
     select e.siteCode, 'Discontinuation', e.patientID, p.clinicPatientID, 'Date de Visite', date(e.visitDate), date(e.visitDate), 'x', date(now())
       from itech.encounter e, itech.patient p
      where e.patientID = p.patientID
        and e.encounterType in (12, 21)
        and e.siteCode = 95698
         on duplicate key update isante_value = values(isante_value), is_valid = if(isanteplus_value = values(isante_value), 'y', 'n');


-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Date d'arret de traitement VIH
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, 'Discontinuation', e.patientID as patient_id, p.clinicPatientID, "Date d'arret de traitement VIH", date(concat(e.visitdateYy,'-',e.visitDateMm,'-',e.visitDateDd)), if( ifnull(de.disEnrollYy,'')='' and ifnull(de.disEnrollMm,'')='' and ifnull(de.disEnrollDd,'')='', '', date(concat(de.disEnrollYy,'-', de.disEnrollMm,'-', de.disEnrollDd))), 'x',date(now())
	from patient p, itech.encounter e, itech.discEnrollment de
	WHERE p.patientID = e.patientID and e.patientID = de.patientID and e.siteCode = de.siteCode 
	and concat(e.visitdateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(de.visitdateYy,'-',de.visitDateMm,'-',de.visitDateDd) 
	and e.seqNum = de.seqNum AND(de.disEnrollYy <> "" AND de.disEnrollYy is not null)
	on duplicate  key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

		
-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Date du dernier contact avec le patient
-- site code: 95698
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, 'Discontinuation', e.patientID as patient_id, p.clinicPatientID, "Date du dernier contact avec le patient", date(concat(e.visitdateYy,'-',e.visitDateMm,'-',e.visitDateDd)), if( ifnull(de.lastContactYy,'')='' and ifnull(de.lastContactMm,'')='' and ifnull(de.lastContactDd,'')='', '', date(concat(de.lastContactYy,'-', de.lastContactMm,'-', de.lastContactDd))), 'x',date(now())       
from itech.patient p, itech.encounter e, itech.discEnrollment de
	WHERE p.patientID = e.patientID and 
	e.patientID = de.patientID and e.siteCode = de.siteCode 
	and concat(e.visitdateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(de.visitdateYy,'-',de.visitDateMm,'-',de.visitDateDd) 
	and e.seqNum = de.seqNum AND(de.lastContactYy <> "" AND de.lastContactYy is not null)
	on duplicate  key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');
		
-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Perdu de Vue de plus de trois ans
-- site code: 95698		
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, 'Discontinuation', e.patientID as patient_id, p.clinicPatientID, 'Raison: Perdu de Vue de plus de trois ans', date(e.encounter_datetime), 'oui', 'x',date(now())
  from itech.patient p, itech.encounter e, itech.discEnrollment de
	WHERE c.uuid = e.encGuid and 
	e.patientID = de.patientID and e.siteCode = de.siteCode 
	and concat(e.visitdateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(de.visitdateYy,'-',de.visitDateMm,'-',de.visitDateDd) 
	and e.seqNum = de.seqNum AND(de.reasonDiscNoFollowup=1)
	on duplicate  key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Émigration
-- site code: 95698	
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, 'Discontinuation', e.patientID as patient_id, p.clinicPatientID, 'Raison: Émigration', e.createDate, 'oui', 'x',date(now())
from itech.patient p,itech.encounter e,obs o
where p.patientID=e.patientID and 
	   e.encounter_id=o.encounter_id and 
	   e.encStatus=0 and 
	   o.concept_id=163623 and 
	   o.value_boolean=1 and 
	   e.siteCode=95698
 on duplicate  key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');


-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Transfert vers un autre établissement
-- site code: 95698		
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, 'Discontinuation',e.patientID as patient_id, p.clinicPatientID, 'Raison: Transfert vers un autre établissement', e.createDate,'oui', 'x',date(now())
from patient p, itech.encounter e, itech.discEnrollment de
	WHERE p.patientID = e.patientID and 
	e.patientID = de.patientID and 
	e.siteCode = de.siteCode and 
	e.siteCode =95698 and
	and e.encStatus<255
	concat(e.visitdateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(de.visitdateYy,'-',de.visitDateMm,'-',de.visitDateDd) and
	e.seqNum = de.seqNum AND 
	de.reasonDiscTransfer=1
on duplicate  key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Préférence du patient
-- site code: 95698			
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, 'Discontinuation',e.patientID as patient_id, p.clinicPatientID, 'Raison: Préférence du patient', e.createDate,'oui', 'x',date(now())
from patient p, itech.encounter e, itech.discEnrollment de
	WHERE p.patientID = e.patientID and 
	e.patientID = de.patientID and 
	e.siteCode = de.siteCode and 
	e.siteCode =95698 and
	and e.encStatus<255
	concat(e.visitdateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(de.visitdateYy,'-',de.visitDateMm,'-',de.visitDateDd) and
	e.seqNum = de.seqNum AND 
	de.reasonDiscRef=1
on duplicate  key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');


-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param:Référence du médecin	
-- site code: 95698	
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, 'Discontinuation',e.patientID as patient_id, p.clinicPatientID, 'Raison: Référence du médecin', e.createDate,'oui', 'x',date(now())
from patient p, itech.encounter e, itech.discEnrollment de
	WHERE p.patientID = e.patientID and 
	e.patientID = de.patientID and 
	e.siteCode = de.siteCode and 
	e.siteCode =95698 and
	and e.encStatus<255
	concat(e.visitdateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(de.visitdateYy,'-',de.visitDateMm,'-',de.visitDateDd) and
	e.seqNum = de.seqNum AND 
	de.reasonDiscRef=2
on duplicate  key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');
    

-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Tuberculose	
-- site code: 95698	
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, 'Discontinuation', e.patientID as patient_id, p.clinicPatientID, 'Décès: Tuberculose', e.createDate, 'oui', 'x',date(now())
from itech.patient p,itech.encounter e,obs o
where p.patientID=e.patientID and 
	   e.encounter_id=o.encounter_id and 
	   e.encStatus<255 and 
	   o.concept_id=163610 and 
	   o.value_boolean=1 and 
	   e.siteCode=95698
 on duplicate  key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Maladies infectieuses et/ou parasitaires liées au VIH	
-- site code: 95698	
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, 'Discontinuation', e.patientID as patient_id, p.clinicPatientID, 'Décès: Maladies infectieuses et/ou parasitaires liées au VIH', e.createDate, 'oui', 'x',date(now())
from itech.patient p,itech.encounter e,obs o
where p.patientID=e.patientID and 
	   e.encounter_id=o.encounter_id and 
	   e.encStatus<255 and 
	   o.concept_id=163611 and 
	   o.value_boolean=1 and 
	   e.siteCode=95698
 on duplicate  key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');
        	
-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Cancer lié au VIH
-- site code: 95698	
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, 'Discontinuation', e.patientID as patient_id, p.clinicPatientID, 'Décès: Cancer lié au VIH', e.createDate, 'oui', 'x',date(now())
from itech.patient p,itech.encounter e,obs o
where p.patientID=e.patientID and 
	   e.encounter_id=o.encounter_id and 
	   e.encStatus<255 and 
	   o.concept_id=163612 and 
	   o.value_boolean=1 and 
	   e.siteCode=95698
 on duplicate  key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');
 
-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Autres maladies ou conditions liées au VIH
-- site code: 95698 
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, 'Discontinuation', e.patientID as patient_id, p.clinicPatientID, 'Décès: Autres maladies ou conditions liées au VIH', e.createDate, 'oui', 'x',date(now())
from itech.patient p,itech.encounter e,obs o
where p.patientID=e.patientID and 
	   e.encounter_id=o.encounter_id and 
	   e.encStatus<255 and 
	   o.concept_id=163613 and 
	   o.value_boolean=1 and 
	   e.siteCode=95698
 on duplicate  key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');
 
-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Causes naturelles (cancer et infections, etc ) non liées au VIH	
-- site code: 95698 
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, 'Discontinuation', e.patientID as patient_id, p.clinicPatientID, 'Décès: Causes naturelles (cancer et infections, etc ) non liées au VIH', e.createDate, 'oui', 'x',date(now())
from itech.patient p,itech.encounter e,obs o
where p.patientID=e.patientID and 
	   e.encounter_id=o.encounter_id and 
	   e.encStatus<255 and 
	   o.concept_id=163614 and 
	   o.value_boolean=1 and 
	   e.siteCode=95698
 on duplicate  key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');
 		
-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Causes non naturelles (traumatisme, accident, suicide, homicide, guerre, etc)
-- site code: 95698 
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, 'Discontinuation', e.patientID as patient_id, p.clinicPatientID, 'Décès: Causes non naturelles (traumatisme, accident, suicide, homicide, guerre, etc)', e.createDate, 'oui', 'x',date(now())
from itech.patient p,itech.encounter e,obs o
where p.patientID=e.patientID and 
	   e.encounter_id=o.encounter_id and 
	   e.encStatus<255 and 
	   o.concept_id=163615 and 
	   o.value_boolean=1 and 
	   e.siteCode=95698
 on duplicate  key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');
 
-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Inconnu
-- site code: 95698 
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, 'Discontinuation', e.patientID as patient_id, p.clinicPatientID, 'Décès: Inconnu', e.createDate, 'oui', 'x',date(now())
from itech.patient p,itech.encounter e,obs o
where p.patientID=e.patientID and 
	   e.encounter_id=o.encounter_id and 
	   e.encStatus<255 and 
	   o.concept_id=163616 and 
	   o.value_boolean=1 and 
	   e.siteCode=95698
 on duplicate  key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Préférence du patient
-- site code: 95698 
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, 'Discontinuation',e.patientID as patient_id, p.clinicPatientID, 'Arrêt: Préférence du patient ou de la personne responsable', e.createDate,'oui', 'x',date(now())
from patient p, itech.encounter e, itech.discEnrollment de
	WHERE p.patientID = e.patientID and 
	e.patientID = de.patientID and 
	e.siteCode = de.siteCode and 
	e.siteCode =95698 and
	and e.encStatus<255
	concat(e.visitdateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(de.visitdateYy,'-',de.visitDateMm,'-',de.visitDateDd) and
	e.seqNum = de.seqNum AND 
	de.patientPreference=1
on duplicate  key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');
	
-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Decision du prestataire
-- site code: 95698 
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, 'Discontinuation', e.patientID as patient_id, p.clinicPatientID, 'Arrêt: Décision du prestataire', e.createDate, 'oui', 'x',date(now())
from itech.patient p,itech.encounter e,obs o
where p.patientID=e.patientID and 
	   e.encounter_id=o.encounter_id and 
	   e.encStatus<255 and 
	   o.concept_id=163617 and 
	   o.value_boolean=1 and 
	   e.siteCode=95698
 on duplicate  key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Adhérence inadéquate
-- site code: 95698 
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, 'Discontinuation',e.patientID as patient_id, p.clinicPatientID, 'Adhérence inadéquate', e.createDate,'oui', 'x',date(now())
from patient p, itech.encounter e, itech.discEnrollment de
	WHERE p.patientID = e.patientID and 
	e.patientID = de.patientID and 
	e.siteCode = de.siteCode and 
	e.siteCode =95698 and
	and e.encStatus<255
	concat(e.visitdateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(de.visitdateYy,'-',de.visitDateMm,'-',de.visitDateDd) and
	e.seqNum = de.seqNum AND 
	de.poorAdherence=1
on duplicate  key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');
	
-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Déni
-- site code: 95698 
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, 'Discontinuation', e.patientID as patient_id, p.clinicPatientID, 'Déni', e.createDate, 'oui', 'x',date(now())
from itech.patient p,itech.encounter e,obs o
where p.patientID=e.patientID and 
	   e.encounter_id=o.encounter_id and 
	   e.encStatus<255 and 
	   o.concept_id=163618 and 
	   o.value_boolean=1 and 
	   e.siteCode=95698
 on duplicate  key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');
	
-- isante consolidated database
-- cepoz
-- form: Discontinuation
-- param: Troubles psychiatriques et/ou psychologiques
-- site code: 95698 
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, 'Discontinuation', e.patientID as patient_id, p.clinicPatientID, 'Troubles psychiatriques et/ou psychologiques', e.createDate, 'oui', 'x',date(now())
from itech.patient p,itech.encounter e,obs o
where p.patientID=e.patientID and 
	   e.encounter_id=o.encounter_id and 
	   e.encStatus<255 and 
	   o.concept_id=163619 and 
	   o.value_boolean=1 and 
	   e.siteCode=95698
 on duplicate  key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');

-- isanteplus database
-- cepoz
-- form: Discontinuation
-- param: Autre raison
-- site code: 95698 
insert into replication.validation(site_id, form_origin, patient_id, st_code, parameter, created_date, isante_value, is_valid, version)
select e.siteCode, 'Discontinuation',e.patientID as patient_id, p.clinicPatientID, 'Autre raison', e.createDate,'oui', 'x',date(now())
from patient p, itech.encounter e, itech.discEnrollment de
	WHERE p.patientID = e.patientID and 
	e.patientID = de.patientID and 
	e.siteCode = de.siteCode and 
	e.siteCode =95698 and
	and e.encStatus<255
	concat(e.visitdateYy,'-',e.visitDateMm,'-',e.visitDateDd) = concat(de.visitdateYy,'-',de.visitDateMm,'-',de.visitDateDd) and
	e.seqNum = de.seqNum AND 
	de.discReasonOther=1
on duplicate  key update isante_value = values(isante_value), is_valid = if(isante_value = values(isanteplus_value), 'y', 'n');
